<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\PlFileRepository")
 * @ORM\Table(name="pl_file")
 * @ExclusionPolicy("all")
 */
class PlFile
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_file", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;
    
    /**
     * @var PlFileCategory
     *
     * @ORM\ManyToOne(targetEntity="PlFileCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_file_category", referencedColumnName="id_file_category", nullable=false)
     * })
     */
    protected $fileCategory;
    
    /**
     * @var PlUser
     *
     * @ORM\ManyToOne(targetEntity="PlUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user_assigned_to", referencedColumnName="id_user", nullable=true)
     * })
     */
    private $userAssignedTo;

    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_account", referencedColumnName="id_account", nullable=false)
     * })
     */
    protected $account;
    
    /**
     * @var string
     *
     * @ORM\Column(name="file_name", type="string", length=45, nullable=true)
     * @Expose
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="file_extension", type="string", length=45, nullable=true)
     * @Expose
     */
    private $extension;

    /**
     * @var integer
     *
     * @ORM\Column(name="file_size", type="integer", nullable=true)
     * @Expose
     */
    private $size;

    /**
     * @var string
     *
     * @ORM\Column(name="file_type", type="string", length=45, nullable=true)
     * @Expose
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="file_delimeter", type="string", length=45, nullable=true)
     * @Expose
     */
    private $delimeter;

    /**
     * @var string
     *
     * @ORM\Column(name="file_identification", type="string", length=45, nullable=true)
     * @Expose
     */
    private $identification;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="file_header", type="datetime", nullable=true)
     * @Expose
     */
    private $header;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Expose
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @Expose
     */
    private $updated;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set extension
     *
     * @param string $extension
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;
    }

    /**
     * Get extension
     *
     * @return string 
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * Set size
     *
     * @param integer $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * Get size
     *
     * @return integer 
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set type
     *
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set delimeter
     *
     * @param string $delimeter
     */
    public function setDelimeter($delimeter)
    {
        $this->delimeter = $delimeter;
    }

    /**
     * Get delimeter
     *
     * @return string 
     */
    public function getDelimeter()
    {
        return $this->delimeter;
    }

    /**
     * Set identification
     *
     * @param string $identification
     */
    public function setIdentification($identification)
    {
        $this->identification = $identification;
    }

    /**
     * Get identification
     *
     * @return string 
     */
    public function getIdentification()
    {
        return $this->identification;
    }

    /**
     * Set header
     *
     * @param boolean $header
     */
    public function setHeader($header)
    {
        $this->header = $header;
    }

    /**
     * Get header
     *
     * @return boolean 
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }
    

    /**
     * Set fileCategory
     *
     * @param \Leadsius\ApiBundle\Entity\PlFileCategory $fileCategory
     * @return PlFile
     */
    public function setFileCategory(\Leadsius\ApiBundle\Entity\PlFileCategory $fileCategory)
    {
        $this->fileCategory = $fileCategory;

        return $this;
    }

    /**
     * Get fileCategory
     *
     * @return \Leadsius\ApiBundle\Entity\PlFileCategory 
     */
    public function getFileCategory()
    {
        return $this->fileCategory;
    }

    /**
     * Set userAssignedTo
     *
     * @param \Leadsius\ApiBundle\Entity\PlUser $userAssignedTo
     * @return PlFile
     */
    public function setUserAssignedTo(\Leadsius\ApiBundle\Entity\PlUser $userAssignedTo = null)
    {
        $this->userAssignedTo = $userAssignedTo;

        return $this;
    }

    /**
     * Get userAssignedTo
     *
     * @return \Leadsius\ApiBundle\Entity\PlUser 
     */
    public function getUserAssignedTo()
    {
        return $this->userAssignedTo;
    }

    /**
     * Set account
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $account
     * @return PlFile
     */
    public function setAccount(\Leadsius\ApiBundle\Entity\PlAccount $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Leadsius\ApiBundle\Entity\PlAccount 
     */
    public function getAccount()
    {
        return $this->account;
    }
}
