<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\PlUserRepository")
 * @ORM\Table(name="pl_user")
 */
class PlUser extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id_user", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="user_firstname", type="string", length=45, nullable=true)
     *
     * @Assert\NotBlank(message="Please enter your Firstname.", groups={"Registration", "Profile"})
     *
     * @Assert\Length(
     *      min = "2",
     *      max = "45",
     *      minMessage = "The Firstname is too short. {{ limit }} characters length",
     *      maxMessage = "The Firstname is too longn. {{ limit }} characters length"
     * )
     */
    protected $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="user_lastname", type="string", length=45, nullable=true)
     *
     * @Assert\NotBlank(message="Please enter your Lastname.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *      min = "2",
     *      max = "45",
     *      minMessage = "The Lastname is too short. {{ limit }} characters length",
     *      maxMessage = "The Lastname is too longn. {{ limit }} characters length"
     * )
     */
    protected $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="user_phone", type="string", length=45, nullable=true)
     */
    protected $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="user_city", type="string", length=45, nullable=true)
     */
    protected $city;

    /**
     * @var string
     *
     * @ORM\Column(name="user_state", type="string", length=45, nullable=true)
     */
    protected $state;

    /**
     * @var string
     *
     * @ORM\Column(name="user_skype", type="string", length=45, nullable=true)
     */
    protected $skypeId;

    /**
     * @var string
     *
     * @ORM\Column(name="user_title", type="string", length=45, nullable=true)
     */
    protected $title;

    /**
     * @var boolean
     *
     * @ORM\Column(name="user_is_administrator", type="boolean", nullable=true)
     */
    protected $isAdministrator;

    /**
     * @var boolean
     *
     * @ORM\Column(name="user_is_super_administrator", type="boolean", nullable=true)
     */
    protected $isSuperAdministrator;

    /**
     * @var datetime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    protected $created;

    /**
     * @var datetime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     */
    protected $updated;

    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount", inversedBy="users")
     * @ORM\JoinColumn(name="id_account", referencedColumnName="id_account", nullable=false)
     */
    protected $account;
    
    /**
     * @var ArrayCollection
     * 
     * @ORM\OneToMany(targetEntity="PlContact", mappedBy="userAssignedTo")
     */
    protected $contacts;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="PlApiKey", mappedBy="user")
     */
    protected $apiKeys;

    public function __toString()
    {
        return $this->firstname . ' ' . $this->lastname;
    }
    
    public function __construct() {
        $this->contacts = new ArrayCollection();
        $this->api_keys = new ArrayCollection();
    }

    /**
     * @param string $city
     *
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param \Leadsius\ApiBundle\Entity\datetime $created
     *
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return \Leadsius\ApiBundle\Entity\datetime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $firstname
     *
     * @return $this
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param boolean $isAdministrator
     *
     * @return $this
     */
    public function setIsAdministrator($isAdministrator)
    {
        $this->isAdministrator = $isAdministrator;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsAdministrator()
    {
        return $this->isAdministrator;
    }

    /**
     * @param boolean $isSuperAdministrator
     *
     * @return $this
     */
    public function setIsSuperAdministrator($isSuperAdministrator)
    {
        $this->isSuperAdministrator = $isSuperAdministrator;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsSuperAdministrator()
    {
        return $this->isSuperAdministrator;
    }

    /**
     * @param string $lastname
     *
     * @return $this
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param string $phone
     *
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $skypeId
     *
     * @return $this
     */
    public function setSkypeId($skypeId)
    {
        $this->skypeId = $skypeId;
        return $this;
    }

    /**
     * @return string
     */
    public function getSkypeId()
    {
        return $this->skypeId;
    }

    /**
     * @param string $state
     *
     * @return $this
     */
    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param \Leadsius\ApiBundle\Entity\datetime $updated
     *
     * @return $this
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
        return $this;
    }

    /**
     * @return \Leadsius\ApiBundle\Entity\datetime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set account
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $account
     * @return PlUser
     */
    public function setAccount(\Leadsius\ApiBundle\Entity\PlAccount $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Leadsius\ApiBundle\Entity\PlAccount 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Add contacts
     *
     * @param \Leadsius\ApiBundle\Entity\PlContact $contacts
     * @return PlUser
     */
    public function addContact(\Leadsius\ApiBundle\Entity\PlContact $contacts)
    {
        $this->contacts[] = $contacts;

        return $this;
    }

    /**
     * Remove contacts
     *
     * @param \Leadsius\ApiBundle\Entity\PlContact $contacts
     */
    public function removeContact(\Leadsius\ApiBundle\Entity\PlContact $contacts)
    {
        $this->contacts->removeElement($contacts);
    }

    /**
     * Get contacts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * Add api_keys
     *
     * @param PlApiKey $apiKeys
     * @return PlUser
     */
    public function addApiKey(PlApiKey $apiKeys)
    {
        $this->apiKeys[] = $apiKeys;

        return $this;
    }

    /**
     * Remove api_keys
     *
     * @param PlApiKey $apiKeys
     */
    public function removeApiKey(PlApiKey $apiKeys)
    {
        $this->api_Keys->removeElement($apiKeys);
    }

    /**
     * Get api_keys
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApiKeys()
    {
        return $this->apiKeys;
    }
}
