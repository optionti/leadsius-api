<?php

namespace Leadsius\ApiBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

class MaWorkflowTaskRepository extends EntityRepository
{
    public function findWorkflowTasks($options = null, $count = false)
    {
        $page = 1;
        $page_size = 20;
        $sort = 'id';
        $sort_dir = 'asc';
        $wheres = null;
        $workflow = null;
        $type = null;
        $sort_fields = array('id', 'type', 'condition', 'startDate', 'endDate', 'delay', 'status', 'contacts', 'created', 'updated', 'deleted', 'active', 'hasChilds');

        if (null !== $options || is_array($options)) {
            $page = isset($options['page']) && is_int($options['page']) && 0 < $options['page'] ? $options['page'] : $page;
            $page_size = isset($options['page_size']) && is_int($options['page_size']) && 0 < $options['page_size'] ? $options['page_size'] : $page_size;
            $sort = isset($options['sort']) && in_array($options['sort'], $sort_fields) ? $options['sort'] : $sort;
            $sort_dir = isset($options['sort_dir']) && in_array($options['sort_dir'], array('asc', 'desc')) ? $options['sort_dir'] : $sort_dir;
            $workflow = isset($options['workflow']) ? $options['workflow'] : $workflow;
            $type = isset($options['type']) ? $options['type'] : $type;
            $wheres = isset($options['wheres']) && is_array($options['wheres']) ? $options['wheres'] : $wheres;
        }

        $skip = $page_size * ($page - 1);

        $qb = $this->createQueryBuilder('wt');

        if (null !== $workflow) {
            $qb
                ->andWhere('wt.workflow = :workflow')
                ->setParameter('workflow', $workflow)
            ;

        }

        if (null !== $type) {
            $qb
                ->andWhere('wt.type = :type')
                ->setParameter('type', $type)
            ;

        }

        if (null !== $wheres) {
            foreach ($wheres as $item_k => $item_v) {
                $qb
                    ->andWhere('wt.' . $item_k . ' = :' . $item_k)
                    ->setParameter($item_k, $item_v)
                ;
            }
        }

        $qb
            ->andWhere('wt.deleted IS NULL')
            ->orderBy('wt.' . $sort, $sort_dir)
        ;

        if ($count) {
            return (int) $qb
                ->select('COUNT(wt)')
                ->getQuery()
                ->getSingleScalarResult()
                ;
        }

        return $qb
            ->setMaxResults($page_size)
            ->setFirstResult( $skip )
            ->getQuery()
            ->getResult()
            ;
    }
}
