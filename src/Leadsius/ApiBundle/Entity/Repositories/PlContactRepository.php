<?php

namespace Leadsius\ApiBundle\Entity\Repositories;
use Doctrine\ORM\EntityRepository;

class PlContactRepository extends EntityRepository
{
    public function findContacts($options = null, $count = false, $builder = false)
    {
        $page = 1;
        $page_size = 20;
        $sort = 'id';
        $sort_dir = 'asc';
        $query = null;
        $wheres = null;
        $joins = null;
        $sort_fields = array('id', 'firstname', 'lastname', 'email', 'created', 'updated');

        if (null !== $options || is_array($options)) {
            $page = isset($options['page']) && is_int($options['page']) && 0 < $options['page'] ? $options['page'] : $page;
            $page_size = isset($options['page_size']) && is_int($options['page_size']) && 0 < $options['page_size'] ? $options['page_size'] : $page_size;
            $sort = isset($options['sort']) && in_array($options['sort'], $sort_fields) ? $options['sort'] : $sort;
            $sort_dir = isset($options['sort_dir']) && in_array($options['sort_dir'], array('asc', 'desc')) ? $options['sort_dir'] : $sort_dir;
            $query = isset($options['query']) ? $options['query'] : $query;
            $wheres = isset($options['wheres']) && is_array($options['wheres']) ? $options['wheres'] : $wheres;
            $joins = isset($options['joins']) && is_array($options['joins']) ? $options['joins'] : $joins;
        }

        $skip = $page_size * ($page - 1);

        $qb = $this->createQueryBuilder('PlContact');

        $qb
            ->leftJoin('PlContact.dynamicFields', 'contactDynamicFields')
        ;

        if (null !== $joins) {
            foreach ($joins as $item_k => $item_v) {
                $qb
                    ->join('PlContact.' . $item_k, $item_k, 'WITH', $item_k . '.id = :' . $item_k)
                    ->setParameter($item_k, $item_v)
                ;
            }
        }

        if (null !== $query && '' !== $query) {
            $qb
                ->andWhere($qb->expr()->orX(
                    $qb->expr()->like('PlContact.firstname', ':query'),
                    $qb->expr()->like('PlContact.lastname', ':query'),
                    $qb->expr()->like('PlContact.email', ':query')
                ))
                ->setParameter('query', '%' . $query . '%')
            ;
        }

        if (null !== $wheres) {
            foreach ($wheres as $item_k => $item_v) {
                $qb
                    ->andWhere('PlContact.' . $item_k . ' = :' . $item_k)
                    ->setParameter($item_k, $item_v)
                ;
            }
        }

        $qb
            ->orderBy('PlContact.' . $sort, $sort_dir)
        ;

        if ($builder) {
            return $qb;
        }

        if ($count) {
            return (int) $qb
                    ->select('COUNT(DISTINCT PlContact)')
                    ->getQuery()
                    ->getSingleScalarResult()
                ;
        }

        return $qb
                ->select('DISTINCT PlContact')
                ->setMaxResults($page_size)
                ->setFirstResult( $skip )
                ->getQuery()
                ->getResult()
            ;
    }

    /**
     * @param array $contacts_id
     * @return array Of PlContact instances
     */
    public function findContactsWithId($contacts_id)
    {
        $qb = $this->createQueryBuilder('c');

        $qb
            ->where('c.id IN (:contacts_id)')
            ->setParameter('contacts_id', $contacts_id)
        ;

        return $qb->getQuery()->getResult();
    }

    public function findContactsWithIdAndNotInList($contacts_id, $list_id)
    {
        $qb = $this->createQueryBuilder('c');
        $qbs = $this->createQueryBuilder('cs');

        $qb
            ->where('c.id IN (:contacts_id)')
            ->andWhere($qb->expr()->notIn(
                    'c.id',
                    $qbs
                        ->select('cs.id')
                        ->join('cs.lists', 'l', 'WITH', 'l.id = :list_id')
                        ->getDQL()
                )
            )
            ->setParameter('contacts_id', $contacts_id)
            ->setParameter('list_id', $list_id)
        ;

        return $qb->getQuery()->getResult();
    } 
}
