<?php

namespace Leadsius\ApiBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

class PlApiKeyRepository extends EntityRepository
{
    public function findAllInAccount($account)
    {
        $qb = $this->createQueryBuilder('ak');

        $qb
            ->select('ak, u')
            ->join('ak.user', 'u')
            ->join('ak.account', 'a')
            ->where('ak.account = :account')
            ->setParameter('account', $account)
        ;

        return $qb->getQuery()->getResult();
    }
}
