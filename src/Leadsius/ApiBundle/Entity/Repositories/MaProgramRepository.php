<?php

namespace Leadsius\ApiBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Leadsius\ApiBundle\Entity\PlAccount;

class MaProgramRepository extends EntityRepository
{
    public function findMaProgram($options = null, $count = false)
    {
        $page = 1;
        $page_size = 20;
        $sort = 'id';
        $sort_dir = 'asc';
        $wheres = null;
        $sort_fields = array('id', 'name', 'description', 'undelete', 'created', 'updated', 'deleted');

        if (null !== $options || is_array($options)) {
            $page = isset($options['page']) && is_int($options['page']) && 0 < $options['page'] ? $options['page'] : $page;
            $page_size = isset($options['page_size']) && is_int($options['page_size']) && 0 < $options['page_size'] ? $options['page_size'] : $page_size;
            $sort = isset($options['sort']) && in_array($options['sort'], $sort_fields) ? $options['sort'] : $sort;
            $sort_dir = isset($options['sort_dir']) && in_array($options['sort_dir'], array('asc', 'desc')) ? $options['sort_dir'] : $sort_dir;
            $wheres = isset($options['wheres']) && is_array($options['wheres']) ? $options['wheres'] : $wheres;
        }

        $skip = $page_size * ($page - 1);

        $qb = $this->createQueryBuilder('c');

        if (null !== $wheres) {
            foreach ($wheres as $item_k => $item_v) {
                $qb
                    ->andWhere('c.' . $item_k . ' = :' . $item_k)
                    ->setParameter($item_k, $item_v)
                ;
            }
        }

        $qb
            ->orderBy('c.' . $sort, $sort_dir)
        ;

        if ($count) {
            return (int) $qb
                ->select('COUNT(c)')
                ->getQuery()
                ->getSingleScalarResult()
                ;
        }

        return $qb
            ->setMaxResults($page_size)
            ->setFirstResult( $skip )
            ->getQuery()
            ->getResult()
            ;
    }

    public function findFirstForAccount(PlAccount $account)
    {
        if (null === $account) {
            return null;
        }

        $qb = $this->createQueryBuilder('p');

        $qb
            ->andWhere('p.account = :account')
            ->setParameter('account', $account)
            ->setMaxResults(1)
        ;

        try {
            $program = $qb->getQuery()->getSingleResult();
        } catch (NoResultException $e) {
            $program = null;
        }

        return $program;
    }
}
