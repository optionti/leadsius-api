<?php

namespace Leadsius\ApiBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

class PlListRepository extends EntityRepository
{
    public function findLists($options = null, $count = false)
    {
        $page = 1;
        $page_size = 20;
        $sort = 'id';
        $sort_dir = 'asc';
        $query = null;
        $wheres = null;
        $sort_fields = array('id', 'name', 'description', 'type', 'populate', 'created', 'updated');

        if (null !== $options || is_array($options)) {
            $page = isset($options['page']) && is_int($options['page']) && 0 < $options['page'] ? $options['page'] : $page;
            $page_size = isset($options['page_size']) && is_int($options['page_size']) && 0 < $options['page_size'] ? $options['page_size'] : $page_size;
            $sort = isset($options['sort']) && in_array($options['sort'], $sort_fields) ? $options['sort'] : $sort;
            $sort_dir = isset($options['sort_dir']) && in_array($options['sort_dir'], array('asc', 'desc')) ? $options['sort_dir'] : $sort_dir;
            $query = isset($options['query']) ? $options['query'] : $query;
            $wheres = isset($options['wheres']) && is_array($options['wheres']) ? $options['wheres'] : $wheres;
        }

        $skip = $page_size * ($page - 1);

        $qb = $this->createQueryBuilder('l');
        
        if (null !== $query && '' !== $query) {
            $qb
                ->andWhere($qb->expr()->orX(
                    $qb->expr()->like('l.name', ':query'),
                    $qb->expr()->like('l.description', ':query'),
                    $qb->expr()->like('l.type', ':query'),
                    $qb->expr()->like('l.populate', ':query')
                ))
                ->setParameter('query', '%' . $query . '%')
            ;
        }
        
        if (null !== $wheres) {
            foreach ($wheres as $item_k => $item_v) {
                if($item_v!=''){
                    $qb
                        ->andWhere('l.' . $item_k . ' = :' . $item_k)
                        ->setParameter($item_k, $item_v)
                    ;
                }
            }
        }

        $qb
            ->orderBy('l.' . $sort, $sort_dir)
        ;

        if ($count) {
            return (int) $qb
                ->select('COUNT(l)')
                ->getQuery()
                ->getSingleScalarResult()
                ;
        }

        return $qb
            ->setMaxResults($page_size)
            ->setFirstResult( $skip )
            ->getQuery()
            ->getResult()
            ;
    }
}
