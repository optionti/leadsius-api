<?php

namespace Leadsius\ApiBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

class MaSystemEmailRepository extends EntityRepository
{
    public function findEmailAddresses($options = null, $count = false)
    {
        $page = 1;
        $page_size = 20;
        $sort = 'id';
        $sort_dir = 'asc';
        $wheres = null;
        $category = null;
        $sort_fields = array('id', 'name', 'address', 'category', 'hasEnabled', 'created', 'updated');

        if (null !== $options || is_array($options)) {
            $page = isset($options['page']) && is_int($options['page']) && 0 < $options['page'] ? $options['page'] : $page;
            $page_size = isset($options['page_size']) && is_int($options['page_size']) && 0 < $options['page_size'] ? $options['page_size'] : $page_size;
            $sort = isset($options['sort']) && in_array($options['sort'], $sort_fields) ? $options['sort'] : $sort;
            $sort_dir = isset($options['sort_dir']) && in_array($options['sort_dir'], array('asc', 'desc')) ? $options['sort_dir'] : $sort_dir;
            $category = isset($options['category']) ? $options['category'] : $category;
            $wheres = isset($options['wheres']) && is_array($options['wheres']) ? $options['wheres'] : $wheres;
        }

        $skip = $page_size * ($page - 1);

        $qb = $this->createQueryBuilder('se');

        if (null !== $category) {
            $qb
                ->andWhere('se.category = :category')
                ->setParameter('category', $category)
            ;

        }

        if (null !== $wheres) {
            foreach ($wheres as $item_k => $item_v) {
                $qb
                    ->andWhere('se.' . $item_k . ' = :' . $item_k)
                    ->setParameter($item_k, $item_v)
                ;
            }
        }

        $qb
            ->orderBy('se.' . $sort, $sort_dir)
        ;

        if ($count) {
            return (int) $qb
                ->select('COUNT(se)')
                ->getQuery()
                ->getSingleScalarResult()
                ;
        }

        return $qb
            ->setMaxResults($page_size)
            ->setFirstResult( $skip )
            ->getQuery()
            ->getResult()
            ;
    }
}
