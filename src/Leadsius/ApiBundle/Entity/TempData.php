<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="temp_data")
 * @ExclusionPolicy("all")
 */
class TempData {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_temp_data", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    protected $id;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id_import", type="integer", nullable=false)
     * @Expose
     */
    private $import;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text", nullable=true)
     * @Expose
     */
    private $value;
    /**
     * @var datetime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Expose
     */
    protected $created;

    /**
     * @var datetime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @Expose
     */
    protected $updated;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set import
     *
     * @param integer $import
     */
    public function setImport($import)
    {
        $this->import = $import;
    }

    /**
     * Get import
     *
     * @return integer 
     */
    public function getImport()
    {
        return $this->import;
    }

    /**
     * Set value
     *
     * @param text $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * Get value
     *
     * @return text 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created) {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
	public function getCreated()
    {
        return $this->created;
    }


    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated) {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
