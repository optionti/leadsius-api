<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\ObPaymentInfoRepository")
 * @ORM\Table(name="ob_payment_info")
 * @ExclusionPolicy("all")
 */
class ObPaymentInfo {
    /**
     * @var integer
     *
     * @ORM\Column(name="id_payment_info", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="payment_info_name", type="string", length=45, nullable=false)
     *
     * @Expose
     */
    private $infoName;
    /**
     * @var string
     *
     * @ORM\Column(name="payment_info_type", type="string", length=45, nullable=false)
     *
     * @Expose
     */
    private $infoType;
    /**
     * @var string
     *
     * @ORM\Column(name="payment_info_price", type="string", length=45, nullable=false)
     *
     * @Expose
     */
    private $infoPrice;
    /**
     * @var ObFeature
     *
     * @ORM\ManyToOne(targetEntity="ObFeature")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_feature", referencedColumnName="id_feature", nullable=true)
     * })
     */
    private $feature;
    /**
     * @var ObPlan
     *
     * @ORM\ManyToOne(targetEntity="ObPlan")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_plan", referencedColumnName="id_plan", nullable=true)
     * })
     */
    private $plan;
    /**
     * @var ObCurrency
     *
     * @ORM\ManyToOne(targetEntity="ObCurrency")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_currency", referencedColumnName="id_currency", nullable=true)
     * })
     */
    private $currency;
    
    /**
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param string $infoName
     */
    public function setInfoName($infoName) {
        $this->infoName = $infoName;
    }

    /**
     * @return string
     */
    public function getInfoName() {
        return $this->infoName;
    }

    /**
     * @param string $infoPrice
     */
    public function setInfoPrice($infoPrice) {
        $this->infoPrice = $infoPrice;
    }

    /**
     * @return string
     */
    public function getInfoPrice() {
        return $this->infoPrice;
    }

    /**
     * @param string $infoType
     */
    public function setInfoType($infoType) {
        $this->infoType = $infoType;
    }

    /**
     * @return string
     */
    public function getInfoType() {
        return $this->infoType;
    }


    /**
     * Set feature
     *
     * @param \Leadsius\ApiBundle\Entity\ObFeature $feature
     * @return ObPaymentInfo
     */
    public function setFeature(\Leadsius\ApiBundle\Entity\ObFeature $feature = null)
    {
        $this->feature = $feature;

        return $this;
    }

    /**
     * Get feature
     *
     * @return \Leadsius\ApiBundle\Entity\ObFeature 
     */
    public function getFeature()
    {
        return $this->feature;
    }

    /**
     * Set plan
     *
     * @param \Leadsius\ApiBundle\Entity\ObPlan $plan
     * @return ObPaymentInfo
     */
    public function setPlan(\Leadsius\ApiBundle\Entity\ObPlan $plan = null)
    {
        $this->plan = $plan;

        return $this;
    }

    /**
     * Get plan
     *
     * @return \Leadsius\ApiBundle\Entity\ObPlan 
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * Set currency
     *
     * @param \Leadsius\ApiBundle\Entity\ObCurrency $currency
     * @return ObPaymentInfo
     */
    public function setCurrency(\Leadsius\ApiBundle\Entity\ObCurrency $currency = null)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return \Leadsius\ApiBundle\Entity\ObCurrency 
     */
    public function getCurrency()
    {
        return $this->currency;
    }
}
