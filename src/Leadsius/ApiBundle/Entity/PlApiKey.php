<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * PlApiKey
 *
 * @ORM\Table(name="pl_api_key")
 * @ORM\Entity(repositoryClass="\Leadsius\PlatformBundle\Entity\Repositories\PlApiKeyRepository")
 * @UniqueEntity(fields="apiKey")
 */
class PlApiKey
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="api_key", type="string", length=64, unique=true)
     * @Assert\NotBlank
     * @Assert\Length(min=64, max=64)
     */
    private $apiKey;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=false)
     * @Assert\Type(type="bool")
     */
    private $enabled;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted", type="datetime", nullable=true)
     */
    private $deleted;

    /**
     * @var PlUser
     *
     * @ORM\ManyToOne(targetEntity="PlUser", inversedBy="apiKeys")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id_user", nullable=false)
     */
    private $user;

    /**
     * @var PlAccout
     *
     * @ORM\ManyToOne(targetEntity="PlAccount", inversedBy="apiKeys")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id_account", nullable=false)
     */
    private $account;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->apiKey = hash('sha256', mt_rand() . microtime() . uniqid(null, true));
        $this->enabled = true;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set apiKey
     *
     * @param string $apiKey
     * @return PlApiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    /**
     * Get apiKey
     *
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param boolean $enabled
     *
     * @return $this
     */
    public function setEnabled($enabled) {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getEnabled() {
        return $this->enabled;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return PlApiKey
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return PlApiKey
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $deleted
     *
     * @return $this
     */
    public function setDeleted($deleted) {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDeleted() {
        return $this->deleted;
    }

    /**
     * @param \Leadsius\PlatformBundle\Entity\PlUser $user
     *
     * @return $this
     */
    public function setUser(PlUser $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return \Leadsius\PlatformBundle\Entity\PlUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param \Leadsius\PlatformBundle\Entity\PlAccount $account
     *
     * @return $this
     */
    public function setAccount(PlAccount $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * @return \Leadsius\PlatformBundle\Entity\PlAccount
     */
    public function getAccount()
    {
        return $this->account;
    }

    public function generateApiKey()
    {
        return hash('sha256', mt_rand() . microtime() . uniqid(null, true));
    }

    public function regenerateApiKey()
    {
        $this->apiKey = $this->generateApiKey();
    }

    public function toggleEnabled()
    {
        $this->enabled = !$this->enabled;
    }
}
