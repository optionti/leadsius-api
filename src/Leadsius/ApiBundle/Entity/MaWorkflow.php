<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\MaWorkflowRepository")
 * @ORM\Table(name="ma_workflow")
 * @Assert\Callback(methods={"validParamsForType"})
 * @ExclusionPolicy("all")
 */
class MaWorkflow
{
    const TYPE_START_WITH_TASK = 'startWithTask';
    const TYPE_START_WITH_CONDITION = 'startWithCondition';

    const STATUS_PENDING = 'pending';

    /**
     * @var integer 
     *
     * @ORM\Column(name="id_workflow", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;

    /**
     * @var string 
     *
     * @ORM\Column(name="workflow_name", type="string", length=45, nullable=false)
     * @Assert\NotBlank(message="Workflow name can't be null")
     *
     * @Expose
     */
    private $name;

    /**
     * @var text 
     *
     * @ORM\Column(name="workflow_description", type="text", nullable=true)
     * @Expose
     */
    private $description;

    /**
     * @var string 
     *
     * @ORM\Column(name="workflow_start_date", type="datetime", nullable=true)
     * @Assert\DateTime
     *
     * @Expose
     */
    private $startDate;

    /**
     * @var string 
     *
     * @ORM\Column(name="workflow_end_date", type="datetime", nullable=true)
     * @Assert\DateTime
     *
     * @Expose
     */
    private $endDate;

    /**
     * @var text 
     *
     * @ORM\Column(name="workflow_type", type="text", nullable=false)
     * @Assert\Regex("/^(startWith(Task|Condition))$/")
     *
     */
    private $type;

    /**
     * @var string 
     *
     * @ORM\Column(name="workflow_status", type="string", length=255, nullable=false)
     * @Assert\Regex("/^(pending)$/")
     *
     */
    private $status;
    
    /**
     * @var datetime $created
     *
     * @Gedmo\Timestampable(on="create")     *
     * @ORM\Column(name="created", type="datetime", nullable=false)     *
     * @JMS\Expose
     * @JMS\Groups({"list"})
     */
    protected $created;

    /**
     * @var datetime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime", nullable=false)     *
     * @JMS\Expose
     * @JMS\Groups({"list"})
     */
    protected $updated;

    /**
     * @var datetime $deleted
     *
     * @ORM\Column(name="deleted", type="datetime", nullable=true)     *
     * @JMS\Expose
     * @JMS\Groups({"list"})
     */
    protected $deleted;

    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_account", referencedColumnName="id_account", nullable=false)
     * })
     */
    protected $account;

    /**
     * @var PlUser
     *
     * @ORM\ManyToOne(targetEntity="PlUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id_user", nullable=true)
     * })
     */
    private $user;

    /**
     * @var MaProgram
     *
     * @ORM\ManyToOne(targetEntity="MaProgram", inversedBy="workflow")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_program", referencedColumnName="id_program", nullable=true)
     * })
     */
    protected $program;

    /**
     * @var PlList
     *
     * @ORM\ManyToOne(targetEntity="PlList")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_list", referencedColumnName="id_list", nullable=true)
     * })
     */
    protected $list;

    /**
     * @var PlPicklistOption
     *
     * @ORM\ManyToOne(targetEntity="PlPicklistOption")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="workflow_category", referencedColumnName="id_picklist_option", nullable=true)
     * })
     */
    private $category;

    public function __construct() {
        $this->status = self::STATUS_PENDING;
    }

    /**
     * Validate if entity params are valid for workflow type
     */
    public function validParamsForType(ExecutionContextInterface $context)
    {
        if (self::TYPE_START_WITH_CONDITION === $this->getType()
            && (null === $this->getStartDate() || null === $this->getEndDate())) {
            $context->addViolationAt('startDate', 'Start date not defined', array(), null);
            $context->addViolationAt('endDate', 'End date not defined', array(), null);
        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param text $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return text 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set startDate
     *
     * @param datetime $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * Get startDate
     *
     * @return datetime 
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param datetime $endDate
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }

    /**
     * Get endDate
     *
     * @return datetime 
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set type
     *
     * @param text $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get type
     *
     * @return text 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set deleted
     *
     * @param datetime $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * Get deleted
     *
     * @return datetime 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set status
     *
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set account
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $account
     * @return MaWorkflow
     */
    public function setAccount(\Leadsius\ApiBundle\Entity\PlAccount $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Leadsius\ApiBundle\Entity\PlAccount 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set user
     *
     * @param \Leadsius\ApiBundle\Entity\PlUser $user
     * @return MaWorkflow
     */
    public function setUser(\Leadsius\ApiBundle\Entity\PlUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Leadsius\ApiBundle\Entity\PlUser 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set program
     *
     * @param \Leadsius\ApiBundle\Entity\MaProgram $program
     * @return MaWorkflow
     */
    public function setProgram(\Leadsius\ApiBundle\Entity\MaProgram $program = null)
    {
        $this->program = $program;

        return $this;
    }

    /**
     * Get program
     *
     * @return \Leadsius\ApiBundle\Entity\MaProgram 
     */
    public function getProgram()
    {
        return $this->program;
    }

    /**
     * Set list
     *
     * @param \Leadsius\ApiBundle\Entity\PlList $list
     * @return MaWorkflow
     */
    public function setList(\Leadsius\ApiBundle\Entity\PlList $list = null)
    {
        $this->list = $list;

        return $this;
    }

    /**
     * Get list
     *
     * @return \Leadsius\ApiBundle\Entity\PlList 
     */
    public function getList()
    {
        return $this->list;
    }

    /**
     * Set category
     *
     * @param \Leadsius\ApiBundle\Entity\PlPicklistOption $category
     * @return MaWorkflow
     */
    public function setCategory(\Leadsius\ApiBundle\Entity\PlPicklistOption $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Leadsius\ApiBundle\Entity\PlPicklistOption 
     */
    public function getCategory()
    {
        return $this->category;
    }
}
