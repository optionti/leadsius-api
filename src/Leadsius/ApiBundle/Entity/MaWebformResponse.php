<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\MaWebformResponseRepository")
 * @ORM\Table(name="ma_webform_response")
 * @ExclusionPolicy("all")
 */
class MaWebformResponse {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_webform_response", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;

    /**
     * @var text
     *
     * @ORM\Column(name="webform_response", type="text", nullable=false)
     *
     * @Expose
     */
    private $webformResponse;

    /**
     * @var datetime $created
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     *
     * @Expose
     * @Gedmo\Timestampable(on="create")
     */
    protected $created;

    /**
     * @var MaWebform
     *
     * @ORM\ManyToOne(targetEntity="MaWebform")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_webform", referencedColumnName="id_webform", nullable=false)
     * })
     */
    protected $webform;

    /**
     * @var MaLandingPage
     *
     * @ORM\ManyToOne(targetEntity="MaLandingPage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_landing_page", referencedColumnName="id_landing_page", nullable=true)
     * })
     */
    protected $landingPage;

    /**
     * @var MaProgram
     *
     * @ORM\ManyToOne(targetEntity="MaProgram")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_program", referencedColumnName="id_program", nullable=false)
     * })
     */
    protected $program;

    /**
     * @var PlContact
     *
     * @ORM\ManyToOne(targetEntity="PlContact")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contact", referencedColumnName="id_contact", nullable=true)
     * })
     */
    protected $contact;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set webformResponse
     *
     * @param string $webformResponse
     */
    public function setWebformResponse($webformResponse) {
        $this->webformResponse = $webformResponse;
    }

    /**
     * Get webformResponse
     *
     * @return string 
     */
    public function getWebformResponse() {
        return $this->webformResponse;
    }

    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created) {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
	public function getCreated()
    {
        return $this->created;
    }


    /**
     * Set webform
     *
     * @param \Leadsius\ApiBundle\Entity\MaWebform $webform
     * @return MaWebformResponse
     */
    public function setWebform(\Leadsius\ApiBundle\Entity\MaWebform $webform)
    {
        $this->webform = $webform;

        return $this;
    }

    /**
     * Get webform
     *
     * @return \Leadsius\ApiBundle\Entity\MaWebform 
     */
    public function getWebform()
    {
        return $this->webform;
    }

    /**
     * Set landingPage
     *
     * @param \Leadsius\ApiBundle\Entity\MaLandingPage $landingPage
     * @return MaWebformResponse
     */
    public function setLandingPage(\Leadsius\ApiBundle\Entity\MaLandingPage $landingPage = null)
    {
        $this->landingPage = $landingPage;

        return $this;
    }

    /**
     * Get landingPage
     *
     * @return \Leadsius\ApiBundle\Entity\MaLandingPage 
     */
    public function getLandingPage()
    {
        return $this->landingPage;
    }

    /**
     * Set program
     *
     * @param \Leadsius\ApiBundle\Entity\MaProgram $program
     * @return MaWebformResponse
     */
    public function setProgram(\Leadsius\ApiBundle\Entity\MaProgram $program)
    {
        $this->program = $program;

        return $this;
    }

    /**
     * Get program
     *
     * @return \Leadsius\ApiBundle\Entity\MaProgram 
     */
    public function getProgram()
    {
        return $this->program;
    }

    /**
     * Set contact
     *
     * @param \Leadsius\ApiBundle\Entity\PlContact $contact
     * @return MaWebformResponse
     */
    public function setContact(\Leadsius\ApiBundle\Entity\PlContact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return \Leadsius\ApiBundle\Entity\PlContact 
     */
    public function getContact()
    {
        return $this->contact;
    }
}
