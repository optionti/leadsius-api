<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\MaSystemEmailRepository")
 * @ORM\Table(name="ma_system_email")
 * @ExclusionPolicy("all")
 */

class MaSystemEmail
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_system", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="system_email_name", type="string", length=255, nullable=true)
     *
     * @Expose
     */
    private $name;

    /**
     * @var text
     * 
     * @ORM\Column(name="system_email_address", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="Email address can't be null")
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email."
     * )
     * @Expose
     */
    private $address;

    /**
     * @var string
     * 
     * @ORM\Column(name="system_email_category", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="Category can't be null")
     * @Assert\Choice(choices = {"sender", "bounce", "reply"}, message = "Category should be sender, bounce or reply")
     * @Expose
     */
    private $category;
    
    /**
     * @var string
     * @ORM\Column(name="system_email_has_enabled", type="boolean", nullable=true)
     *
     */
    private $hasEnabled;
    
   /**
     * @var datetime $created
    *
     * @Gedmo\Timestampable(on="create")     *
     * @ORM\Column(name="created", type="datetime", nullable=false)     *
     * @JMS\Expose
     * @JMS\Groups({"list"})
     */
    protected $created;

    /**
     * @var datetime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime", nullable=false)     *
     * @JMS\Expose
     * @JMS\Groups({"list"})
     */
    protected $updated;
    
    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_account", referencedColumnName="id_account", nullable=false)
     * })
     */
    protected $account;

    public function __toString() {
        return (string) $this->name;
    }

    public function __construct() {
        $this->hasEnabled = true;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set category
     *
     * @param string $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * Get category
     *
     * @return string 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created) {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
	public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated) {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set hasEnabled
     *
     * @param boolean $hasEnabled
     */
    public function setHasEnabled($hasEnabled)
    {
        $this->hasEnabled = $hasEnabled;
    }

    /**
     * Get hasEnabled
     *
     * @return boolean 
     */
    public function getHasEnabled()
    {
        return $this->hasEnabled;
    }

    /**
     * Set account
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $account
     * @return MaSystemEmail
     */
    public function setAccount(\Leadsius\ApiBundle\Entity\PlAccount $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Leadsius\ApiBundle\Entity\PlAccount 
     */
    public function getAccount()
    {
        return $this->account;
    }
}
