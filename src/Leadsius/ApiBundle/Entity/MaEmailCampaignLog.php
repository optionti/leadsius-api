<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\MaEmailCampaignLogRepository")
 * @ORM\Table(name="ma_email_campaign_log")
 * @ExclusionPolicy("all")
 */
class MaEmailCampaignLog
{
    /**
     * @var integer 
     *
     * @ORM\Column(name="id_email_campaign_log", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;
    
    /**
     * @var string 
     *
     * @ORM\Column(name="activity_type", type="string", length=255, nullable=false)
     *
     * @Expose
     */
    private $activityType;

    /**
     * @var string 
     *
     * @ORM\Column(name="activity_detail", type="string", length=255, nullable=true)
     *
     * @Expose
     */
    private $activityDetail;

   /**
     * @var datetime $created
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     *
     * @Expose
     */
    protected $created;

    /**
     * @var datetime $updated
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     *
     * @Expose
     */
    protected $updated;

    /**
     * @var PlContact
     *
     * @ORM\ManyToOne(targetEntity="PlContact", inversedBy="emailCampaignLogs")
     * @ORM\JoinColumn(name="id_contact", referencedColumnName="id_contact", nullable=true)
     */
    protected $contact;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set activityType
     *
     * @param string $activityType
     */
    public function setActivityType($activityType)
    {
        $this->activityType = $activityType;
    }

    /**
     * Get activityType
     *
     * @return string 
     */
    public function getActivityType()
    {
        return $this->activityType;
    }

    /**
     * Set activityDetail
     *
     * @param string $activityDetail
     */
    public function setActivityDetail($activityDetail)
    {
        $this->activityDetail = $activityDetail;
    }

    /**
     * Get activityDetail
     *
     * @return string 
     */
    public function getActivityDetail()
    {
        return $this->activityDetail;
    }

    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created) {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
	public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated) {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }


    /**
     * Set contact
     *
     * @param \Leadsius\ApiBundle\Entity\PlContact $contact
     * @return MaEmailCampaignLog
     */
    public function setContact(\Leadsius\ApiBundle\Entity\PlContact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return \Leadsius\ApiBundle\Entity\PlContact 
     */
    public function getContact()
    {
        return $this->contact;
    }
}
