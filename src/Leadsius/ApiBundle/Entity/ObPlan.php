<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\ObPlanRepository")
 * @ORM\Table(name="ob_plan")
 * @ExclusionPolicy("all")
 */
class ObPlan {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_plan", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="plan_name", type="string", length=45, nullable=false)
     *
     * @Expose
     */
    private $name;
    /**
     * @var integer
     *
     * @ORM\Column(name="plan_users", type="integer", nullable=false)
     *
     * @Expose
     */
    private $users;
    /**
     * @var integer
     *
     * @ORM\Column(name="plan_contacts", type="integer", nullable=false)
     *
     * @Expose
     */
    private $contacts;
    /**
     * @var boolean
     *
     * @ORM\Column(name="plan_billable", type="boolean", nullable=false)
     *
     * @Expose
     */
    private $billable = false;
    /**
     * @var boolean
     *
     * @ORM\Column(name="plan_active", type="boolean", nullable=false)
     *
     * @Expose
     */
    private $active = false;
    /**
     * @var string
     *
     * @ORM\Column(name="plan_admin_email", type="string", length=45, nullable=false)
     *
     * @Expose
     */
    private $adminEmail;
    /**
     * @var string
     *
     * @ORM\Column(name="plan_user_email", type="string", length=45, nullable=false)
     *
     * @Expose
     */
    private $userEmail;
    /**
     * @var string
     *
     * @ORM\Column(name="plan_view", type="text", nullable=false)
     *
     * @Expose
     */
    private $view;
    /**
     * @var planFeatures
     *
     * @ORM\ManyToMany(targetEntity="ObFeature", inversedBy="planes", cascade={"persist"})
     * @ORM\JoinTable(name="ob_plan_has_feature",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_plan", referencedColumnName="id_plan")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_feature", referencedColumnName="id_feature")
     *   }
     * )
     */
    private $features;
    
    /**
     * Constructor
     */
    public function __construct() {
        $this->planFeatures = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ObPlan
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $adminEmail
     */
    public function setAdminEmail($adminEmail) {
        $this->adminEmail = $adminEmail;
    }

    /**
     * @return string
     */
    public function getAdminEmail() {
        return $this->adminEmail;
    }

    /**
     * @param string $userEmail
     */
    public function setUserEmail($userEmail) {
        $this->userEmail = $userEmail;
    }

    /**
     * @return string
     */
    public function getUserEmail() {
        return $this->userEmail;
    }

    /**
     * @param string $view
     */
    public function setView($view) {
        $this->view = $view;
    }

    /**
     * @return string
     */
    public function getView() {
        return $this->view;
    }

    /**
     * Set users
     *
     * @param integer $users
     * @return ObPlan
     */
    public function setUsers($users) {
        $this->users = $users;

        return $this;
    }

    /**
     * Get users
     *
     * @return integer
     */
    public function getUsers() {
        return $this->users;
    }

    /**
     * Set contacts
     *
     * @param integer $contacts
     * @return ObPlan
     */
    public function setContacts($contacts) {
        $this->contacts = $contacts;

        return $this;
    }

    /**
     * Get contacts
     *
     * @return integer
     */
    public function getContacts() {
        return $this->contacts;
    }

    /**
     * @param boolean $billable
     */
    public function setBillable($billable) {
        $this->billable = $billable;
    }

    /**
     * @return boolean
     */
    public function getBillable() {
        return $this->billable;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return ObPlan
     */
    public function setActive($active) {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive() {
        return $this->active;
    }


    /**
     * Add features
     *
     * @param \Leadsius\ApiBundle\Entity\ObFeature $features
     * @return ObPlan
     */
    public function addFeature(\Leadsius\ApiBundle\Entity\ObFeature $features)
    {
        $this->features[] = $features;

        return $this;
    }

    /**
     * Remove features
     *
     * @param \Leadsius\ApiBundle\Entity\ObFeature $features
     */
    public function removeFeature(\Leadsius\ApiBundle\Entity\ObFeature $features)
    {
        $this->features->removeElement($features);
    }

    /**
     * Get features
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFeatures()
    {
        return $this->features;
    }
}
