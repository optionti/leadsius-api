<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;
/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\PlImportRepository")
 * @ORM\Table(name="pl_import")
 * @ExclusionPolicy("all")
 */
class PlImport {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_import", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var PlUser
     *
     * @ORM\ManyToOne(targetEntity="PlUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id_user", nullable=true)
     * })
     */
    private $user;

    /**
     * @var PlList
     *
     * @ORM\ManyToOne(targetEntity="PlList")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_list", referencedColumnName="id_list", nullable=true)
     * })
     */
    private $list;

    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_account", referencedColumnName="id_account", nullable=false)
     * })
     */
    protected $account;

    /**
     * @var string
     *
     * @ORM\Column(name="file_name", type="string", length=255, nullable=true)
     * @Expose
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="file_extension", type="string", length=45, nullable=true)
     * @Expose
     */
    private $extension;

    /**
     * @var integer
     *
     * @ORM\Column(name="file_size", type="integer", nullable=true)
     * @Expose
     */
    private $size;

    /**
     * @var string
     *
     * @ORM\Column(name="file_delimeter", type="string", length=45, nullable=true)
     * @Expose
     */
    private $delimeter;

    /**
     * @var boolean
     *
     * @ORM\Column(name="file_header", type="boolean", nullable=true)
     * @Expose
     */
    private $header;

    /**
     * @var string
     *
     * @ORM\Column(name="mapping", type="text", nullable=true)
     * @Expose
     */
    private $mapping;

    /**
     * @var string
     *
     * @ORM\Column(name="overwrite_rules", type="string", length=255, nullable=true)
     * @Expose
     */
    private $overwriteRules;

    /**
     * @var string
     *
     * @ORM\Column(name="file_status", type="string", length=255, nullable=true)
     * @Expose
     */
    private $status;

    /**
     * @var datetime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Expose
     * @Gedmo\Timestampable(on="create")
     */
    protected $created;

    /**
     * @var datetime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @Expose
     * @Gedmo\Timestampable(on="update")
     */
    protected $updated;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set extension
     *
     * @param string $extension
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;
    }

    /**
     * Get extension
     *
     * @return string 
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * Set size
     *
     * @param integer $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * Get size
     *
     * @return integer 
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set delimeter
     *
     * @param string $delimeter
     */
    public function setDelimeter($delimeter)
    {
        $this->delimeter = $delimeter;
    }

    /**
     * Get delimeter
     *
     * @return string 
     */
    public function getDelimeter()
    {
        return $this->delimeter;
    }

    /**
     * Set header
     *
     * @param boolean $header
     */
    public function setHeader($header)
    {
        $this->header = $header;
    }

    /**
     * Get header
     *
     * @return boolean 
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * Set mapping
     *
     * @param text $mapping
     */
    public function setMapping($mapping)
    {
        $this->mapping = $mapping;
    }

    /**
     * Get mapping
     *
     * @return text 
     */
    public function getMapping()
    {
        return $this->mapping;
    }

    /**
     * Set overwriteRules
     *
     * @param string $overwriteRules
     */
    public function setOverwriteRules($overwriteRules)
    {
        $this->overwriteRules = $overwriteRules;
    }

    /**
     * Get overwriteRules
     *
     * @return string 
     */
    public function getOverwriteRules()
    {
        return $this->overwriteRules;
    }

    /**
     * Set status
     *
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created) {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
	public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated) {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }
    

    /**
     * Set user
     *
     * @param \Leadsius\ApiBundle\Entity\PlUser $user
     * @return PlImport
     */
    public function setUser(\Leadsius\ApiBundle\Entity\PlUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Leadsius\ApiBundle\Entity\PlUser 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set list
     *
     * @param \Leadsius\ApiBundle\Entity\PlList $list
     * @return PlImport
     */
    public function setList(\Leadsius\ApiBundle\Entity\PlList $list = null)
    {
        $this->list = $list;

        return $this;
    }

    /**
     * Get list
     *
     * @return \Leadsius\ApiBundle\Entity\PlList 
     */
    public function getList()
    {
        return $this->list;
    }

    /**
     * Set account
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $account
     * @return PlImport
     */
    public function setAccount(\Leadsius\ApiBundle\Entity\PlAccount $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Leadsius\ApiBundle\Entity\PlAccount 
     */
    public function getAccount()
    {
        return $this->account;
    }
}
