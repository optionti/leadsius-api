<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\MaCronjobLogRepository")
 * @ORM\Table(name="ma_cronjob_log")
 * @ExclusionPolicy("all")
 */
class MaCronjobLog {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_cronjob_log", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cronjob_status", type="string", length=255, nullable=false)
     *
     * @Expose
     */
    private $status;

    /**
     * @var text
     *
     * @ORM\Column(name="cronjob_error", type="text", nullable=true)
     *
     * @Expose
     */
    private $error;

    /**
     * @var text
     *
     * @ORM\Column(name="cronjob_type", type="string", length=255, nullable=false)
     *
     * @Expose
     */
    private $type;

    /**
     * @var text
     *
     * @ORM\Column(name="cronjob_alert_sent", type="boolean", nullable=false)
     *
     * @Expose
     */
    private $alertSent;

    /**
     * @var datetime $created
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     *
     * @Expose
     */
    protected $created;

    /**
     * @var datetime $updated
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     *
     * @Expose
     */
    protected $updated;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set error
     *
     * @param text $error
     */
    public function setError($error)
    {
        $this->error = $error;
    }

    /**
     * Get error
     *
     * @return text 
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set type
     *
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set alertSent
     *
     * @param boolean $alertSent
     */
    public function setAlertSent($alertSent)
    {
        $this->alertSent = $alertSent;
    }

    /**
     * Get alertSent
     *
     * @return boolean 
     */
    public function getAlertSent()
    {
        return $this->alertSent;
    }
}
