<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\PlFieldRepository")
 * @ORM\Table(name="pl_field")
 * @ExclusionPolicy("all")
 */
class PlField
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_field", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="field_name", type="string", length=45, nullable=true)
     * @Expose
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="field_type", type="string", length=45, nullable=true)
     * @Expose
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="field_category", type="string", length=45, nullable=true)
     * @Expose
     */
    private $category;

    /**
     * @var boolean
     *
     * @ORM\Column(name="field_visible", type="boolean", nullable=true)
     * @Expose
     */
    private $visible;

    /**
     * @var boolean
     *
     * @ORM\Column(name="field_required", type="boolean", length=45, nullable=true)
     * @Expose
     */
    private $required;

    /**
     * @var string
     *
     * @ORM\Column(name="field_type_html5", type="string", length=45, nullable=true)
     * @Expose
     */
    private $typeHtml5;

    /**
     * @var string
     *
     * @ORM\Column(name="field_html5_pattern", type="string", length=45, nullable=true)
     * @Expose
     */
    private $html5Pattern;

    /**
     * @var string
     *
     * @ORM\Column(name="field_description", type="string", length=45, nullable=true)
     * @Expose
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Expose
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @Expose
     */
    private $updated;


    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="PlDynamicFields", mappedBy="field")
     */
    private $dynamicFields;

    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount", inversedBy="fields")
     * @ORM\JoinColumn(name="id_account", referencedColumnName="id_account", nullable=false)
     */
    protected $account;

    /**
     * @ORM\OneToMany(targetEntity="PlFieldValue", mappedBy="idField")
     */
    protected $fieldValue;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set category
     *
     * @param string $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * Get category
     *
     * @return string 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
    }

    /**
     * Get visible
     *
     * @return boolean 
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set required
     *
     * @param boolean $required
     */
    public function setRequired($required)
    {
        $this->required = $required;
    }

    /**
     * Get required
     *
     * @return boolean 
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * Set typeHtml5
     *
     * @param string $typeHtml5
     */
    public function setTypeHtml5($typeHtml5)
    {
        $this->typeHtml5 = $typeHtml5;
    }

    /**
     * Get typeHtml5
     *
     * @return string 
     */
    public function getTypeHtml5()
    {
        return $this->typeHtml5;
    }

    /**
     * Set html5Pattern
     *
     * @param string $html5Pattern
     */
    public function setHtml5Pattern($html5Pattern)
    {
        $this->html5Pattern = $html5Pattern;
    }

    /**
     * Get html5Pattern
     *
     * @return string 
     */
    public function getHtml5Pattern()
    {
        return $this->html5Pattern;
    }

    /**
     * Set description
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fieldValue = new ArrayCollection();
        $this->dynamicFields = new ArrayCollection();
    }

    /**
     * Set account
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $account
     * @return PlField
     */
    public function setAccount(\Leadsius\ApiBundle\Entity\PlAccount $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Leadsius\ApiBundle\Entity\PlAccount 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Add fieldValue
     *
     * @param \Leadsius\ApiBundle\Entity\PlFieldValue $fieldValue
     * @return PlField
     */
    public function addFieldValue(\Leadsius\ApiBundle\Entity\PlFieldValue $fieldValue)
    {
        $this->fieldValue[] = $fieldValue;

        return $this;
    }

    /**
     * Remove fieldValue
     *
     * @param \Leadsius\ApiBundle\Entity\PlFieldValue $fieldValue
     */
    public function removeFieldValue(\Leadsius\ApiBundle\Entity\PlFieldValue $fieldValue)
    {
        $this->fieldValue->removeElement($fieldValue);
    }

    /**
     * Get fieldValue
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFieldValue()
    {
        return $this->fieldValue;
    }

    /**
     * Add dynamicFields
     *
     * @param \Leadsius\ApiBundle\Entity\PlDynamicFields $dynamicFields
     * @return PlField
     */
    public function addDynamicField(\Leadsius\ApiBundle\Entity\PlDynamicFields $dynamicFields)
    {
        $this->dynamicFields[] = $dynamicFields;

        return $this;
    }

    /**
     * Remove dynamicFields
     *
     * @param \Leadsius\ApiBundle\Entity\PlDynamicFields $dynamicFields
     */
    public function removeDynamicField(\Leadsius\ApiBundle\Entity\PlDynamicFields $dynamicFields)
    {
        $this->dynamicFields->removeElement($dynamicFields);
    }

    /**
     * Get dynamicFields
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDynamicFields()
    {
        return $this->dynamicFields;
    }
}
