<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\MaProgramRepository")
 * @ORM\Table(name="ma_program")
 * @ExclusionPolicy("all")
 */
class MaProgram {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_program", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="program_name", type="string", length=45, nullable=false)
     *
     * @Expose
     */
    private $name;

    /**
     * @var text
     *
     * @ORM\Column(name="program_description", type="text", nullable=true)
     *
     * @Expose
     */
    private $description;
    
    /**
     * @var text
     *
     * @ORM\Column(name="program_undelete", type="boolean", nullable=true)
     *
     * @Expose
     */
    private $undelete;
    
    /**
     * @var datetime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     *
     * @Expose
     */
    protected $created;

    /**
     * @var datetime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     *
     * @Expose
     */
    protected $updated;

    /**
     * @var datetime
     *
     * @ORM\Column(name="deleted", type="datetime", nullable=true)
     *
     * @Expose
     */
    protected $deleted;
    
    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_account", referencedColumnName="id_account", nullable=false)
     * })
     */
    protected $account;

    /**
     * @var PlUser
     *
     * @ORM\ManyToOne(targetEntity="PlUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id_user", nullable=true)
     * })
     */    
    private $user;

    /**
     * @var PlPicklistOption
     *
     * @ORM\ManyToOne(targetEntity="PlPicklistOption", inversedBy="programs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="program_category", referencedColumnName="id_picklist_option", nullable=true)
     * })
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="MaLandingPage", mappedBy="program")
     */
    protected $landingPage;

    /**
     * @ORM\OneToMany(targetEntity="MaWebform", mappedBy="program")
     */
    protected $webform;

    /**
     * @ORM\OneToMany(targetEntity="MaEmail", mappedBy="program")
     */
    protected $email;
    
    /**
     * @ORM\OneToMany(targetEntity="MaWorkflow", mappedBy="program")
     */
    protected $workflow;
    
    /**
     * @ORM\OneToMany(targetEntity="MaEmailCampaign", mappedBy="program")
     */
    protected $emailCampaign;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     */
    public function setDescription($description) {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }
    
     /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created) {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
	public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated) {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set deleted
     *
     * @param datetime $deleted
     */
    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }
	
    /**
     * Get deleted
     *
     * @return datetime 
     */
    public function getDeleted() {
        return $this->deleted;
    }

    /**
     * Set undelete
     *
     * @param boolean $undelete
     */
    public function setUndelete($undelete)
    {
        $this->undelete = $undelete;
    }

    /**
     * Get undelete
     *
     * @return boolean 
     */
    public function getUndelete()
    {
        return $this->undelete;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->landingPage = new \Doctrine\Common\Collections\ArrayCollection();
        $this->webform = new \Doctrine\Common\Collections\ArrayCollection();
        $this->email = new \Doctrine\Common\Collections\ArrayCollection();
        $this->workflow = new \Doctrine\Common\Collections\ArrayCollection();
        $this->emailCampaign = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set account
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $account
     * @return MaProgram
     */
    public function setAccount(\Leadsius\ApiBundle\Entity\PlAccount $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Leadsius\ApiBundle\Entity\PlAccount 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set user
     *
     * @param \Leadsius\ApiBundle\Entity\PlUser $user
     * @return MaProgram
     */
    public function setUser(\Leadsius\ApiBundle\Entity\PlUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Leadsius\ApiBundle\Entity\PlUser 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set category
     *
     * @param \Leadsius\ApiBundle\Entity\PlPicklistOption $category
     * @return MaProgram
     */
    public function setCategory(\Leadsius\ApiBundle\Entity\PlPicklistOption $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Leadsius\ApiBundle\Entity\PlPicklistOption 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add landingPage
     *
     * @param \Leadsius\ApiBundle\Entity\MaLandingPage $landingPage
     * @return MaProgram
     */
    public function addLandingPage(\Leadsius\ApiBundle\Entity\MaLandingPage $landingPage)
    {
        $this->landingPage[] = $landingPage;

        return $this;
    }

    /**
     * Remove landingPage
     *
     * @param \Leadsius\ApiBundle\Entity\MaLandingPage $landingPage
     */
    public function removeLandingPage(\Leadsius\ApiBundle\Entity\MaLandingPage $landingPage)
    {
        $this->landingPage->removeElement($landingPage);
    }

    /**
     * Get landingPage
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLandingPage()
    {
        return $this->landingPage;
    }

    /**
     * Add webform
     *
     * @param \Leadsius\ApiBundle\Entity\MaWebform $webform
     * @return MaProgram
     */
    public function addWebform(\Leadsius\ApiBundle\Entity\MaWebform $webform)
    {
        $this->webform[] = $webform;

        return $this;
    }

    /**
     * Remove webform
     *
     * @param \Leadsius\ApiBundle\Entity\MaWebform $webform
     */
    public function removeWebform(\Leadsius\ApiBundle\Entity\MaWebform $webform)
    {
        $this->webform->removeElement($webform);
    }

    /**
     * Get webform
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWebform()
    {
        return $this->webform;
    }

    /**
     * Add email
     *
     * @param \Leadsius\ApiBundle\Entity\MaEmail $email
     * @return MaProgram
     */
    public function addEmail(\Leadsius\ApiBundle\Entity\MaEmail $email)
    {
        $this->email[] = $email;

        return $this;
    }

    /**
     * Remove email
     *
     * @param \Leadsius\ApiBundle\Entity\MaEmail $email
     */
    public function removeEmail(\Leadsius\ApiBundle\Entity\MaEmail $email)
    {
        $this->email->removeElement($email);
    }

    /**
     * Get email
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Add workflow
     *
     * @param \Leadsius\ApiBundle\Entity\MaWorkflow $workflow
     * @return MaProgram
     */
    public function addWorkflow(\Leadsius\ApiBundle\Entity\MaWorkflow $workflow)
    {
        $this->workflow[] = $workflow;

        return $this;
    }

    /**
     * Remove workflow
     *
     * @param \Leadsius\ApiBundle\Entity\MaWorkflow $workflow
     */
    public function removeWorkflow(\Leadsius\ApiBundle\Entity\MaWorkflow $workflow)
    {
        $this->workflow->removeElement($workflow);
    }

    /**
     * Get workflow
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWorkflow()
    {
        return $this->workflow;
    }

    /**
     * Add emailCampaign
     *
     * @param \Leadsius\ApiBundle\Entity\MaEmailCampaign $emailCampaign
     * @return MaProgram
     */
    public function addEmailCampaign(\Leadsius\ApiBundle\Entity\MaEmailCampaign $emailCampaign)
    {
        $this->emailCampaign[] = $emailCampaign;

        return $this;
    }

    /**
     * Remove emailCampaign
     *
     * @param \Leadsius\ApiBundle\Entity\MaEmailCampaign $emailCampaign
     */
    public function removeEmailCampaign(\Leadsius\ApiBundle\Entity\MaEmailCampaign $emailCampaign)
    {
        $this->emailCampaign->removeElement($emailCampaign);
    }

    /**
     * Get emailCampaign
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEmailCampaign()
    {
        return $this->emailCampaign;
    }
}
