<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\PlActionLogRepository")
 * @ORM\Table(name="pl_action_log")
 * @ExclusionPolicy("all")
 */
class PlActionLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_action_log", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var datetime
     *
     * @ORM\Column(name="log_date", type="datetime", nullable=true)
     * @Expose
     */
    private $date;

    /**
     * @var text
     *
     * @ORM\Column(name="log_description", type="text", nullable=true)
     * @Expose
     */
    private $description;

    /**
     * @var text
     *
     * @ORM\Column(name="log_type", type="text", nullable=true)
     * @Expose
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="log_system", type="string", length=100, nullable=true)
     * @Expose
     */
    private $system;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="showed", type="boolean", nullable=false)
     * @Expose
     */
    private $showed;
    /**
     * @var boolean
     *
     * @ORM\Column(name="log_global", type="boolean", nullable=true)
     * @Expose
     */
    private $global;

   /**
     * @var datetime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Expose
     */
    protected $created;

    /**
     * @var datetime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @Expose
     */
    protected $updated;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param datetime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * Get date
     *
     * @return datetime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set description
     *
     * @param text $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return text 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set type
     *
     * @param text $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get type
     *
     * @return text 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set system
     *
     * @param string $system
     */
    public function setSystem($system)
    {
        $this->system = $system;
    }

    /**
     * Get system
     *
     * @return string 
     */
    public function getSystem()
    {
        return $this->system;
    }

    /**
     * Set showed
     *
     * @param boolean $showed
     */
    public function setShowed($showed)
    {
        $this->showed = $showed;
    }

    /**
     * Get showed
     *
     * @return boolean 
     */
    public function getShowed()
    {
        return $this->showed;
    }
    /**
     * Set global
     *
     * @param boolean $global
     */
    public function setGlobal($global)
    {
        $this->global = $global;
    }

    /**
     * Get showed
     *
     * @return boolean 
     */
    public function getGlobal()
    {
        return $this->global;
    }
    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created) {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
	public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated) {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

}
