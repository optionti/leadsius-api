<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\ObBillingAddressRepository")
 * @ORM\Table(name="ob_billing_address")
 * @ExclusionPolicy("all")
 */
class ObBillingAddress {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_billing_address", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="billing_address_email", type="string", length=45)
     *
     * @Expose
     */
    private $email;
    /**
     * @var string
     *
     * @ORM\Column(name="billing_address_phone", type="string", length=45)
     *
     * @Expose
     */
    private $phone;
    /**
     * @var string
     *
     * @ORM\Column(name="billing_address_reference_name", type="string", length=45)
     *
     * @Expose
     */
    private $referenceName;
    /**
     * @var string
     *
     * @ORM\Column(name="billing_address_company_name", type="string", length=45)
     *
     * @Expose
     */
    private $companyName;
    /**
     * @var string
     *
     * @ORM\Column(name="billing_address_city", type="string", length=45)
     *
     * @Expose
     */
    private $city;
    /**
     * @var string
     *
     * @ORM\Column(name="billing_address_state", type="string", length=45)
     *
     * @Expose
     */
    private $state;
    /**
     * @var string
     *
     * @ORM\Column(name="billing_address_primary", type="string", length=45)
     *
     * @Expose
     */
    private $primary;
    /**
     * @var string
     *
     * @ORM\Column(name="billing_address_secondary", type="string", length=45)
     *
     * @Expose
     */
    private $secondary;
    /**
     * @var string
     *
     * @ORM\Column(name="billing_address_zip_code", type="string", length=45)
     *
     * @Expose
     */
    private $zipCode;
    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_account", referencedColumnName="id_account", nullable=false)
     * })
     */
    protected $account;
    /**
     * @var ObCountry
     *
     * @ORM\ManyToOne(targetEntity="ObCountry")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_country", referencedColumnName="id_country", nullable=true)
     * })
     */
    protected $country;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return ObBillingAddress
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return ObBillingAddress
     */
    public function setPhone($phone) {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone() {
        return $this->phone;
    }

    /**
     * Set referenceName
     *
     * @param string $referenceName
     * @return ObBillingAddress
     */
    public function setReferenceName($referenceName) {
        $this->referenceName = $referenceName;

        return $this;
    }

    /**
     * Get referenceName
     *
     * @return string
     */
    public function getReferenceName() {
        return $this->referenceName;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     * @return ObBillingAddress
     */
    public function setCompanyName($companyName) {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string
     */
    public function getCompanyName() {
        return $this->companyName;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return ObBillingAddress
     */
    public function setCity($city) {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity() {
        return $this->city;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return ObBillingAddress
     */
    public function setState($state) {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState() {
        return $this->state;
    }

    /**
     * Set primary
     *
     * @param string $primary
     * @return ObBillingAddress
     */
    public function setPrimary($primary) {
        $this->primary = $primary;

        return $this;
    }

    /**
     * Get primary
     *
     * @return string
     */
    public function getPrimary() {
        return $this->primary;
    }

    /**
     * Set secondary
     *
     * @param string $secondary
     * @return ObBillingAddress
     */
    public function setSecondary($secondary) {
        $this->secondary = $secondary;

        return $this;
    }

    /**
     * Get secondary
     *
     * @return string
     */
    public function getSecondary() {
        return $this->secondary;
    }

    /**
     * Set zipCode
     *
     * @param string $zipCode
     * @return ObBillingAddress
     */
    public function setZipCode($zipCode) {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode
     *
     * @return string
     */
    public function getZipCode() {
        return $this->zipCode;
    }


    /**
     * Set account
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $account
     * @return ObBillingAddress
     */
    public function setAccount(\Leadsius\ApiBundle\Entity\PlAccount $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Leadsius\ApiBundle\Entity\PlAccount 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set country
     *
     * @param \Leadsius\ApiBundle\Entity\ObCountry $country
     * @return ObBillingAddress
     */
    public function setCountry(\Leadsius\ApiBundle\Entity\ObCountry $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \Leadsius\ApiBundle\Entity\ObCountry 
     */
    public function getCountry()
    {
        return $this->country;
    }
}
