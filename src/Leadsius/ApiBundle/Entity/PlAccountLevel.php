<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\PlAccountLevelRepository")
 * @ORM\Table(name="pl_account_level")
 * @ExclusionPolicy("all")
 */
class PlAccountLevel {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_account_level", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="level_name", type="string", length=45, nullable=false)
     * @Expose
     */
    protected $name;
    
    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="PlAccount", mappedBy="level")
     */
    protected $accounts;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->accounts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add accounts
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $accounts
     * @return PlAccountLevel
     */
    public function addAccount(\Leadsius\ApiBundle\Entity\PlAccount $accounts)
    {
        $this->accounts[] = $accounts;

        return $this;
    }

    /**
     * Remove accounts
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $accounts
     */
    public function removeAccount(\Leadsius\ApiBundle\Entity\PlAccount $accounts)
    {
        $this->accounts->removeElement($accounts);
    }

    /**
     * Get accounts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAccounts()
    {
        return $this->accounts;
    }
}
