<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\PlFilterRepository")
 * @ORM\Table(name="pl_filter")
 * @ExclusionPolicy("all")
 */
class PlFilter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_filter", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;
    
    /**
     * @var text
     *
     * @ORM\Column(name="filter_advanced", type="text", nullable=true)
     * @Expose
     */
    private $advanced;

    /**
     * @var string
     *
     * @ORM\Column(name="filter_name", type="string", length=45, nullable=true)
     * @Expose
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="filter_category", type="string", length=45, nullable=true)
     * @Assert\Regex(pattern="/^(workflowTask|contact|lead_management)$/")
     * 
     * @Expose
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="filter_and_or", type="string", length=45, nullable=false)
     */
    private $andOr;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Expose
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @Expose
     */
    private $updated;

    /**
     * @var PlUser
     *
     * @ORM\ManyToOne(targetEntity="PlUser")
     * @ORM\JoinColumn(name="id_user", referencedColumnName="id_user", nullable=false)
     */
    private $user;
    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount")
     * @ORM\JoinColumn(name="id_account", referencedColumnName="id_account", nullable=false)
     */
    protected $account;

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set advanced
     *
     * @param text $advanced
     */
    public function setAdvanced($advanced)
    {
        $this->advanced = $advanced;
    }

    /**
     * Get advanced
     *
     * @return text 
     */
    public function getAdvanced()
    {
        return $this->advanced;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set category
     *
     * @param string $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * Get category
     *
     * @return string 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set andOr
     *
     * @param string $andOr
     */
    public function setAndOr($andOr)
    {
        $this->andOr = $andOr;
    }

    /**
     * Get andOr
     *
     * @return string 
     */
    public function getAndOr()
    {
        return $this->andOr;
    }

    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }


    /**
     * Set user
     *
     * @param \Leadsius\ApiBundle\Entity\PlUser $user
     * @return PlFilter
     */
    public function setUser(\Leadsius\ApiBundle\Entity\PlUser $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Leadsius\ApiBundle\Entity\PlUser 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set account
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $account
     * @return PlFilter
     */
    public function setAccount(\Leadsius\ApiBundle\Entity\PlAccount $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Leadsius\ApiBundle\Entity\PlAccount 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @assert\Callback
     */
    public function validateFilter(ExecutionContextInterface $context)
    {
        $json = null;
        try {
            $json = json_decode($this->getAdvanced(), true);
        } catch (\Exception $e) {
            $context->addViolationAt(
                'advanced',
                $e->getMessage(),
                array(),
                null
            );
        }

        if (null === $json || !is_array($json) || !isset($json['advancedFilter']) || !is_array($json['advancedFilter'])) {
            $context->addViolationAt(
                'advanced',
                'Filter not valid',
                array(),
                null
            );
        }
    }
}
