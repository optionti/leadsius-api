<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\PlAccountAppSettingsRepository")
 * @ORM\Table(name="pl_account_app_settings")
 * @ExclusionPolicy("all")
 */
class PlAccountAppSettings
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_account_app_settings", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="settings_name", type="string", length=255, nullable=false)
     * @Expose
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="total_users", type="integer", nullable=false)
     * @Expose
     */
    private $totalUsers;

    /**
     * @var integer
     *
     * @ORM\Column(name="total_companies", type="integer", nullable=false)
     * @Expose
     */
    private $totalCompanies;

    /**
     * @var integer
     *
     * @ORM\Column(name="total_contacts", type="integer", nullable=false)
     * @Expose
     */
    private $totalContacts;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Expose
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @Expose
     */
    private $updated;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set totalUsers
     *
     * @param integer $totalUsers
     */
    public function setTotalUsers($totalUsers)
    {
        $this->totalUsers = $totalUsers;
    }

    /**
     * Get totalUsers
     *
     * @return integer 
     */
    public function getTotalUsers()
    {
        return $this->totalUsers;
    }

    /**
     * Set totalCompanies
     *
     * @param integer $totalCompanies
     */
    public function setTotalCompanies($totalCompanies)
    {
        $this->totalCompanies = $totalCompanies;
    }

    /**
     * Get totalCompanies
     *
     * @return integer 
     */
    public function getTotalCompanies()
    {
        return $this->totalCompanies;
    }

    /**
     * Set totalContacts
     *
     * @param integer $totalContacts
     */
    public function setTotalContacts($totalContacts)
    {
        $this->totalContacts = $totalContacts;
    }

    /**
     * Get totalContacts
     *
     * @return integer 
     */
    public function getTotalContacts()
    {
        return $this->totalContacts;
    }

    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
