<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\PlCountryRepository")
 * @ORM\Table(name="pl_country")
 * @ExclusionPolicy("all")
 */
class PlCountry
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_country", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="iso", type="string", length=3, nullable=false)
     * @Expose
     */
    private $iso;

    /**
     * @var string
     *
     * @ORM\Column(name="country_name", type="string", length=80, nullable=false)
     * @Expose
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="printable_name", type="string", length=80, nullable=false)
     * @Expose
     */
    private $printableName;

    /**
     * @var string
     *
     * @ORM\Column(name="iso3", type="string", length=3, nullable=true)
     * @Expose
     */
    private $iso3;

    /**
     * @var integer
     *
     * @ORM\Column(name="numcode", type="integer", nullable=true)
     * @Expose
     */
    private $numcode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Expose
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @Expose
     */
    private $updated;
    
    /**
     * Get idCountry
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }
    
    /**
     * Get iso
     *
     * @return string 
     */
    public function getIso() {
        return $this->iso;
    }
    
    /**
     * Get countryName
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }
    
    /**
     * Get printableName
     *
     * @return string 
     */
    public function getPrintableName() {
        return $this->printableName;
    }
    
    /**
     * Get iso3
     *
     * @return string 
     */
    public function getIso3() {
        return $this->iso3;
    }
    
    /**
     * Set numcode
     *
     * @return integer
     */
    public function getNumcode() {
        return $this->numcode;
    }
    
    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated() {
        return $this->updated;
    }
    /**
     * Set iso
     *
     * @param string $iso
     * @return $this
     */
    public function setIso($iso) {
        $this->iso = $iso;
        return $this;
    }
    
    /**
     * Set name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }
    
    /**
     * Set printableName
     *
     * @param string $printableName
     * @return $this
     */
    public function setPrintableName($printableName) {
        $this->printableName = $printableName;
        return $this;
    }
    
    /**
     * Set iso3
     *
     * @param string $iso3
     * @return $this
     */
    public function setIso3($iso3) {
        $this->iso3 = $iso3;
        return $this;
    }
    
    /**
     * Set numcode
     *
     * @param integer $numcode
     * @return $this
     */
    public function setNumcode($numcode) {
        $this->numcode = $numcode;
        return $this;
    }
    
    /**
     * Set created
     *
     * @param \DateTime $created
     * @return $this
     */
    public function setCreated(\DateTime $created) {
        $this->created = $created;
        return $this;
    }
    
    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return $this
     */
    public function setUpdated(\DateTime $updated) {
        $this->updated = $updated;
        return $this;
    }


}
