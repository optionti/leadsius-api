<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\MaCustomHtmlBlockRepository")
 * @ORM\Table(name="ma_custom_html_block")
 * @ExclusionPolicy("all")
 */
class MaCustomHtmlBlock
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_custom_html_block", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="custom_html_block_name", type="string", length=255, nullable=true)
     *
     * @Expose
     */
    private $htmlBlockName;

    /**
     * @var text
     * @ORM\Column(name="custom_html_block_content", type="text", nullable=true)
     *
     * @Expose
     */
    private $htmlBlockContent;

   /**
     * @var datetime $created
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     *
     * @Expose
     */
    protected $created;

    /**
     * @var datetime $updated
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     *
     * @Expose
     */
    protected $updated;
    
    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_account", referencedColumnName="id_account", nullable=false)
     * })
     */
    protected $account;

    /**
     * @var PlUser
     *
     * @ORM\ManyToOne(targetEntity="PlUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id_user", nullable=true)
     * })
     */    
    private $user;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set htmlBlockName
     *
     * @param string $htmlBlockName
     */
    public function setHtmlBlockName($htmlBlockName)
    {
        $this->htmlBlockName = $htmlBlockName;
    }

    /**
     * Get htmlBlockName
     *
     * @return string 
     */
    public function getHtmlBlockName()
    {
        return $this->htmlBlockName;
    }

    /**
     * Set htmlBlockContent
     *
     * @param text $htmlBlockContent
     */
    public function setHtmlBlockContent($htmlBlockContent)
    {
        $this->htmlBlockContent = $htmlBlockContent;
    }

    /**
     * Get htmlBlockContent
     *
     * @return text 
     */
    public function getHtmlBlockContent()
    {
        return $this->htmlBlockContent;
    }

    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created) {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
	public function getCreated()
    {
        return $this->created;
    }
    
    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated) {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }
    

    /**
     * Set account
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $account
     * @return MaCustomHtmlBlock
     */
    public function setAccount(\Leadsius\ApiBundle\Entity\PlAccount $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Leadsius\ApiBundle\Entity\PlAccount 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set user
     *
     * @param \Leadsius\ApiBundle\Entity\PlUser $user
     * @return MaCustomHtmlBlock
     */
    public function setUser(\Leadsius\ApiBundle\Entity\PlUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Leadsius\ApiBundle\Entity\PlUser 
     */
    public function getUser()
    {
        return $this->user;
    }
}
