<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\PlManageAutogeneratedEmailsRepository")
 * @ORM\Table(name="pl_manage_autogenerated_emails")
 * @ExclusionPolicy("all")
 */
class PlManageAutogeneratedEmails {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_manage_autogenerated_emails", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var text
     *
     * @ORM\Column(name="manage_email_name", type="text", nullable=false)
     * @Expose
     */
    private $name;

    /**
     * @var text
     *
     * @ORM\Column(name="manage_email_from", type="text", nullable=false)
     * @Expose
     */
    private $from;
    //--

    /**
     * @var text
     *
     * @ORM\Column(name="manage_email_message", type="text", nullable=true)
     * @Expose
     */
    private $message;

    /**
     * @var text
     *
     * @ORM\Column(name="manage_email_subject", type="text",  nullable=false)
     * @Expose
     */
    private $subject;

    /**
     * @var text
     *
     * @ORM\Column(name="manage_email_type", type="text",  nullable=true)
     * @Expose
     */
    private $type;

    /**
     * @var datetime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="create")
     * @Expose
     */
    protected $created;

    /**
     * @var datetime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="update")
     * @Expose
     */
    protected $updated;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set subject
     *
     * @param string $subject
     */
    public function setSubject($subject) {
        $this->subject = $subject;
    }


    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * Get from
     *
     * @return string 
     */
    public function getFrom() {
        return $this->from;
    }

    /**
     * Set from
     *
     * @param string $from
     */
    public function setFrom($from) {
        $this->from = $from;
    }

 
    /**
     * Get subject
     *
     * @return string 
     */
    public function getSubject() {
        return $this->subject;
    }

    /**
     * Set message
     *
     * @param string $message
     */
    public function setMessage($message) {
        $this->message = $message;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage() {
        return $this->message;
    }

    /**
     * Set type
     *
     * @param string $type
     */
    public function setType($type) {
        $this->type = $type;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
	public function getCreated()
    {
        return $this->created;
    }

    /**
     * Get updated
     *
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }
}
