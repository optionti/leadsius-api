<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\PlAccountRepository")
 * @ORM\Table(name="pl_account")
 * @ExclusionPolicy("all")
 */
class PlAccount
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_account", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="account_company_name", type="string", length=45, nullable=false)
     * @Assert\NotBlank(message="Please enter the Company name.")
     *
     * @Expose
     */
    private $companyName;

    /**
     * @var string
     *
     * @ORM\Column(name="account_company_address", type="text", length=45, nullable=true)
     *
     * @Expose
     */
    private $companyAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="account_company_pobox", type="string", length=45, nullable=true)
     *
     * @Expose
     */
    private $companyPobox;

    /**
     * @var string
     *
     * @ORM\Column(name="account_company_city", type="string", length=45, nullable=true)
     *
     * @Expose
     */
    private $companyCity;

    /**
     * @var string
     *
     * @ORM\Column(name="account_company_id", type="string", length=45, nullable=true)
     *
     * @Expose
     */
    private $companyId;

    /**
     * @var string
     *
     * @ORM\Column(name="account_company_phone", type="string", length=45, nullable=true)
     *
     * @Expose
     */
    private $companyPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="account_company_website", type="string", length=255, nullable=true, unique=true)
     * @Assert\NotBlank(message="Please enter the Company website.")
     * @Assert\Url(message="This value is not a valid URL")
     *
     * @Expose
     */
    private $companyWebsite;

    /**
     * @var datetime
     *
     * @ORM\Column(name="acccount_contract_due_date", type="datetime", nullable=true)
     *
     * @Expose
     */
    private $companyDueDate;

    /**
     * @var string
     *
     * @ORM\Column(name="systemKey", type="string", length=64, unique=true)
     *
     * @Expose
     */
    private $systemKey;

    /**
     * @var boolean
     *
     * @ORM\Column(name="super_account_administration", type="boolean", nullable=true)
     *
     * @Expose
     */
    private $superAccountAdministration;

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_tracker_code", type="boolean", nullable=false)
     *
     * @Expose
     */
    private $showTrackerCode;

    /**
     * @var boolean
     *
     * @ORM\Column(name="account_is_locked", type="boolean", nullable=true)
     *
     * @Expose
     */
    private $isLocked;

    /**
     * @var boolean
     *
     * @ORM\Column(name="account_is_validated", type="boolean", nullable=true)
     *
     * @Expose
     */
    private $isValidated;

    /**
     * @var boolean
     *
     * @ORM\Column(name="account_root_validation", type="boolean", nullable=true)
     *
     * @Expose
     */
    private $rootValidation;

    /**
     * @var string
     *
     * @ORM\Column(name="account_vat_number", type="string", length=45, nullable=true)
     *
     * @Expose
     */
    private $vatNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="account_referer_plan", type="string", length=45, nullable=true)
     *
     * @Expose
     */
    private $refererPlan;

    /**
     * @var string
     *
     * @ORM\Column(name="account_contacts", type="string", length=45, nullable=true)
     *
     * @Expose
     */
    private $accountContacts;

    /**
     * @var string
     *
     * @ORM\Column(name="account_users", type="string", length=45, nullable=true)
     *
     * @Expose
     */
    private $accountUsers;

    /**
     * @var datetime $created
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     *
     * @Expose
     */
    private $created;

    /**
     * @var datetime $updated
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     *
     * @Expose
     */
    private $updated;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="PlUser", mappedBy="account")
     */
    private $users;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="PlContact", mappedBy="account")
     */
    private $contacts;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="PlCompany", mappedBy="account")
     */
    private $companies;

    /**
     * @var PlCountry
     *
     * @ORM\ManyToOne(targetEntity="PlCountry")
     * @ORM\JoinColumn(name="id_country", referencedColumnName="id_country", nullable=false)
     * @Assert\Type(type="PlCountry")
     * @Assert\NotBlank(message="Please select a Country.")
     */
    protected $country;

    /**
     * @var PlAccountAppSettings
     *
     * @ORM\ManyToOne(targetEntity="PlAccountAppSettings")
     * @ORM\JoinColumn(name="id_account_app_settings", referencedColumnName="id_account_app_settings", nullable=false)
     */
    protected $appSettings;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="PlRol", mappedBy="account")
     */
    protected $roles;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="PlDepartment", mappedBy="account")
     */
    protected $departments;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="PlCustomUrl", mappedBy="account")
     */
    protected $customUrls;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="PlField", mappedBy="account")
     */
    protected $fields;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="PlApp", inversedBy="accounts")
     * @ORM\JoinTable(name="pl_account_has_pl_app",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_account", referencedColumnName="id_account")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_app", referencedColumnName="id_app")
     *   }
     * )
     */
    private $apps;

    /**
     * @var PlAccountLevel
     *
     * @ORM\ManyToOne(targetEntity="PlAccountLevel", inversedBy="accounts")
     * @ORM\JoinColumn(name="id_account_level", referencedColumnName="id_account_level", nullable=true)
     */
    protected $level;

    /**
     * @var ObPlan
     *
     * @ORM\ManyToOne(targetEntity="ObPlan")
     * @ORM\JoinColumn(name="account_id_plan", referencedColumnName="id_plan", nullable=true)
     */
    private $plan;

    /**
     * @var ObPaymentInfo
     *
     * @ORM\ManyToOne(targetEntity="ObPaymentInfo")
     * @ORM\JoinColumn(name="account_id_payment_info", referencedColumnName="id_payment_info", nullable=true)
     */
    private $paymentInfo;

    /**
     * @var ObCountry
     *
     * @ORM\ManyToOne(targetEntity="ObCountry")
     * @ORM\JoinColumn(name="account_id_billing_country", referencedColumnName="id_country", nullable=true)
     */
    private $billingCountry;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="ObFeature", inversedBy="accounts", cascade={"persist"})
     * @ORM\JoinTable(name="pl_account_has_feature",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_account", referencedColumnName="id_account")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_feature", referencedColumnName="id_feature")
     *   }
     * )
     */
    private $features;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="PlApiKey", mappedBy="account")
     */
    protected $apiKeys;

    public function __toString()
    {
        return $this->companyName;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->contacts = new ArrayCollection();
        $this->companies = new ArrayCollection();
        $this->roles = new ArrayCollection();
        $this->departments = new ArrayCollection();
        $this->customUrls = new ArrayCollection();
        $this->fields = new ArrayCollection();
        $this->apps = new ArrayCollection();
        $this->features = new ArrayCollection();
        $this->apiKeys = new ArrayCollection();
    }

    /**
     * @param string $accountContacts
     *
     * @return $this
     */
    public function setAccountContacts($accountContacts)
    {
        $this->accountContacts = $accountContacts;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccountContacts()
    {
        return $this->accountContacts;
    }

    /**
     * @param string $accountUsers
     *
     * @return $this
     */
    public function setAccountUsers($accountUsers)
    {
        $this->accountUsers = $accountUsers;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccountUsers()
    {
        return $this->accountUsers;
    }

    /**
     * @param \Leadsius\ApiBundle\Entity\stirng $companyAddress
     *
     * @return $this
     */
    public function setCompanyAddress($companyAddress)
    {
        $this->companyAddress = $companyAddress;
        return $this;
    }

    /**
     * @return \Leadsius\ApiBundle\Entity\stirng
     */
    public function getCompanyAddress()
    {
        return $this->companyAddress;
    }

    /**
     * @param string $companyCity
     *
     * @return $this
     */
    public function setCompanyCity($companyCity)
    {
        $this->companyCity = $companyCity;
        return $this;
    }

    /**
     * @return string
     */
    public function getCompanyCity()
    {
        return $this->companyCity;
    }

    /**
     * @param \Leadsius\ApiBundle\Entity\datetime $companyDueDate
     *
     * @return $this
     */
    public function setCompanyDueDate($companyDueDate)
    {
        $this->companyDueDate = $companyDueDate;
        return $this;
    }

    /**
     * @return \Leadsius\ApiBundle\Entity\datetime
     */
    public function getCompanyDueDate()
    {
        return $this->companyDueDate;
    }

    /**
     * @param string $companyId
     *
     * @return $this
     */
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;
        return $this;
    }

    /**
     * @return string
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * @param string $companyName
     *
     * @return $this
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
        return $this;
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param string $companyPhone
     *
     * @return $this
     */
    public function setCompanyPhone($companyPhone)
    {
        $this->companyPhone = $companyPhone;
        return $this;
    }

    /**
     * @return string
     */
    public function getCompanyPhone()
    {
        return $this->companyPhone;
    }

    /**
     * @param string $companyPobox
     *
     * @return $this
     */
    public function setCompanyPobox($companyPobox)
    {
        $this->companyPobox = $companyPobox;
        return $this;
    }

    /**
     * @return string
     */
    public function getCompanyPobox()
    {
        return $this->companyPobox;
    }

    /**
     * @param string $companyWebsite
     *
     * @return $this
     */
    public function setCompanyWebsite($companyWebsite)
    {
        $this->companyWebsite = $companyWebsite;
        return $this;
    }

    /**
     * @return string
     */
    public function getCompanyWebsite()
    {
        return $this->companyWebsite;
    }

    /**
     * @param \Leadsius\ApiBundle\Entity\datetime $created
     *
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return \Leadsius\ApiBundle\Entity\datetime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param boolean $isLocked
     *
     * @return $this
     */
    public function setIsLocked($isLocked)
    {
        $this->isLocked = $isLocked;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsLocked()
    {
        return $this->isLocked;
    }

    /**
     * @param boolean $isValidated
     *
     * @return $this
     */
    public function setIsValidated($isValidated)
    {
        $this->isValidated = $isValidated;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsValidated()
    {
        return $this->isValidated;
    }

    /**
     * @param string $refererPlan
     *
     * @return $this
     */
    public function setRefererPlan($refererPlan)
    {
        $this->refererPlan = $refererPlan;
        return $this;
    }

    /**
     * @return string
     */
    public function getRefererPlan()
    {
        return $this->refererPlan;
    }

    /**
     * @param boolean $rootValidation
     *
     * @return $this
     */
    public function setRootValidation($rootValidation)
    {
        $this->rootValidation = $rootValidation;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getRootValidation()
    {
        return $this->rootValidation;
    }

    /**
     * @param boolean $showTrackerCode
     *
     * @return $this
     */
    public function setShowTrackerCode($showTrackerCode)
    {
        $this->showTrackerCode = $showTrackerCode;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getShowTrackerCode()
    {
        return $this->showTrackerCode;
    }

    /**
     * @param boolean $superAccountAdministration
     *
     * @return $this
     */
    public function setSuperAccountAdministration($superAccountAdministration)
    {
        $this->superAccountAdministration = $superAccountAdministration;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getSuperAccountAdministration()
    {
        return $this->superAccountAdministration;
    }

    /**
     * @param string $systemKey
     *
     * @return $this
     */
    public function setSystemKey($systemKey)
    {
        $this->systemKey = $systemKey;
        return $this;
    }

    /**
     * @return string
     */
    public function getSystemKey()
    {
        return $this->systemKey;
    }

    /**
     * @param \Leadsius\ApiBundle\Entity\datetime $updated
     *
     * @return $this
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
        return $this;
    }

    /**
     * @return \Leadsius\ApiBundle\Entity\datetime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param string $vatNumber
     *
     * @return $this
     */
    public function setVatNumber($vatNumber)
    {
        $this->vatNumber = $vatNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getVatNumber()
    {
        return $this->vatNumber;
    }

    /**
     * Add users
     *
     * @param \Leadsius\ApiBundle\Entity\PlUser $users
     * @return PlAccount
     */
    public function addUser(\Leadsius\ApiBundle\Entity\PlUser $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \Leadsius\ApiBundle\Entity\PlUser $users
     */
    public function removeUser(\Leadsius\ApiBundle\Entity\PlUser $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add contacts
     *
     * @param \Leadsius\ApiBundle\Entity\PlContact $contacts
     * @return PlAccount
     */
    public function addContact(\Leadsius\ApiBundle\Entity\PlContact $contacts)
    {
        $this->contacts[] = $contacts;

        return $this;
    }

    /**
     * Remove contacts
     *
     * @param \Leadsius\ApiBundle\Entity\PlContact $contacts
     */
    public function removeContact(\Leadsius\ApiBundle\Entity\PlContact $contacts)
    {
        $this->contacts->removeElement($contacts);
    }

    /**
     * Get contacts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * Add companies
     *
     * @param \Leadsius\ApiBundle\Entity\PlCompany $companies
     * @return PlAccount
     */
    public function addCompany(\Leadsius\ApiBundle\Entity\PlCompany $companies)
    {
        $this->companies[] = $companies;

        return $this;
    }

    /**
     * Remove companies
     *
     * @param \Leadsius\ApiBundle\Entity\PlCompany $companies
     */
    public function removeCompany(\Leadsius\ApiBundle\Entity\PlCompany $companies)
    {
        $this->companies->removeElement($companies);
    }

    /**
     * Get companies
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCompanies()
    {
        return $this->companies;
    }

    /**
     * Set country
     *
     * @param \Leadsius\ApiBundle\Entity\PlCountry $country
     * @return PlAccount
     */
    public function setCountry(\Leadsius\ApiBundle\Entity\PlCountry $country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \Leadsius\ApiBundle\Entity\PlCountry 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set appSettings
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccountAppSettings $appSettings
     * @return PlAccount
     */
    public function setAppSettings(\Leadsius\ApiBundle\Entity\PlAccountAppSettings $appSettings)
    {
        $this->appSettings = $appSettings;

        return $this;
    }

    /**
     * Get appSettings
     *
     * @return \Leadsius\ApiBundle\Entity\PlAccountAppSettings 
     */
    public function getAppSettings()
    {
        return $this->appSettings;
    }

    /**
     * Add roles
     *
     * @param \Leadsius\ApiBundle\Entity\PlRol $roles
     * @return PlAccount
     */
    public function addRole(\Leadsius\ApiBundle\Entity\PlRol $roles)
    {
        $this->roles[] = $roles;

        return $this;
    }

    /**
     * Remove roles
     *
     * @param \Leadsius\ApiBundle\Entity\PlRol $roles
     */
    public function removeRole(\Leadsius\ApiBundle\Entity\PlRol $roles)
    {
        $this->roles->removeElement($roles);
    }

    /**
     * Get roles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Add departments
     *
     * @param \Leadsius\ApiBundle\Entity\PlDepartment $departments
     * @return PlAccount
     */
    public function addDepartment(\Leadsius\ApiBundle\Entity\PlDepartment $departments)
    {
        $this->departments[] = $departments;

        return $this;
    }

    /**
     * Remove departments
     *
     * @param \Leadsius\ApiBundle\Entity\PlDepartment $departments
     */
    public function removeDepartment(\Leadsius\ApiBundle\Entity\PlDepartment $departments)
    {
        $this->departments->removeElement($departments);
    }

    /**
     * Get departments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDepartments()
    {
        return $this->departments;
    }

    /**
     * Add customUrls
     *
     * @param \Leadsius\ApiBundle\Entity\PlCustomUrl $customUrls
     * @return PlAccount
     */
    public function addCustomUrl(\Leadsius\ApiBundle\Entity\PlCustomUrl $customUrls)
    {
        $this->customUrls[] = $customUrls;

        return $this;
    }

    /**
     * Remove customUrls
     *
     * @param \Leadsius\ApiBundle\Entity\PlCustomUrl $customUrls
     */
    public function removeCustomUrl(\Leadsius\ApiBundle\Entity\PlCustomUrl $customUrls)
    {
        $this->customUrls->removeElement($customUrls);
    }

    /**
     * Get customUrls
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCustomUrls()
    {
        return $this->customUrls;
    }

    /**
     * Add fields
     *
     * @param \Leadsius\ApiBundle\Entity\PlField $fields
     * @return PlAccount
     */
    public function addField(\Leadsius\ApiBundle\Entity\PlField $fields)
    {
        $this->fields[] = $fields;

        return $this;
    }

    /**
     * Remove fields
     *
     * @param \Leadsius\ApiBundle\Entity\PlField $fields
     */
    public function removeField(\Leadsius\ApiBundle\Entity\PlField $fields)
    {
        $this->fields->removeElement($fields);
    }

    /**
     * Get fields
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Add apps
     *
     * @param \Leadsius\ApiBundle\Entity\PlApp $apps
     * @return PlAccount
     */
    public function addApp(\Leadsius\ApiBundle\Entity\PlApp $apps)
    {
        $this->apps[] = $apps;

        return $this;
    }

    /**
     * Remove apps
     *
     * @param \Leadsius\ApiBundle\Entity\PlApp $apps
     */
    public function removeApp(\Leadsius\ApiBundle\Entity\PlApp $apps)
    {
        $this->apps->removeElement($apps);
    }

    /**
     * Get apps
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getApps()
    {
        return $this->apps;
    }

    /**
     * Set level
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccountLevel $level
     * @return PlAccount
     */
    public function setLevel(\Leadsius\ApiBundle\Entity\PlAccountLevel $level = null)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return \Leadsius\ApiBundle\Entity\PlAccountLevel 
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set plan
     *
     * @param \Leadsius\ApiBundle\Entity\ObPlan $plan
     * @return PlAccount
     */
    public function setPlan(\Leadsius\ApiBundle\Entity\ObPlan $plan = null)
    {
        $this->plan = $plan;

        return $this;
    }

    /**
     * Get plan
     *
     * @return \Leadsius\ApiBundle\Entity\ObPlan 
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * Set paymentInfo
     *
     * @param \Leadsius\ApiBundle\Entity\ObPaymentInfo $paymentInfo
     * @return PlAccount
     */
    public function setPaymentInfo(\Leadsius\ApiBundle\Entity\ObPaymentInfo $paymentInfo = null)
    {
        $this->paymentInfo = $paymentInfo;

        return $this;
    }

    /**
     * Get paymentInfo
     *
     * @return \Leadsius\ApiBundle\Entity\ObPaymentInfo 
     */
    public function getPaymentInfo()
    {
        return $this->paymentInfo;
    }

    /**
     * Set billingCountry
     *
     * @param \Leadsius\ApiBundle\Entity\ObCountry $billingCountry
     * @return PlAccount
     */
    public function setBillingCountry(\Leadsius\ApiBundle\Entity\ObCountry $billingCountry = null)
    {
        $this->billingCountry = $billingCountry;

        return $this;
    }

    /**
     * Get billingCountry
     *
     * @return \Leadsius\ApiBundle\Entity\ObCountry 
     */
    public function getBillingCountry()
    {
        return $this->billingCountry;
    }

    /**
     * Add features
     *
     * @param \Leadsius\ApiBundle\Entity\ObFeature $features
     * @return PlAccount
     */
    public function addFeature(\Leadsius\ApiBundle\Entity\ObFeature $features)
    {
        $this->features[] = $features;

        return $this;
    }

    /**
     * Remove features
     *
     * @param \Leadsius\ApiBundle\Entity\ObFeature $features
     */
    public function removeFeature(\Leadsius\ApiBundle\Entity\ObFeature $features)
    {
        $this->features->removeElement($features);
    }

    /**
     * Get features
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFeatures()
    {
        return $this->features;
    }

    /**
     * Add apiKeys
     *
     * @param PlApiKey $apiKeys
     * @return PlAccount
     */
    public function addApiKey(PlApiKey $apiKeys)
    {
        $this->apiKeys[] = $apiKeys;

        return $this;
    }

    /**
     * Remove apiKeys
     *
     * @param PlApiKey $apiKeys
     */
    public function removeApiKey(PlApiKey $apiKeys)
    {
        $this->apiKeys->removeElement($apiKeys);
    }

    /**
     * Get apiKeys
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApiKeys()
    {
        return $this->apiKeys;
    }
}
