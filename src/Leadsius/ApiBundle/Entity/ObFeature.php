<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\ObFeatureRepository")
 * @ORM\Table(name="ob_feature")
 * @ExclusionPolicy("all")
 */
class ObFeature {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_feature", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="feature_code", type="string", length=45)
     *
     * @Expose
     */
    private $code;
    /**
     * @var string $name
     *
     * @ORM\Column(name="feature_name", type="string", length=45)
     *
     * @Expose
     */
    private $name;
    /**
     * @var string
     *
     * @ORM\Column(name="feature_type", type="string", length=45)
     *
     * @Expose
     */
    private $type;
    /**
     * @var string
     *
     * @ORM\Column(name="feature_detail", type="string", length=45)
     *
     * @Expose
     */
    private $detail;
    /**
     * @var string
     *
     * @ORM\Column(name="feature_category", type="string", length=45)
     *
     * @Expose
     */
    private $category;
    /**
     * @var $featurePlanes
     *
     * @ORM\ManyToMany(targetEntity="ObPlan", mappedBy="features", cascade={"persist"})
     */
    private $planes;
   /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="PlAccount", mappedBy="features", cascade={"persist"})
     */
    private $accounts;

    public function __construct()
    {
        $this->accounts = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param string $code
     */
    public function setCode($code) {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getCode() {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ObFeature
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return ObFeature
     */
    public function setType($type) {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Set detail
     *
     * @param string $detail
     * @return ObFeature
     */
    public function setDetail($detail) {
        $this->detail = $detail;

        return $this;
    }

    /**
     * Get detail
     *
     * @return string
     */
    public function getDetail() {
        return $this->detail;
    }

    /**
     * Set category
     *
     * @param string $category
     * @return ObFeature
     */
    public function setCategory($category) {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory() {
        return $this->category;
    }


    /**
     * Add planes
     *
     * @param \Leadsius\ApiBundle\Entity\ObPlan $planes
     * @return ObFeature
     */
    public function addPlane(\Leadsius\ApiBundle\Entity\ObPlan $planes)
    {
        $this->planes[] = $planes;

        return $this;
    }

    /**
     * Remove planes
     *
     * @param \Leadsius\ApiBundle\Entity\ObPlan $planes
     */
    public function removePlane(\Leadsius\ApiBundle\Entity\ObPlan $planes)
    {
        $this->planes->removeElement($planes);
    }

    /**
     * Get planes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPlanes()
    {
        return $this->planes;
    }

    /**
     * Add accounts
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $accounts
     * @return ObFeature
     */
    public function addAccount(\Leadsius\ApiBundle\Entity\PlAccount $accounts)
    {
        $this->accounts[] = $accounts;

        return $this;
    }

    /**
     * Remove accounts
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $accounts
     */
    public function removeAccount(\Leadsius\ApiBundle\Entity\PlAccount $accounts)
    {
        $this->accounts->removeElement($accounts);
    }

    /**
     * Get accounts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAccounts()
    {
        return $this->accounts;
    }
}
