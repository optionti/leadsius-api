<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\MaWorkflowTaskRepository")
 * @ORM\Table(name="ma_workflow_task")
 * @ExclusionPolicy("all")
 * @Assert\Callback(methods={
 *      "validDelay",
 *      "validFilter",
 *      "validTypeEmail",
 *      "validTypeStartWith",
 *      "validStartWithConditionDates",
 *      "validStartWithTaskDate",
 *      "validWhenHasParentWorkflowTask"
 * })
 */
class MaWorkflowTask
{
    const TYPE_START_WITH_TASK = 'startWithTask';
    const TYPE_START_WITH_CONDITION = 'startWithCondition';
    const TYPE_EMAIL = 'email';

    const CONDITION_OPENED_EMAIL = 'openedEmail';
    const CONDITION_NOT_OPENED_EMAIL = 'notOpenedEmail';

    const STATUS_PENDING = 'pending';

    /**
     * @var integer $idWorkflow
     *
     * @ORM\Column(name="id_workflow_task", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;

    /**
     * @var string 
     *
     * @ORM\Column(name="workflow_task_type", type="string", length=45, nullable=false)
     * @Assert\Regex("/^(startWith(Task|Condition)|email)$/")
     *
     * @Expose
     */
    private $type;

    /**
     * @var string 
     *
     * @ORM\Column(name="workflow_task_condition", type="string", length=45, nullable=true)
     * @Assert\Regex("/^(openedEmail|notOpenedEmail)$/")
     * @Expose
     */
    private $condition;

    /**
     * @var datetime $startDate
     *
     * @ORM\Column(name="workflow_task_start_date", type="datetime", nullable=true)
     * @Assert\DateTime()
     * @Expose
     */
    private $startDate;

    /**
     * @var string 
     *
     * @ORM\Column(name="workflow_task_end_date", type="datetime", nullable=true)
     * @Assert\DateTime()
     * @Expose
     */
    private $endDate;

    /**
     * @var string 
     *
     * @ORM\Column(name="workflow_task_delay", type="string", nullable=true)
     * @Assert\Regex("/\d+,[0-2]?\d,[0-6]?\d/")
     *
     * @Expose
     */
    private $delay;

    /**
     * @var string 
     *
     * @ORM\Column(name="workflow_task_status", type="string", length=255, nullable=false)
     * @Assert\Regex("/^(pending|complete|processing)$/")
     * @Expose
     */
    private $status;

    /**
     * @var text 
     *
     * @ORM\Column(name="workflow_task_contacts", type="text", nullable=true)
     *
     * @Expose
     */
    private $contacts;

    /**
     * @var datetime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=false)
     *
     */
    protected $created;

    /**
     * @var datetime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     *
     * @Expose
     */
    protected $updated;

    /**
     * @var datetime $deleted
     *
     * @ORM\Column(name="deleted", type="datetime", nullable=true)
     *
     * @Expose
     */
    protected $deleted;

    /**
     * @var boolean 
     *
     * @ORM\Column(name="workflow_task_active", type="boolean", nullable=true)
     *
     * @Expose
     */
    protected $active = true;

    /**
     * @var boolean 
     *
     * @ORM\Column(name="workflow_task_has_childs", type="boolean", nullable=true)
     *
     */
    protected $hasChilds = true;

    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_account", referencedColumnName="id_account", nullable=false)
     * })
     */
    protected $account;

    /**
     * @var PlUser
     *
     * @ORM\ManyToOne(targetEntity="PlUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id_user", nullable=true)
     * })
     */
    private $user;

    /**
     * @var MaWorkflow
     *
     * @ORM\ManyToOne(targetEntity="MaWorkflow")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_workflow", referencedColumnName="id_workflow", nullable=false, onDelete="CASCADE")
     * })
     *
     * @Expose
     */
    protected $workflow;

    /**
     * @var MaEmailCampaign
     *
     * @ORM\ManyToOne(targetEntity="MaEmailCampaign", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_email_campaign", referencedColumnName="id_email_campaign", nullable=true)
     * })
     * @Assert\Valid
     * @Expose
     */
    protected $emailCampaign;

    /**
     * @var MaLandingPage
     *
     * @ORM\ManyToOne(targetEntity="MaLandingPage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_landing_page", referencedColumnName="id_landing_page", nullable=true)
     * })
     */
    protected $landingPage;

    /**
     * @var MaWebform
     *
     * @ORM\ManyToOne(targetEntity="MaWebform")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_webform", referencedColumnName="id_webform", nullable=true)
     * })
     */
    protected $webform;

    /**
     * @var PlFilter
     *
     * @ORM\ManyToOne(targetEntity="PlFilter")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_filter", referencedColumnName="id_filter", nullable=true)
     * })
     */
    protected $filter;

    /**
     * @var MaWorkflowTask
     *
     * @ORM\ManyToOne(targetEntity="MaWorkflowTask")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_parent_workflow_task", referencedColumnName="id_workflow_task", onDelete="CASCADE")
     * })
     *
     * @Expose
     */
    protected $parentWorkflowTask;

    /**
     * @var PlPicklistOption
     *
     * @ORM\ManyToOne(targetEntity="PlPicklistOption")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="workflow_task_category", referencedColumnName="id_picklist_option", nullable=true)
     * })
     */
    private $taskCategory;

   public function __construct()
    {
        $this->status = self::STATUS_PENDING;
    }

    public function __toString()
    {
        return $this->getType();
    }

    /**
     * Valid if delay value is correct and contain correct ranges
     *
     * @param ExecutionContextInterface $context
     */
    public function validDelay(ExecutionContextInterface $context)
    {
        if (self::TYPE_EMAIL === $this->getType() && null === $this->getDelay()) {
            $context->addViolationAt('delay', 'Delay not must be null', array(), null);
        }

        if (null !== $this->getDelay()) {
            $delay = explode(',', $this->getDelay());

            if (3 !== count($delay)) {
                $context->addViolationAt('delay', 'Delay should contain days,hours,minutes', array(), null);
                return;
            }

            if (0 > $delay[0]) {
                $context->addViolationAt('delay', 'Delay days must have values great or equal to zero', array(), null);
            }

            if (0 > $delay[1] || 24 < $delay[1]) {
                $context->addViolationAt('delay', 'Delay hours must have values great or equal to zero and less or equal twenty-four', array(), null);
            }

            if (0 > $delay[2] || 60 < $delay[1]) {
                $context->addViolationAt('delay', 'Delay minutes must have values great or equal to zero and less or equal to sixty', array(), null);
            }
        }
    }

    /**
     * Valid if filter is defined when type is startWithCondition
     *
     * @param ExecutionContextInterface $context
     */
    public function validFilter(ExecutionContextInterface $context)
    {
        if (self::TYPE_START_WITH_CONDITION === $this->getType() && null === $this->getFilter()) {
            $context->addViolationAt('filter', 'Filter not must be null', array(), null);
        }
    }

    /**
     * Valid when type is email, condition not must be null and parentWorkflowTask not must me null
     *
     * @param ExecutionContextInterface $context
     */
    public function validTypeEmail(ExecutionContextInterface $context)
    {
        if (self::TYPE_EMAIL === $this->getType()) {
            if (null === $this->getCondition()) {
                $context->addViolationAt('condition', 'Condition not must be null', array(), null);
            }

            if (null === $this->getParentWorkflowTask()) {
                $context->addViolationAt('parentWorkflowTask', 'Parent workflow task not must be null', array(), null);
            }
        }
    }

    /**
     *  Valid when type is startWithTask or startWithCondition, parentWorkflowTask is must be null
     *
     * @param ExecutionContextInterface $context
     */
    public function validTypeStartWith(ExecutionContextInterface $context)
    {
        if (self::TYPE_START_WITH_CONDITION === $this->getType()
            || self::TYPE_START_WITH_TASK === $this->getType()) {
            if (null !== $this->getParentWorkflowTask()) {
                $context->addViolationAt('parentWorkflowTask', 'Parent workflow task must be null', array(), null);
            }
        }
    }

    /**
     * Valid when type startWithCondition if set start date and end date
     *
     * @param ExecutionContextInterface $context
     */
    public function validStartWithConditionDates(ExecutionContextInterface $context)
    {
        if (self::TYPE_START_WITH_CONDITION === $this->getType()
            && (null === $this->getStartDate() || null === $this->getEndDate())) {
            $context->addViolationAt('startDate', 'Start and End date is not set', array(), null);
        }
    }

    /**
     * Valid when type is startWithTask if set start date
     *
     * @param ExecutionContextInterface $context
     */
    public function validStartWithTaskDate(ExecutionContextInterface $context)
    {
        if (self::TYPE_START_WITH_CONDITION === $this->getType()
            && null === $this->getStartDate()) {
            $context->addViolationAt('startDate', 'Start date is not set', array(), null);
        }
    }

    /**
     * Valid when has Parent Workflow is present or not present
     *
     * @param ExecutionContextInterface $context
     */
    public function validWhenHasParentWorkflowTask(ExecutionContextInterface $context)
    {
        if (null !== $this->getParentWorkflowTask()
            && self::TYPE_EMAIL !== $this->getType()) {
            $context->addViolationAt('type', 'Only type email is permitted', array(), null);
        }
    }

    /**
     * Set id
     *
     * @param string $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     */
    public function setType($type) {
        $this->type = $type;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Set status
     *
     * @param string $status
     */
    public function setStatus($status) {
        $this->status = $status;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created) {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated) {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime 
     */
    public function getUpdated() {
        return $this->updated;
    }

    /**
     * Set deleted
     *
     * @param datetime $deleted
     */
    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    /**
     * Get deleted
     *
     * @return datetime 
     */
    public function getDeleted() {
        return $this->deleted;
    }

    /**
     * Set contacts
     *
     * @param text $contacts
     */
    public function setContacts($contacts) {
        $this->contacts = $contacts;
    }

    /**
     * Get contacts
     *
     * @return text 
     */
    public function getContacts() {
        return $this->contacts;
    }

    /**
     * Set delay
     *
     * @param datetime $delay
     */
    public function setDelay($delay) {
        $this->delay = $delay;
    }

    /**
     * Get delay
     *
     * @return datetime 
     */
    public function getDelay() {
        return $this->delay;
    }

    /**
     * Set condition
     *
     * @param string $condition
     */
    public function setCondition($condition) {
        $this->condition = $condition;
    }

    /**
     * Get condition
     *
     * @return string 
     */
    public function getCondition() {
        return $this->condition;
    }

    /**
     * Set startDate
     *
     * @param datetime $startDate
     */
    public function setStartDate($startDate) {
        $this->startDate = $startDate;
    }

    /**
     * Get startDate
     *
     * @return datetime 
     */
    public function getStartDate() {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param datetime $endDate
     */
    public function setEndDate($endDate) {
        $this->endDate = $endDate;
    }

    /**
     * Get endDate
     *
     * @return datetime 
     */
    public function getEndDate() {
        return $this->endDate;
    }

    /**
     * Set active
     *
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set hasChilds
     *
     * @param boolean $hasChilds
     */
    public function setHasChilds($hasChilds)
    {
        $this->hasChilds = $hasChilds;
    }

    /**
     * Get hasChilds
     *
     * @return boolean 
     */
    public function getHasChilds()
    {
        return $this->hasChilds;
    }

    /**
     * Set account
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $account
     * @return MaWorkflowTask
     */
    public function setAccount(\Leadsius\ApiBundle\Entity\PlAccount $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Leadsius\ApiBundle\Entity\PlAccount 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set user
     *
     * @param \Leadsius\ApiBundle\Entity\PlUser $user
     * @return MaWorkflowTask
     */
    public function setUser(\Leadsius\ApiBundle\Entity\PlUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Leadsius\ApiBundle\Entity\PlUser 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set workflow
     *
     * @param \Leadsius\ApiBundle\Entity\MaWorkflow $workflow
     * @return MaWorkflowTask
     */
    public function setWorkflow(\Leadsius\ApiBundle\Entity\MaWorkflow $workflow)
    {
        $this->workflow = $workflow;

        return $this;
    }

    /**
     * Get workflow
     *
     * @return \Leadsius\ApiBundle\Entity\MaWorkflow 
     */
    public function getWorkflow()
    {
        return $this->workflow;
    }

    /**
     * Set emailCampaign
     *
     * @param \Leadsius\ApiBundle\Entity\MaEmailCampaign $emailCampaign
     * @return MaWorkflowTask
     */
    public function setEmailCampaign(\Leadsius\ApiBundle\Entity\MaEmailCampaign $emailCampaign = null)
    {
        $this->emailCampaign = $emailCampaign;

        return $this;
    }

    /**
     * Get emailCampaign
     *
     * @return \Leadsius\ApiBundle\Entity\MaEmailCampaign 
     */
    public function getEmailCampaign()
    {
        return $this->emailCampaign;
    }

    /**
     * Set landingPage
     *
     * @param \Leadsius\ApiBundle\Entity\MaLandingPage $landingPage
     * @return MaWorkflowTask
     */
    public function setLandingPage(\Leadsius\ApiBundle\Entity\MaLandingPage $landingPage = null)
    {
        $this->landingPage = $landingPage;

        return $this;
    }

    /**
     * Get landingPage
     *
     * @return \Leadsius\ApiBundle\Entity\MaLandingPage 
     */
    public function getLandingPage()
    {
        return $this->landingPage;
    }

    /**
     * Set webform
     *
     * @param \Leadsius\ApiBundle\Entity\MaWebform $webform
     * @return MaWorkflowTask
     */
    public function setWebform(\Leadsius\ApiBundle\Entity\MaWebform $webform = null)
    {
        $this->webform = $webform;

        return $this;
    }

    /**
     * Get webform
     *
     * @return \Leadsius\ApiBundle\Entity\MaWebform 
     */
    public function getWebform()
    {
        return $this->webform;
    }

    /**
     * Set filter
     *
     * @param \Leadsius\ApiBundle\Entity\PlFilter $filter
     * @return MaWorkflowTask
     */
    public function setFilter(\Leadsius\ApiBundle\Entity\PlFilter $filter = null)
    {
        $this->filter = $filter;

        return $this;
    }

    /**
     * Get filter
     *
     * @return \Leadsius\ApiBundle\Entity\PlFilter 
     */
    public function getFilter()
    {
        return $this->filter;
    }

    /**
     * Set parentWorkflowTask
     *
     * @param \Leadsius\ApiBundle\Entity\MaWorkflowTask $parentWorkflowTask
     * @return MaWorkflowTask
     */
    public function setParentWorkflowTask(\Leadsius\ApiBundle\Entity\MaWorkflowTask $parentWorkflowTask = null)
    {
        $this->parentWorkflowTask = $parentWorkflowTask;

        return $this;
    }

    /**
     * Get parentWorkflowTask
     *
     * @return \Leadsius\ApiBundle\Entity\MaWorkflowTask 
     */
    public function getParentWorkflowTask()
    {
        return $this->parentWorkflowTask;
    }

    /**
     * Set taskCategory
     *
     * @param \Leadsius\ApiBundle\Entity\PlPicklistOption $taskCategory
     * @return MaWorkflowTask
     */
    public function setTaskCategory(\Leadsius\ApiBundle\Entity\PlPicklistOption $taskCategory = null)
    {
        $this->taskCategory = $taskCategory;

        return $this;
    }

    /**
     * Get taskCategory
     *
     * @return \Leadsius\ApiBundle\Entity\PlPicklistOption 
     */
    public function getTaskCategory()
    {
        return $this->taskCategory;
    }
}
