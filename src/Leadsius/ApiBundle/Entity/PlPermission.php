<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\PlPermissionRepository")
 * @ORM\Table(name="pl_permission")
 * @ExclusionPolicy("all")
 */
class PlPermission
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_permission", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var PlRol
     *
     * @ORM\ManyToMany(targetEntity="PlRol", mappedBy="idPermission", cascade={"persist"})
     */
    private $rol;

    /**
     * @var PlApp
     *
     * @ORM\ManyToOne(targetEntity="PlApp", inversedBy="idPermission")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_app", referencedColumnName="id_app", nullable=false)
     * })
     */
    private $app;

    /**
     * @var string
     *
     * @ORM\Column(name="filter_name", type="string", length=155, nullable=true)
     * @Expose
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="permission_description", type="string", length=500, nullable=true)
     * @Expose
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="permission_value", type="string", length=155, nullable=false)
     * @Expose
     */
    private $value;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Expose
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @Expose
     */
    private $updated;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set value
     *
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->rol = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add rol
     *
     * @param \Leadsius\ApiBundle\Entity\PlRol $rol
     * @return PlPermission
     */
    public function addRol(\Leadsius\ApiBundle\Entity\PlRol $rol)
    {
        $this->rol[] = $rol;

        return $this;
    }

    /**
     * Remove rol
     *
     * @param \Leadsius\ApiBundle\Entity\PlRol $rol
     */
    public function removeRol(\Leadsius\ApiBundle\Entity\PlRol $rol)
    {
        $this->rol->removeElement($rol);
    }

    /**
     * Get rol
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRol()
    {
        return $this->rol;
    }

    /**
     * Set app
     *
     * @param \Leadsius\ApiBundle\Entity\PlApp $app
     * @return PlPermission
     */
    public function setApp(\Leadsius\ApiBundle\Entity\PlApp $app)
    {
        $this->app = $app;

        return $this;
    }

    /**
     * Get app
     *
     * @return \Leadsius\ApiBundle\Entity\PlApp 
     */
    public function getApp()
    {
        return $this->app;
    }
}
