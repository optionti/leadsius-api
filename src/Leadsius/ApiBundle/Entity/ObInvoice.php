<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\ObInvoiceRepository")
 * @ORM\Table(name="ob_invoice")
 * @ExclusionPolicy("all")
 */
class ObInvoice {
    /**
     * @var integer
     *
     * @ORM\Column(name="id_invoice", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="invoice_number", type="string", length=45, nullable=false)
     *
     * @Expose
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="invoice_description", type="string", length=45, nullable=false)
     *
     * @Expose
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="invoice_amount", type="string", length=45, nullable=false)
     *
     * @Expose
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="invoice_vat_percentage", type="string", length=45, nullable=false)
     *
     * @Expose
     */
    private $vatPercentage;

    /**
     * @var string
     *
     * @ORM\Column(name="invoice_vat_amount", type="string", length=45, nullable=false)
     *
     * @Expose
     */
    private $vatAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="invoice_total", type="string", length=45, nullable=false)
     *
     * @Expose
     */
    private $total;

    /**
     * @var string
     *
     * @ORM\Column(name="invoice_status", type="string", length=45, nullable=false)
     *
     * @Expose
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="invoice_issue_date", type="datetime", nullable=false)
     *
     * @Expose
     */
    private $issueDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="invoice_due_date", type="datetime", nullable=false)
     *
     * @Expose
     */
    private $dueDate;

    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount", inversedBy="user")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_account", referencedColumnName="id_account", nullable=false)
     * })
     */
    private $account;

    /**
     * @var ObPaymentMethod
     *
     * @ORM\ManyToOne(targetEntity="ObPaymentMethod")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_payment_method", referencedColumnName="id_payment_method", nullable=true)
     * })
     */
    private $paymentMethod;

    /**
     * @var ObCurrency
     *
     * @ORM\ManyToOne(targetEntity="ObCurrency")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_currency", referencedColumnName="id_currency", nullable=true)
     * })
     */
    private $currency;

    /**
     * @var ObBillingAddress
     *
     * @ORM\ManyToOne(targetEntity="ObBillingAddress")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_billing_address", referencedColumnName="id_billing_address", nullable=true)
     * })
     */
    private $address;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param string $number
     * @return ObInvoice
     */
    public function setNumber($number)
    {
        $this->number = $number;
    
        return $this;
    }

    /**
     * Get number
     *
     * @return string 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ObInvoice
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set amount
     *
     * @param string $amount
     * @return ObInvoice
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return string 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set vatPercentage
     *
     * @param string $vatPercentage
     * @return ObInvoice
     */
    public function setVatPercentage($vatPercentage)
    {
        $this->vatPercentage = $vatPercentage;
    
        return $this;
    }

    /**
     * Get vatPercentage
     *
     * @return string 
     */
    public function getVatPercentage()
    {
        return $this->vatPercentage;
    }

    /**
     * Set vatAmount
     *
     * @param string $vatAmount
     * @return ObInvoice
     */
    public function setVatAmount($vatAmount)
    {
        $this->vatAmount = $vatAmount;
    
        return $this;
    }

    /**
     * Get vatAmount
     *
     * @return string 
     */
    public function getVatAmount()
    {
        return $this->vatAmount;
    }

    /**
     * Set total
     *
     * @param string $total
     * @return ObInvoice
     */
    public function setTotal($total)
    {
        $this->total = $total;
    
        return $this;
    }

    /**
     * Get total
     *
     * @return string 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return ObInvoice
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set issueDate
     *
     * @param \DateTime $issueDate
     * @return ObInvoice
     */
    public function setIssueDate($issueDate)
    {
        $this->issueDate = $issueDate;
    
        return $this;
    }

    /**
     * Get issueDate
     *
     * @return \DateTime 
     */
    public function getIssueDate()
    {
        return $this->issueDate;
    }

    /**
     * Set dueDate
     *
     * @param \DateTime $dueDate
     * @return ObInvoice
     */
    public function setDueDate($dueDate)
    {
        $this->dueDate = $dueDate;
    
        return $this;
    }

    /**
     * Get dueDate
     *
     * @return \DateTime 
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }


    /**
     * Set account
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $account
     * @return ObInvoice
     */
    public function setAccount(\Leadsius\ApiBundle\Entity\PlAccount $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Leadsius\ApiBundle\Entity\PlAccount 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set paymentMethod
     *
     * @param \Leadsius\ApiBundle\Entity\ObPaymentMethod $paymentMethod
     * @return ObInvoice
     */
    public function setPaymentMethod(\Leadsius\ApiBundle\Entity\ObPaymentMethod $paymentMethod = null)
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * Get paymentMethod
     *
     * @return \Leadsius\ApiBundle\Entity\ObPaymentMethod 
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * Set currency
     *
     * @param \Leadsius\ApiBundle\Entity\ObCurrency $currency
     * @return ObInvoice
     */
    public function setCurrency(\Leadsius\ApiBundle\Entity\ObCurrency $currency = null)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return \Leadsius\ApiBundle\Entity\ObCurrency 
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set address
     *
     * @param \Leadsius\ApiBundle\Entity\ObBillingAddress $address
     * @return ObInvoice
     */
    public function setAddress(\Leadsius\ApiBundle\Entity\ObBillingAddress $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return \Leadsius\ApiBundle\Entity\ObBillingAddress 
     */
    public function getAddress()
    {
        return $this->address;
    }
}
