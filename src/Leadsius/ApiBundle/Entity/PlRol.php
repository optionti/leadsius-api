<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\PlRolRepository")
 * @ORM\Table(name="pl_rol")
 * @ExclusionPolicy("all")
 */
class PlRol
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_rol", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var PlPermission
     *
     * @ORM\ManyToMany(targetEntity="PlPermission", inversedBy="idRol", cascade={"persist"})
     * @ORM\JoinTable(name="pl_rol_has_permission",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_rol", referencedColumnName="id_rol")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_permission", referencedColumnName="id_permission")
     *   }
     * )
     */
    private $permission;

    /**
     * @var PlUser
     *
     * @ORM\ManyToMany(targetEntity="PlUser", mappedBy="idRol")
     */
    private $user;

    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount", inversedBy="roles")
     * @ORM\JoinColumn(name="id_account", referencedColumnName="id_account", nullable=false)
     */
    private $account;
    
    /**
     * @var string
     *
     * @ORM\Column(name="rol_name", type="string", length=45, nullable=false)
     * @Expose
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="rol_description", type="string", length=255, nullable=true)
     * @Expose
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="rol_is_editable", type="datetime", nullable=false)
     * @Expose
     */
    private $isEditable;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Expose
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @Expose
     */
    private $updated;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set isEditable
     *
     * @param boolean $isEditable
     */
    public function setIsEditable($isEditable)
    {
        $this->isEditable = $isEditable;
    }

    /**
     * Get isEditable
     *
     * @return boolean 
     */
    public function getIsEditable()
    {
        return $this->isEditable;
    }

    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->permission = new \Doctrine\Common\Collections\ArrayCollection();
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add permission
     *
     * @param \Leadsius\ApiBundle\Entity\PlPermission $permission
     * @return PlRol
     */
    public function addPermission(\Leadsius\ApiBundle\Entity\PlPermission $permission)
    {
        $this->permission[] = $permission;

        return $this;
    }

    /**
     * Remove permission
     *
     * @param \Leadsius\ApiBundle\Entity\PlPermission $permission
     */
    public function removePermission(\Leadsius\ApiBundle\Entity\PlPermission $permission)
    {
        $this->permission->removeElement($permission);
    }

    /**
     * Get permission
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPermission()
    {
        return $this->permission;
    }

    /**
     * Add user
     *
     * @param \Leadsius\ApiBundle\Entity\PlUser $user
     * @return PlRol
     */
    public function addUser(\Leadsius\ApiBundle\Entity\PlUser $user)
    {
        $this->user[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \Leadsius\ApiBundle\Entity\PlUser $user
     */
    public function removeUser(\Leadsius\ApiBundle\Entity\PlUser $user)
    {
        $this->user->removeElement($user);
    }

    /**
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set account
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $account
     * @return PlRol
     */
    public function setAccount(\Leadsius\ApiBundle\Entity\PlAccount $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Leadsius\ApiBundle\Entity\PlAccount 
     */
    public function getAccount()
    {
        return $this->account;
    }
}
