<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\PlListRepository")
 * @ORM\Table(name="pl_list")
 * @ExclusionPolicy("all")
 * @Assert\Callback(methods={"validateListFilter"})
 */
class PlList
{
     /**
     * @var integer
     *
     * @ORM\Column(name="id_list", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="list_name", type="string", length=45, nullable=false)
     * @Assert\NotBlank
     * @Expose
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="list_description", type="string", length=255, nullable=true)
     * @Expose
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="list_type", type="string", length=6, nullable=false)
     * @Assert\NotBlank
     * @Assert\Regex(pattern="/^(smart|static)$/")
     * @Expose
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="list_populate", type="string", length=10, nullable=true)
     * @Assert\NotBlank
     * @Assert\Regex(pattern="/^(manually|criteria)$/")
     * @Expose
     */
    private $populate;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Expose
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @Expose
     */
    private $updated;

    /**
     * @var PlFilter
     * 
     * @ORM\ManyToOne(targetEntity="PlFilter")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="list_filter", referencedColumnName="id_filter", nullable=true)
     * })
     * @Expose
     */
    private $listFilter;

    /**
     * @var PlContact
     *
     * @ORM\ManyToMany(targetEntity="PlContact", mappedBy="lists", cascade={"persist"})
     */
    private $contacts;

    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount")
     * @ORM\JoinColumn(name="id_account", referencedColumnName="id_account", nullable=false)
     */
    private $account;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="PlUnsubscribe", mappedBy="list")
     */
    protected $unsubscribe;

    public function __toString() {
        return (string) $this->name;
    }

    /**
     * @param \DateTime $created
     *
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $populate
     *
     * @return $this
     */
    public function setPopulate($populate)
    {
        $this->populate = $populate;
        return $this;
    }

    /**
     * @return string
     */
    public function getPopulate()
    {
        return $this->populate;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param \DateTime $updated
     *
     * @return $this
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->contacts = new ArrayCollection();
        $this->unsubscribe = new ArrayCollection();
    }

    /**
     * Set listFilter
     *
     * @param PlFilter $listFilter
     * @return PlList
     */
    public function setListFilter(PlFilter $listFilter = null)
    {
        $this->listFilter = $listFilter;

        return $this;
    }

    /**
     * Get listFilter
     *
     * @return PlFilter 
     */
    public function getListFilter()
    {
        return $this->listFilter;
    }

    /**
     * Set account
     *
     * @param PlAccount $account
     * @return PlList
     */
    public function setAccount(PlAccount $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return PlAccount 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Add unsubscribe
     *
     * @param PlUnsubscribe $unsubscribe
     * @return PlList
     */
    public function addUnsubscribe(PlUnsubscribe $unsubscribe)
    {
        $this->unsubscribe[] = $unsubscribe;

        return $this;
    }

    /**
     * Remove unsubscribe
     *
     * @param PlUnsubscribe $unsubscribe
     */
    public function removeUnsubscribe(PlUnsubscribe $unsubscribe)
    {
        $this->unsubscribe->removeElement($unsubscribe);
    }

    /**
     * Get unsubscribe
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUnsubscribe()
    {
        return $this->unsubscribe;
    }

    /**
     * Add contacts
     *
     * @param PlContact $contacts
     * @return PlList
     */
    public function addContact(PlContact $contacts)
    {
        $contacts->addList($this);
        $this->contacts[] = $contacts;

        return $this;
    }

    /**
     * Remove contacts
     *
     * @param PlContact $contacts
     */
    public function removeContact(PlContact $contacts)
    {
        $this->contacts->removeElement($contacts);
    }

    /**
     * Get contacts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getContacts()
    {
        return $this->contacts;
    }
    
    /**
     * @Assert\Callback
     */
    public function validateListFilter(ExecutionContextInterface $context)
    {
        if ($this->type==='smart' && $this->listFilter ===null) {
            $context->addViolationAt(
                'listFilter',
                'Declare the Variable listFilter',
                array(),
                null
            );
        }
    }
}
