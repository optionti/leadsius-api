<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\ObSubscriptionRepository")
 * @ORM\Table(name="ob_subscription")
 * @ExclusionPolicy("all")
 */
class ObSubscription {
    /**
     * @var integer
     *
     * @ORM\Column(name="id_subscription", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="subscription_order_number", type="string", length=45, nullable=false)
     *
     * @Expose
     */
    private $orderNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="subscription_billing_frequency", type="string", length=45, nullable=false)
     *
     * @Expose
     */
    private $billingFrequency;

    /**
     * @var boolean
     *
     * @ORM\Column(name="subscription_automatic_renewal", type="boolean", nullable=false)
     *
     * @Expose
     */
    private $automaticRenewal;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="subscription_start_date", type="datetime", nullable=false)
     *
     * @Expose
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="subscription_start_date_current_period", type="datetime", nullable=false)
     *
     * @Expose
     */
    private $startDateCurrentPeriod;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="subscription_end_date", type="datetime", nullable=false)
     *
     * @Expose
     */
    private $endDate;

    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount", inversedBy="user")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_account", referencedColumnName="id_account", nullable=false)
     * })
     */
    protected $account;

    /**
     * @var ObBillingAddress
     *
     * @ORM\ManyToOne(targetEntity="ObBillingAddress")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_billing_address", referencedColumnName="id_billing_address", nullable=false)
     * })
     */
    private $billingAddress;

    /**
     * @var ObDiscount
     *
     * @ORM\ManyToOne(targetEntity="ObDiscount")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_discount", referencedColumnName="id_discount", nullable=true)
     * })
     */
    private $discount;

    /**
     * @var ObPaymentMethod
     *
     * @ORM\ManyToOne(targetEntity="ObPaymentMethod")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_payment_method", referencedColumnName="id_payment_method", nullable=false)
     * })
     */
    private $paymentMethod;

    /**
     * @var ObPaymentInfo
     *
     * @ORM\ManyToOne(targetEntity="ObPaymentInfo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_payment_info", referencedColumnName="id_payment_info", nullable=false)
     * })
     */
    private $paymentInfo;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set orderNumber
     *
     * @param string $orderNumber
     * @return ObSubscription
     */
    public function setOrderNumber($orderNumber) {
        $this->orderNumber = $orderNumber;

        return $this;
    }

    /**
     * Get orderNumber
     *
     * @return string
     */
    public function getOrderNumber() {
        return $this->orderNumber;
    }

    /**
     * Set billingFrequency
     *
     * @param string $billingFrequency
     * @return ObSubscription
     */
    public function setBillingFrequency($billingFrequency) {
        $this->billingFrequency = $billingFrequency;

        return $this;
    }

    /**
     * Get billingFrequency
     *
     * @return string
     */
    public function getBillingFrequency() {
        return $this->billingFrequency;
    }

    /**
     * Set automaticRenewal
     *
     * @param boolean $automaticRenewal
     * @return ObSubscription
     */
    public function setAutomaticRenewal($automaticRenewal) {
        $this->automaticRenewal = $automaticRenewal;

        return $this;
    }

    /**
     * Get automaticRenewal
     *
     * @return boolean
     */
    public function getAutomaticRenewal() {
        return $this->automaticRenewal;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return ObSubscription
     */
    public function setStartDate($startDate) {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate() {
        return $this->startDate;
    }

    /**
     * Set startDateCurrentPeriod
     *
     * @param \DateTime $startDateCurrentPeriod
     * @return ObSubscription
     */
    public function setStartDateCurrentPeriod($startDateCurrentPeriod) {
        $this->startDateCurrentPeriod = $startDateCurrentPeriod;

        return $this;
    }

    /**
     * Get startDateCurrentPeriod
     *
     * @return \DateTime
     */
    public function getStartDateCurrentPeriod() {
        return $this->startDateCurrentPeriod;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return ObSubscription
     */
    public function setEndDate($endDate) {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate() {
        return $this->endDate;
    }


    /**
     * Set account
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $account
     * @return ObSubscription
     */
    public function setAccount(\Leadsius\ApiBundle\Entity\PlAccount $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Leadsius\ApiBundle\Entity\PlAccount 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set billingAddress
     *
     * @param \Leadsius\ApiBundle\Entity\ObBillingAddress $billingAddress
     * @return ObSubscription
     */
    public function setBillingAddress(\Leadsius\ApiBundle\Entity\ObBillingAddress $billingAddress)
    {
        $this->billingAddress = $billingAddress;

        return $this;
    }

    /**
     * Get billingAddress
     *
     * @return \Leadsius\ApiBundle\Entity\ObBillingAddress 
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * Set discount
     *
     * @param \Leadsius\ApiBundle\Entity\ObDiscount $discount
     * @return ObSubscription
     */
    public function setDiscount(\Leadsius\ApiBundle\Entity\ObDiscount $discount = null)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return \Leadsius\ApiBundle\Entity\ObDiscount 
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set paymentMethod
     *
     * @param \Leadsius\ApiBundle\Entity\ObPaymentMethod $paymentMethod
     * @return ObSubscription
     */
    public function setPaymentMethod(\Leadsius\ApiBundle\Entity\ObPaymentMethod $paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * Get paymentMethod
     *
     * @return \Leadsius\ApiBundle\Entity\ObPaymentMethod 
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * Set paymentInfo
     *
     * @param \Leadsius\ApiBundle\Entity\ObPaymentInfo $paymentInfo
     * @return ObSubscription
     */
    public function setPaymentInfo(\Leadsius\ApiBundle\Entity\ObPaymentInfo $paymentInfo)
    {
        $this->paymentInfo = $paymentInfo;

        return $this;
    }

    /**
     * Get paymentInfo
     *
     * @return \Leadsius\ApiBundle\Entity\ObPaymentInfo 
     */
    public function getPaymentInfo()
    {
        return $this->paymentInfo;
    }
}
