<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\PlDepartmentRepository")
 * @ORM\Table(name="pl_department")
 * @ExclusionPolicy("all")
 */
class PlDepartment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_department", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount", inversedBy="idDepartment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_account", referencedColumnName="id_account", nullable=false)
     * })
     */
    protected $parentDepartment;

    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount", inversedBy="departments")
     * @ORM\JoinColumn(name="id_account", referencedColumnName="id_account", nullable=false)
     */
    protected $account;

    /**
     * @ORM\OneToMany(targetEntity="PlUser", mappedBy="idDepartment")
     */
    protected $user;
    
    /**
     * @var string
     *
     * @ORM\Column(name="department_name", type="string", length=45, nullable=true)
     * @Expose
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="department_description", type="string", length=45, nullable=true)
     * @Expose
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Expose
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @Expose
     */
    private $updated;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $departmentName
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param text $description
     */
    public function setDepartmentDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return text 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set description
     *
     * @param string $description
     * @return PlDepartment
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Set parentDepartment
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $parentDepartment
     * @return PlDepartment
     */
    public function setParentDepartment(\Leadsius\ApiBundle\Entity\PlAccount $parentDepartment)
    {
        $this->parentDepartment = $parentDepartment;

        return $this;
    }

    /**
     * Get parentDepartment
     *
     * @return \Leadsius\ApiBundle\Entity\PlAccount 
     */
    public function getParentDepartment()
    {
        return $this->parentDepartment;
    }

    /**
     * Set account
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $account
     * @return PlDepartment
     */
    public function setAccount(\Leadsius\ApiBundle\Entity\PlAccount $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Leadsius\ApiBundle\Entity\PlAccount 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Add user
     *
     * @param \Leadsius\ApiBundle\Entity\PlUser $user
     * @return PlDepartment
     */
    public function addUser(\Leadsius\ApiBundle\Entity\PlUser $user)
    {
        $this->user[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \Leadsius\ApiBundle\Entity\PlUser $user
     */
    public function removeUser(\Leadsius\ApiBundle\Entity\PlUser $user)
    {
        $this->user->removeElement($user);
    }

    /**
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUser()
    {
        return $this->user;
    }
}
