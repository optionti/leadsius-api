<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\ObTransactionRepository")
 * @ORM\Table(name="ob_transaction")
 * @ExclusionPolicy("all")
 */
class ObTransaction {
    /**
     * @var integer
     *
     * @ORM\Column(name="id_transaction", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;

    /**
     * @var ObTransaction
     *
     * @ORM\ManyToOne(targetEntity="ObTransaction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="transaction_id_transaction", referencedColumnName="id_transaction", nullable=true)
     * })
     */
    private $transaction;

    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount", inversedBy="user")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_account", referencedColumnName="id_account", nullable=false)
     * })
     */
    protected $account;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    

    /**
     * Set transaction
     *
     * @param \Leadsius\ApiBundle\Entity\ObTransaction $transaction
     * @return ObTransaction
     */
    public function setTransaction(\Leadsius\ApiBundle\Entity\ObTransaction $transaction = null)
    {
        $this->transaction = $transaction;

        return $this;
    }

    /**
     * Get transaction
     *
     * @return \Leadsius\ApiBundle\Entity\ObTransaction 
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * Set account
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $account
     * @return ObTransaction
     */
    public function setAccount(\Leadsius\ApiBundle\Entity\PlAccount $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Leadsius\ApiBundle\Entity\PlAccount 
     */
    public function getAccount()
    {
        return $this->account;
    }
}
