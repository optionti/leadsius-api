<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\PlBounceRepository")
 * @ORM\Table(name="pl_bounce")
 * @ExclusionPolicy("all")
 */
class PlBounce
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_bounce", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var PlContact
     *
     * @ORM\ManyToOne(targetEntity="PlContact")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contact", referencedColumnName="id_contact", nullable=false)
     * })
     */
    protected $contact;
    
    /**
     * @var PlList
     *
     * @ORM\ManyToOne(targetEntity="PlList")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_list", referencedColumnName="id_list", nullable=true)
     * })
     */
    protected $list;

    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_account", referencedColumnName="id_account", nullable=false)
     * })
     */
    protected $account;

    /**
     * @var MaEmailCampaign
     *
     * @ORM\ManyToOne(targetEntity="MaEmailCampaign")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_email_campaign", referencedColumnName="id_email_campaign", nullable=false)
     * })
     */
    protected $emailCampaign;

    /**
     * @var string
     *
     * @ORM\Column(name="bounce_time", type="integer", nullable=true)
     * @Expose
     */
    protected $time;

    /**
     * @var string
     *
     * @ORM\Column(name="bounce_rule", type="string", length=255, nullable=true)
     * @Expose
     */
    protected $rule;
    
    /**
     * @var string
     *
     * @ORM\Column(name="bounce_type", type="string", length=255, nullable=true)
     * @Expose
     */
    protected $type;

   /**
     * @var datetime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Expose
     */
    protected $created;

    /**
     * @var datetime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @Expose
     */
    protected $updated;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set time
     *
     * @param integer $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * Get time
     *
     * @return integer 
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set rule
     *
     * @param string $rule
     */
    public function setRule($rule)
    {
        $this->rule = $rule;
    }

    /**
     * Get rule
     *
     * @return string 
     */
    public function getRule()
    {
        return $this->rule;
    }

    /**
     * Set type
     *
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created) {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
	public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated) {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }
    

    /**
     * Set contact
     *
     * @param \Leadsius\ApiBundle\Entity\PlContact $contact
     * @return PlBounce
     */
    public function setContact(\Leadsius\ApiBundle\Entity\PlContact $contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return \Leadsius\ApiBundle\Entity\PlContact 
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set list
     *
     * @param \Leadsius\ApiBundle\Entity\PlList $list
     * @return PlBounce
     */
    public function setList(\Leadsius\ApiBundle\Entity\PlList $list = null)
    {
        $this->list = $list;

        return $this;
    }

    /**
     * Get list
     *
     * @return \Leadsius\ApiBundle\Entity\PlList 
     */
    public function getList()
    {
        return $this->list;
    }

    /**
     * Set account
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $account
     * @return PlBounce
     */
    public function setAccount(\Leadsius\ApiBundle\Entity\PlAccount $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Leadsius\ApiBundle\Entity\PlAccount 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set emailCampaign
     *
     * @param \Leadsius\ApiBundle\Entity\MaEmailCampaign $emailCampaign
     * @return PlBounce
     */
    public function setEmailCampaign(\Leadsius\ApiBundle\Entity\MaEmailCampaign $emailCampaign)
    {
        $this->emailCampaign = $emailCampaign;

        return $this;
    }

    /**
     * Get emailCampaign
     *
     * @return \Leadsius\ApiBundle\Entity\MaEmailCampaign 
     */
    public function getEmailCampaign()
    {
        return $this->emailCampaign;
    }
}
