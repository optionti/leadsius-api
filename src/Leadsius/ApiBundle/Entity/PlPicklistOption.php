<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\PlPicklistOptionRepository")
 * @ORM\Table(name="pl_picklist_option")
 * @ExclusionPolicy("all")
 */
class PlPicklistOption
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_picklist_option", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="picklist_option_name", type="string", length=45, nullable=true)
     * @Expose
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Expose
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @Expose
     */
    private $updated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted", type="datetime", nullable=true)
     * @Expose
     */
    private $deleted;

    /**
     * @var PlPicklist
     *
     * @ORM\ManyToOne(targetEntity="PlPicklist")
     * @ORM\JoinColumn(name="id_picklist", referencedColumnName="id_picklist", nullable=false)
     */
    protected $picklist;
    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount")
     * @ORM\JoinColumn(name="id_account", referencedColumnName="id_account", nullable=false)
     */
    protected $account;

    /**
     * @var PlUser
     *
     * @ORM\ManyToOne(targetEntity="PlUser")
     * @ORM\JoinColumn(name="id_user", referencedColumnName="id_user", nullable=true)
     */
    private $user;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="MaProgram", mappedBy="category")
     */
    protected $programs;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->programs = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getname()
    {
        return $this->name;
    }

    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set deleted
     *
     * @param datetime $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * Get deleted
     *
     * @return datetime 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set picklist
     *
     * @param \Leadsius\ApiBundle\Entity\PlPicklist $picklist
     * @return PlPicklistOption
     */
    public function setPicklist(\Leadsius\ApiBundle\Entity\PlPicklist $picklist)
    {
        $this->picklist = $picklist;

        return $this;
    }

    /**
     * Get picklist
     *
     * @return \Leadsius\ApiBundle\Entity\PlPicklist 
     */
    public function getPicklist()
    {
        return $this->picklist;
    }

    /**
     * Set account
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $account
     * @return PlPicklistOption
     */
    public function setAccount(\Leadsius\ApiBundle\Entity\PlAccount $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Leadsius\ApiBundle\Entity\PlAccount 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set user
     *
     * @param \Leadsius\ApiBundle\Entity\PlUser $user
     * @return PlPicklistOption
     */
    public function setUser(\Leadsius\ApiBundle\Entity\PlUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Leadsius\ApiBundle\Entity\PlUser 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add programs
     *
     * @param \Leadsius\ApiBundle\Entity\MaProgram $programs
     * @return PlPicklistOption
     */
    public function addProgram(\Leadsius\ApiBundle\Entity\MaProgram $programs)
    {
        $this->programs[] = $programs;

        return $this;
    }

    /**
     * Remove programs
     *
     * @param \Leadsius\ApiBundle\Entity\MaProgram $programs
     */
    public function removeProgram(\Leadsius\ApiBundle\Entity\MaProgram $programs)
    {
        $this->programs->removeElement($programs);
    }

    /**
     * Get programs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPrograms()
    {
        return $this->programs;
    }
}
