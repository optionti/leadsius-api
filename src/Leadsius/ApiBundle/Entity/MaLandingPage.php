<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\MaLandingPageRepository")
 * @ORM\Table(name="ma_landing_page")
 * @ExclusionPolicy("all")
 */
class MaLandingPage {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_landing_page", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="landing_page_name", type="string", length=255, nullable=false)
     *
     * @Expose
     */
    private $name;

    
    /**
     * @var text
     *
     * @ORM\Column(name="landing_page_content", type="text", nullable=true)
     *
     * @Expose
     */
    private $content;
    
    /**
     * @var text
     *
     * @ORM\Column(name="landing_page_url", type="text", nullable=true)
     *
     * @Expose
     */
    private $url;

    /**
     * @var text
     *
     * @ORM\Column(name="landing_page_tags", type="text", nullable=true)
     *
     * @Expose
     */
    private $tags;

    /**
     * @var datetime
     *
     * @ORM\Column(name="landing_page_from", type="datetime", nullable=true)
     *
     * @Expose
     */
    private $from;

    /**
     * @var datetime
     *
     * @ORM\Column(name="landing_page_until", type="datetime", nullable=true)
     *
     * @Expose
     */
    private $until;

    /**
     * @var boolean
     *
     * @ORM\Column(name="landing_page_active", type="boolean", nullable=true)
     *
     * @Expose
     */
    private $active;

    /**
     * @var text
     *
     * @ORM\Column(name="landing_page_meta_keywords", type="text", nullable=true)
     *
     * @Expose
     */
    private $metaKeywords;
    
    /**
     * @var text
     *
     * @ORM\Column(name="landing_page_meta_description", type="text", nullable=true)
     *
     * @Expose
     */
    private $metaDescription;
    
    /**
     * @var text
     *
     * @ORM\Column(name="landing_page_meta_title", type="text", nullable=true)
     *
     * @Expose
     */
    private $metaTitle;
    
    /**
     * @var text
     *
     * @ORM\Column(name="landing_page_tracking", type="text", nullable=true)
     *
     * @Expose
     */
    private $metaTracking;
    
    /**
     * @var datetime $created
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     *
     * @Expose
     */
    protected $created;

    /**
     * @var datetime $updated
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     *
     * @Expose
     */
    protected $updated;

    /**
     * @var datetime $deleted
     *
     * @ORM\Column(name="deleted", type="datetime", nullable=true)
     *
     * @Expose
     */
    protected $deleted;
    
    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_account", referencedColumnName="id_account", nullable=false)
     * })
     */
    protected $account;

    /**
     * @var PlUser
     *
     * @ORM\ManyToOne(targetEntity="PlUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id_user", nullable=true)
     * })
     */
    private $user;
    
    /**
     * @var MaProgram
     *
     * @ORM\ManyToOne(targetEntity="MaProgram", inversedBy="landingPage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_program", referencedColumnName="id_program", nullable=true)
     * })
     */
    protected $program;

    /**
     * @var PlPicklistOption
     *
     * @ORM\ManyToOne(targetEntity="PlPicklistOption")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="landingpage_category", referencedColumnName="id_picklist_option", nullable=true)
     * })
     */
    private $category;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set content
     *
     * @param string $content
     */
    public function setContent($content) {
        $this->content = $content;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Set url
     *
     * @param string $url
     */
    public function setUrl($url) {
        $this->url = $url;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl() {
        return $this->url;
    }

    /**
     * Set tags
     *
     * @param string $landingPagelTags
     */
    public function setTags($tags) {
        $this->tags = $tags;
    }

    /**
     * Get tags
     *
     * @return string 
     */
    public function getTags() {
        return $this->tags;
    }

    /**
     * Set from
     *
     * @param datetime $from
     */
    public function setFrom($from) {
        $this->from = $from;
    }

    /**
     * Get from
     *
     * @return datetime 
     */
    public function getFrom() {
        return $this->from;
    }

    /**
     * Set until
     *
     * @param datetime $until
     */
    public function setUntil($until) {
        $this->until = $until;
    }

    /**
     * Get until
     *
     * @return datetime 
     */
    public function getUntil() {
        return $this->until;
    }

    /**
     * Set active
     *
     * @param boolean $active
     */
    public function setActive($active) {
        $this->active = $active;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive() {
        return $this->active;
    }

    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created) {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
	public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated) {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set deleted
     *
     * @param datetime $deleted
     */
    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }
	
    /**
     * Get deleted
     *
     * @return datetime 
     */
    public function getDeleted() {
        return $this->deleted;
    }

    /**
     * Set metaKeywords
     *
     * @param text $metaKeywords
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;
    }

    /**
     * Get metaKeywords
     *
     * @return text 
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * Set metaDescription
     *
     * @param text $metaDescription
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    }

    /**
     * Get metaDescription
     *
     * @return text 
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set metaTitle
     *
     * @param text $metaTitle
     */
    public function setMetaTitle($metaTitle)
    {
        $this->metaTitle = $metaTitle;
    }

    /**
     * Get metaTitle
     *
     * @return text 
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * Set metaTracking
     *
     * @param text $metaTracking
     */
    public function setMetaTracking($metaTracking)
    {
        $this->metaTracking = $metaTracking;
    }

    /**
     * Get metaTracking
     *
     * @return text 
     */
    public function getMetaTracking()
    {
        return $this->metaTracking;
    }

    /**
     * Set account
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $account
     * @return MaLandingPage
     */
    public function setAccount(\Leadsius\ApiBundle\Entity\PlAccount $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Leadsius\ApiBundle\Entity\PlAccount 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set user
     *
     * @param \Leadsius\ApiBundle\Entity\PlUser $user
     * @return MaLandingPage
     */
    public function setUser(\Leadsius\ApiBundle\Entity\PlUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Leadsius\ApiBundle\Entity\PlUser 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set program
     *
     * @param \Leadsius\ApiBundle\Entity\MaProgram $program
     * @return MaLandingPage
     */
    public function setProgram(\Leadsius\ApiBundle\Entity\MaProgram $program = null)
    {
        $this->program = $program;

        return $this;
    }

    /**
     * Get program
     *
     * @return \Leadsius\ApiBundle\Entity\MaProgram 
     */
    public function getProgram()
    {
        return $this->program;
    }

    /**
     * Set category
     *
     * @param \Leadsius\ApiBundle\Entity\PlPicklistOption $category
     * @return MaLandingPage
     */
    public function setCategory(\Leadsius\ApiBundle\Entity\PlPicklistOption $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Leadsius\ApiBundle\Entity\PlPicklistOption 
     */
    public function getCategory()
    {
        return $this->category;
    }
}
