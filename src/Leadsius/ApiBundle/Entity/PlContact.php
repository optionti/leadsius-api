<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\PlContactRepository")
 * @ORM\Table(name="pl_contact")
 * @JMS\ExclusionPolicy("all")
 * @UniqueEntity(fields={"account","email"})
 */
class PlContact
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_contact", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Expose
     * @JMS\Groups({"list"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_firstname", type="string", length=45, nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"list"})
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_lastname", type="string", length=45, nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"list"})
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_email", type="string", length=255, unique=true)
     * @Assert\NotBlank
     * @JMS\Expose
     * @JMS\Groups({"list"})
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_phone", type="string", length=45, nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"list"})
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_mobile_phone", type="string", length=45, nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"list"})
     */
    private $mobilePhone;

    /**
     * @var boolean
     *
     * @ORM\Column(name="contact_do_not_call", type="boolean", nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"list"})
     */
    private $doNotCall;

    /**
     * @var boolean
     *
     * @ORM\Column(name="contact_do_not_email", type="boolean", nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"list"})
     */
    private $doNotEmail;

    /**
     * @var boolean
     *
     * @ORM\Column(name="contact_do_not_email_until", type="datetime", nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"list"})
     */
    private $doNotEmailUntil;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_bounce", type="boolean", nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"list"})
     */
    private $bounce;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_reports_to", type="string", length=45, nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"list"})
     */
    private $reportsTo;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_title", type="string", length=45, nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"list"})
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_role", type="string", length=45, nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"list"})
     */
    private $role;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_description", type="text", nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"list"})
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_other", type="text", nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"list"})
     */
    private $other;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @JMS\Expose
     * @JMS\Groups({"list"})
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @JMS\Expose
     * @JMS\Groups({"list"})
     */
    private $updated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted", type="datetime", nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"list"})
     */
    private $deleted;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_source", type="string", length=45, nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"list"})
     */
    private $source;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_user_account", type="string", length=45, nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"list"})
     */
    private $userAccount;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_status", type="string", length=45, nullable=true)
     * @JMS\Expose
     * @JMS\Groups({"list"})
     */
    private $status;

    /**
     * @var PlCompany
     *
     * @ORM\ManyToOne(targetEntity="PlCompany", inversedBy="contacts")
     * @ORM\JoinColumn(name="id_company", referencedColumnName="id_company")
     * @JMS\Groups({"details"})
     * @JMS\MaxDepth(1)
     */
    private $company;

    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount", inversedBy="contacts")
     * @ORM\JoinColumn(name="id_account", referencedColumnName="id_account", nullable=false)
     * @JMS\Groups({"details"})
     * @JMS\MaxDepth(1)
     */
    private $account;
    
    /**
     * @var PlUser
     *
     * @ORM\ManyToOne(targetEntity="PlUser", inversedBy="contacts")
     * @ORM\JoinColumn(name="id_user_assigned_to", referencedColumnName="id_user", nullable=true)
     * @JMS\Groups({"details"})
     * @JMS\MaxDepth(1)
     */
    private $userAssignedTo;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="MaEmailCampaignLog", mappedBy="contact")
     */
    private $emailCampaignLogs;

    /**
     * @var PlPicklistOption
     *
     * @ORM\ManyToOne(targetEntity="PlPicklistOption")
     * @ORM\JoinColumn(name="department", referencedColumnName="id_picklist_option", nullable=true)
     * @JMS\Groups({"details"})
     * @JMS\MaxDepth(1)
     */
    private $departmentOption;

    /**
     * @var PlPicklistOption
     *
     * @ORM\ManyToOne(targetEntity="PlPicklistOption")
     * @ORM\JoinColumn(name="stage", referencedColumnName="id_picklist_option", nullable=true)
     * @JMS\Groups({"details"})
     * @JMS\MaxDepth(1)
     */
    private $stageOption;

    /**
     * @var PlPicklistOption
     *
     * @ORM\ManyToOne(targetEntity="PlPicklistOption")
     * @ORM\JoinColumn(name="status", referencedColumnName="id_picklist_option", nullable=true)
     * @JMS\Groups({"details"})
     * @JMS\MaxDepth(1)
     */
    private $statusOption;

    /**
     * @var PlPicklistOption
     *
     * @ORM\ManyToOne(targetEntity="PlPicklistOption")
     * @ORM\JoinColumn(name="source", referencedColumnName="id_picklist_option", nullable=true)
     */
    private $sourceOption;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="PlList", inversedBy="contacts")
     * @ORM\JoinTable(name="pl_contact_has_pl_list",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_contact", referencedColumnName="id_contact")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_list", referencedColumnName="id_list")
     *   }
     * )
     */
    protected $lists;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="PlDynamicFields", mappedBy="contact")
     * @JMS\Groups({"details"})
     * @JMS\MaxDepth(1)
     */
    private $dynamicFields;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="PlUnsubscribe", mappedBy="contact")
     * @JMS\Expose
     * @JMS\Groups({"details"})
     * @JMS\MaxDepth(1)
     */
    protected $unsubscribe;

    /**
     * @var Array
     * @JMS\Expose
     */
    protected $activitiesSummary;
    
    public function __construct()
    {
        $this->doNotCall = false;
        $this->doNotEmail = false;
        $this->bounce = false;
        $this->userAccount = null;

        $this->lists = new ArrayCollection();
        $this->emailCampaignLogs = new ArrayCollection();
        $this->dynamicFields = new ArrayCollection();
        $this->unsubscribe = new ArrayCollection();
        $this->activitiesSummary = Array();
    }

    public function __toString()
    {
        return $this->completeName();
    }

    public function completeName()
    {
        return $this->firstname . ' ' . $this->lastname;
    }

    /**
     * @param string $bounce
     *
     * @return $this
     */
    public function setBounce($bounce)
    {
        $this->bounce = $bounce;
        return $this;
    }

    /**
     * @return string
     */
    public function getBounce()
    {
        return $this->bounce;
    }

    /**
     * @param \DateTime $created
     *
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $deleted
     *
     * @return $this
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param string $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param boolean $doNotCall
     *
     * @return $this
     */
    public function setDoNotCall($doNotCall)
    {
        $this->doNotCall = $doNotCall;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getDoNotCall()
    {
        return $this->doNotCall;
    }

    /**
     * @param boolean $doNotEmail
     *
     * @return $this
     */
    public function setDoNotEmail($doNotEmail)
    {
        $this->doNotEmail = $doNotEmail;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getDoNotEmail()
    {
        return $this->doNotEmail;
    }

    /**
     * @param boolean $doNotEmailUntil
     *
     * @return $this
     */
    public function setDoNotEmailUntil($doNotEmailUntil)
    {
        $this->doNotEmailUntil = $doNotEmailUntil;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getDoNotEmailUntil()
    {
        return $this->doNotEmailUntil;
    }

    /**
     * @param string $email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $firstname
     *
     * @return $this
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $lastname
     *
     * @return $this
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param string $mobilePhone
     *
     * @return $this
     */
    public function setMobilePhone($mobilePhone)
    {
        $this->mobilePhone = $mobilePhone;
        return $this;
    }

    /**
     * @return string
     */
    public function getMobilePhone()
    {
        return $this->mobilePhone;
    }

    /**
     * @param string $other
     *
     * @return $this
     */
    public function setOther($other)
    {
        $this->other = $other;
        return $this;
    }

    /**
     * @return string
     */
    public function getOther()
    {
        return $this->other;
    }

    /**
     * @param string $phone
     *
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $reportsTo
     *
     * @return $this
     */
    public function setReportsTo($reportsTo)
    {
        $this->reportsTo = $reportsTo;
        return $this;
    }

    /**
     * @return string
     */
    public function getReportsTo()
    {
        return $this->reportsTo;
    }

    /**
     * @param string $role
     *
     * @return $this
     */
    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string $source
     *
     * @return $this
     */
    public function setSource($source)
    {
        $this->source = $source;
        return $this;
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param string $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param \DateTime $updated
     *
     * @return $this
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param string $userAccount
     *
     * @return $this
     */
    public function setUserAccount($userAccount)
    {
        $this->userAccount = $userAccount;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserAccount()
    {
        return $this->userAccount;
    }

    /**
     * Set company
     *
     * @param PlCompany $company
     * @return PlContact
     */
    public function setCompany(PlCompany $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return PlCompany 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set account
     *
     * @param PlAccount $account
     * @return PlContact
     */
    public function setAccount(PlAccount $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return PlAccount 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set userAssignedTo
     *
     * @param PlUser $userAssignedTo
     * @return PlContact
     */
    public function setUserAssignedTo(PlUser $userAssignedTo = null)
    {
        $this->userAssignedTo = $userAssignedTo;

        return $this;
    }

    /**
     * Get userAssignedTo
     *
     * @return PlUser 
     */
    public function getUserAssignedTo()
    {
        return $this->userAssignedTo;
    }

    /**
     * Add campaignLogs
     *
     * @param MaEmailCampaignLog $campaignLogs
     * @return PlContact
     */
    public function addCampaignLog(MaEmailCampaignLog $campaignLogs)
    {
        $this->campaignLogs[] = $campaignLogs;

        return $this;
    }

    /**
     * Remove campaignLogs
     *
     * @param MaEmailCampaignLog $campaignLogs
     */
    public function removeCampaignLog(MaEmailCampaignLog $campaignLogs)
    {
        $this->campaignLogs->removeElement($campaignLogs);
    }

    /**
     * Get campaignLogs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCampaignLogs()
    {
        return $this->campaignLogs;
    }

    /**
     * Set departmentOption
     *
     * @param PlPicklistOption $departmentOption
     * @return PlContact
     */
    public function setDepartmentOption(PlPicklistOption $departmentOption = null)
    {
        $this->departmentOption = $departmentOption;

        return $this;
    }

    /**
     * Get departmentOption
     *
     * @return PlPicklistOption 
     */
    public function getDepartmentOption()
    {
        return $this->departmentOption;
    }

    /**
     * Set stageOption
     *
     * @param PlPicklistOption $stageOption
     * @return PlContact
     */
    public function setStageOption(PlPicklistOption $stageOption = null)
    {
        $this->stageOption = $stageOption;

        return $this;
    }

    /**
     * Get stageOption
     *
     * @return PlPicklistOption 
     */
    public function getStageOption()
    {
        return $this->stageOption;
    }

    /**
     * Set statusOption
     *
     * @param PlPicklistOption $statusOption
     * @return PlContact
     */
    public function setStatusOption(PlPicklistOption $statusOption = null)
    {
        $this->statusOption = $statusOption;

        return $this;
    }

    /**
     * Get statusOption
     *
     * @return PlPicklistOption 
     */
    public function getStatusOption()
    {
        return $this->statusOption;
    }

    /**
     * Set sourceOption
     *
     * @param PlPicklistOption $sourceOption
     * @return PlContact
     */
    public function setSourceOption(PlPicklistOption $sourceOption = null)
    {
        $this->sourceOption = $sourceOption;

        return $this;
    }

    /**
     * Get sourceOption
     *
     * @return PlPicklistOption 
     */
    public function getSourceOption()
    {
        return $this->sourceOption;
    }

    /**
     * Add lists
     *
     * @param PlList $lists
     * @return PlContact
     */
    public function addList(PlList $lists)
    {
        $this->lists[] = $lists;

        return $this;
    }

    /**
     * Remove lists
     *
     * @param PlList $lists
     */
    public function removeList(PlList $lists)
    {
        $this->lists->removeElement($lists);
    }

    /**
     * Get lists
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLists()
    {
        return $this->lists;
    }

    /**
     * Add dynamicFields
     *
     * @param PlDynamicFields $dynamicFields
     * @return PlContact
     */
    public function addDynamicField(PlDynamicFields $dynamicFields)
    {
        $this->dynamicFields[] = $dynamicFields;

        return $this;
    }

    /**
     * Remove dynamicFields
     *
     * @param PlDynamicFields $dynamicFields
     */
    public function removeDynamicField(PlDynamicFields $dynamicFields)
    {
        $this->dynamicFields->removeElement($dynamicFields);
    }

    /**
     * Get dynamicFields
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDynamicFields()
    {
        return $this->dynamicFields;
    }

    /**
     * Add unsubscribe
     *
     * @param PlUnsubscribe $unsubscribe
     * @return PlContact
     */
    public function addUnsubscribe(PlUnsubscribe $unsubscribe)
    {
        $this->unsubscribe[] = $unsubscribe;

        return $this;
    }

    /**
     * Remove unsubscribe
     *
     * @param PlUnsubscribe $unsubscribe
     */
    public function removeUnsubscribe(PlUnsubscribe $unsubscribe)
    {
        $this->unsubscribe->removeElement($unsubscribe);
    }

    /**
     * Get unsubscribe
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUnsubscribe()
    {
        return $this->unsubscribe;
    }

    /**
     * Add emailCampaignLogs
     *
     * @param MaEmailCampaignLog $emailCampaignLogs
     * @return PlContact
     */
    public function addEmailCampaignLog(MaEmailCampaignLog $emailCampaignLogs)
    {
        $this->emailCampaignLogs[] = $emailCampaignLogs;

        return $this;
    }

    /**
     * Remove emailCampaignLogs
     *
     * @param MaEmailCampaignLog $emailCampaignLogs
     */
    public function removeEmailCampaignLog(MaEmailCampaignLog $emailCampaignLogs)
    {
        $this->emailCampaignLogs->removeElement($emailCampaignLogs);
    }

    /**
     * Get emailCampaignLogs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEmailCampaignLogs()
    {
        return $this->emailCampaignLogs;
    }
    
    /**
     * Get activitiesSummary
     *
     * @return array
     */
    public function getActivitiesSummary() 
    {
        return $this->activitiesSummary;
    }
    
    /**
     * Set activitiesSummary
     *
     * @param array $activitiesSummary
     * @return PlContact
     */
    public function setActivitiesSummary(array $activitiesSummary = null)
    {
        $this->activitiesSummary = $activitiesSummary;

        return $this;
    }
}
