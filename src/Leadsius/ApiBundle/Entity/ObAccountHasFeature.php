<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\ObAccountHasFeatureRepository")
 * @ORM\Table(name="ob_account_has_feature")
 * @ExclusionPolicy("all")
 */
class ObAccountHasFeature {
    /**
     * @var integer
     *
     * @ORM\Column(name="id_account_has_feature", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;

    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount", inversedBy="idUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_account", referencedColumnName="id_account", nullable=false)
     * })
     */
    private $account;

    /**
     * @var ObFeature
     *
     * @ORM\ManyToOne(targetEntity="ObFeature")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_feature", referencedColumnName="id_feature", nullable=true)
     * })
     */
    private $feature;

    /**
     * @var ObPaymentInfo
     *
     * @ORM\ManyToOne(targetEntity="ObPaymentInfo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_payment_info", referencedColumnName="id_payment_info", nullable=true)
     * })
     */
    private $paymentInfo;

    /**
     * @return int
     */
    public function getIdAccountHasFeature() {
        return $this->id;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set account
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $account
     * @return ObAccountHasFeature
     */
    public function setAccount(\Leadsius\ApiBundle\Entity\PlAccount $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Leadsius\ApiBundle\Entity\PlAccount 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set feature
     *
     * @param \Leadsius\ApiBundle\Entity\ObFeature $feature
     * @return ObAccountHasFeature
     */
    public function setFeature(\Leadsius\ApiBundle\Entity\ObFeature $feature = null)
    {
        $this->feature = $feature;

        return $this;
    }

    /**
     * Get feature
     *
     * @return \Leadsius\ApiBundle\Entity\ObFeature 
     */
    public function getFeature()
    {
        return $this->feature;
    }

    /**
     * Set paymentInfo
     *
     * @param \Leadsius\ApiBundle\Entity\ObPaymentInfo $paymentInfo
     * @return ObAccountHasFeature
     */
    public function setPaymentInfo(\Leadsius\ApiBundle\Entity\ObPaymentInfo $paymentInfo = null)
    {
        $this->paymentInfo = $paymentInfo;

        return $this;
    }

    /**
     * Get paymentInfo
     *
     * @return \Leadsius\ApiBundle\Entity\ObPaymentInfo 
     */
    public function getPaymentInfo()
    {
        return $this->paymentInfo;
    }
}
