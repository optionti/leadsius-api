<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\ObCurrencyRepository")
 * @ORM\Table(name="ob_currency")
 * @ExclusionPolicy("all")
 */
class ObCurrency {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_currency", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="currency_name", type="string", length=45, nullable=false)
     *
     * @Expose
     */
    private $name;
    /**
     * @var string
     *
     * @ORM\Column(name="currency_code", type="string", length=45, nullable=false)
     *
     * @Expose
     */
    private $code;
    /**
     * @var ObCountry
     *
     * @ORM\ManyToOne(targetEntity="ObCountry")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_country", referencedColumnName="id_country", nullable=true)
     * })
     */
    private $country;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ObCurrency
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return ObCurrency
     */
    public function setCode($code) {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode() {
        return $this->code;
    }


    /**
     * Set country
     *
     * @param \Leadsius\ApiBundle\Entity\ObCountry $country
     * @return ObCurrency
     */
    public function setCountry(\Leadsius\ApiBundle\Entity\ObCountry $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \Leadsius\ApiBundle\Entity\ObCountry 
     */
    public function getCountry()
    {
        return $this->country;
    }
}
