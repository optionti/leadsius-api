<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\MaEmailCampaignRepository")
 * @ORM\Table(name="ma_email_campaign")
 * @ExclusionPolicy("all")
 * @Assert\Callback(methods={
 *      "validFromAccount",
 *      "validReplyAccount",
 *      "validEmailAccount",
 *      "validListAccount"
 * })
 */
class MaEmailCampaign
{
    const STATUS_PENDING = 'pending';

    /**
     * @var integer 
     *
     * @ORM\Column(name="id_email_campaign", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;

    /**
     * @var string 
     *
     * @ORM\Column(name="email_campaign_name", type="string", length=255, nullable=false)
     *
     * @Expose
     */
    private $name;
    
    /**
     * @var string 
     *
     * @ORM\Column(name="email_campaign_type", type="string", length=255, nullable=false)
     *
     * @Expose
     */
    private $type;

    /**
     * @var string 
     *
     * @ORM\Column(name="email_campaign_description", type="string", length=255, nullable=true)
     *
     * @Expose
     */
    private $description;

    /**
     * @var string 
     *
     * @ORM\Column(name="email_campaign_subject", type="string", length=255, nullable=false)
     *
     * @Expose
     */
    private $subject;

    /**
     * @var string 
     *
     * @ORM\Column(name="email_campaign_sending_date", type="datetime", nullable=false)
     *
     * @Expose
     */
    private $sendingDate;

    /**
     * @var string 
     *
     * @ORM\Column(name="email_campaign_status", type="string", length=255, nullable=false)
     * @Assert\Regex("/^(pending)$/")
     * @Expose
     */
    private $status;

    /**
     * @var datetime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=false)
     *
     * @Expose
     */
    protected $created;

    /**
     * @var datetime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     *
     * @Expose
     */
    protected $updated;

    /**
     * @var datetime $deleted
     *
     * @ORM\Column(name="deleted", type="datetime", nullable=true)
     */
    protected $deleted;

    /**
     * @var MaSystemEmail
     *
     * @ORM\ManyToOne(targetEntity="MaSystemEmail")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="email_campaign_from", referencedColumnName="id_system", nullable=false)
     * })
     */
    private $from;

    /**
     * @var MaSystemEmail
     *
     * @ORM\ManyToOne(targetEntity="MaSystemEmail")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="email_campaign_reply_email", referencedColumnName="id_system", nullable=false)
     * })
     */
    private $reply;

    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_account", referencedColumnName="id_account", nullable=false)
     * })
     */
    protected $account;

    /**
     * @var PlUser
     *
     * @ORM\ManyToOne(targetEntity="PlUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id_user", nullable=true)
     * })
     */    
    private $user;
    
    /**
     * @var MaEmail
     *
     * @ORM\ManyToOne(targetEntity="MaEmail", inversedBy="idEmailCampaign")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_email", referencedColumnName="id_email", nullable=false)
     * })
     */
    protected $email;
    
    /**
     * @var PlList
     *
     * @ORM\ManyToOne(targetEntity="PlList")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_list", referencedColumnName="id_list", nullable=false)
     * })
     */
    protected $list;
    
    /**
     * @var MaProgram
     *
     * @ORM\ManyToOne(targetEntity="MaProgram", inversedBy="idEmailCampaign")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_program", referencedColumnName="id_program", nullable=true)
     * })
     */
    protected $program;

    function __construct()
    {
        $this->status = self::STATUS_PENDING;
    }

    public function validFromAccount(ExecutionContextInterface $context)
    {
        if ($this->getAccount()->getId() !== $this->getFrom()->getAccount()->getId()) {
            $context->addViolationAt('from', 'This account doesn\'t have this from', array(), null);
        }
    }

    public function validReplyAccount(ExecutionContextInterface $context)
    {
        if ($this->getAccount()->getId() !== $this->getReply()->getAccount()->getId()) {
            $context->addViolationAt('reply', 'This account doesn\'t have this reply', array(), null);
        }
    }

    public function validEmailAccount(ExecutionContextInterface $context)
    {
        if ($this->getAccount()->getId() !== $this->getEmail()->getAccount()->getId()) {
            $context->addViolationAt('email', 'This account doesn\'t have this email', array(), null);
        }
    }

    public function validListAccount(ExecutionContextInterface $context)
    {
        if ($this->getAccount()->getId() !== $this->getList()->getAccount()->getId()) {
            $context->addViolationAt('list', 'This account doesn\'t have this list', array(), null);
        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return MaEmailCampaign
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set sendingDate
     *
     * @param datetime $sendingDate
     * @return MaEmailCampaign
     */
    public function setSendingDate($sendingDate)
    {
        $this->sendingDate = $sendingDate;

        return $this;
    }

    /**
     * Get sendingDate
     *
     * @return datetime
     */
    public function getSendingDate()
    {
        return $this->sendingDate;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return MaEmailCampaign
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param datetime $created
     * @return MaEmailCampaign
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return datetime
     */
	public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     * @return MaEmailCampaign
     */
    public function setUpdated($updated) {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return datetime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set deleted
     *
     * @param datetime $deleted
     * @return MaEmailCampaign
     */
    public function setDeleted($deleted) {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return datetime
     */
    public function getDeleted() {
        return $this->deleted;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return MaEmailCampaign
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return MaEmailCampaign
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return MaEmailCampaign
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set from
     *
     * @param \Leadsius\ApiBundle\Entity\MaSystemEmail $from
     * @return MaEmailCampaign
     */
    public function setFrom(\Leadsius\ApiBundle\Entity\MaSystemEmail $from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * Get from
     *
     * @return \Leadsius\ApiBundle\Entity\MaSystemEmail
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Set reply
     *
     * @param \Leadsius\ApiBundle\Entity\MaSystemEmail $reply
     * @return MaEmailCampaign
     */
    public function setReply(\Leadsius\ApiBundle\Entity\MaSystemEmail $reply)
    {
        $this->reply = $reply;

        return $this;
    }

    /**
     * Get Reply
     *
     * @return \Leadsius\ApiBundle\Entity\MaSystemEmail
     */
    public function getReply()
    {
        return $this->reply;
    }

    /**
     * Set account
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $account
     * @return MaEmailCampaign
     */
    public function setAccount(\Leadsius\ApiBundle\Entity\PlAccount $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Leadsius\ApiBundle\Entity\PlAccount
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set user
     *
     * @param \Leadsius\ApiBundle\Entity\PlUser user
     * @return MaEmailCampaign
     */
    public function setUser(\Leadsius\ApiBundle\Entity\PlUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \Leadsius\ApiBundle\Entity\PlUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set email
     *
     * @param \Leadsius\ApiBundle\Entity\MaEmail $email
     * @return MaEmailCampaign
     */
    public function setEmail(\Leadsius\ApiBundle\Entity\MaEmail $email = null)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return \Leadsius\ApiBundle\Entity\MaEmail
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set list
     *
     * @param \Leadsius\ApiBundle\Entity\PlList $list
     * @return MaEmailCampaign
     */
    public function setList(\Leadsius\ApiBundle\Entity\PlList $list = null)
    {
        $this->list = $list;

        return $this;
    }

    /**
     * Get list
     *
     * @return \Leadsius\ApiBundle\Entity\PlList
     */
    public function getList()
    {
        return $this->list;
    }

    /**
     * Set program
     *
     * @param MaProgram $program
     * @return MaEmailCampaign
     */
    public function setProgram(MaProgram $program = null)
    {
        $this->program = $program;

        return $this;
    }

    /**
     * Get idProgram
     *
     * @return \Leadsius\ApiBundle\Entity\MaProgram
     */
    public function getProgram()
    {
        return $this->program;
    }
}
