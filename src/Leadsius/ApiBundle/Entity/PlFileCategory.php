<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\PlFileCategoryRepository")
 * @ORM\Table(name="pl_file_category")
 * @ExclusionPolicy("all")
 */
class PlFileCategory {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_file_category", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var PlFileCategory
     *
     * @ORM\ManyToOne(targetEntity="PlFileCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_parent_file_category", referencedColumnName="id_file_category", nullable=true)
     * })
     */
    protected $parentFileCategory;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="PlFileCategory", mappedBy="idParentFileCategory")
     */
    private $childFildCategory;

    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_account", referencedColumnName="id_account", nullable=true)
     * })
     */
    protected $account;

    /**
     * @var string
     *
     * @ORM\Column(name="file_category_name", type="string", length=45, nullable=false)
     * @Expose
     */
    private $name;

    /**
     * @var text
     *
     * @ORM\Column(name="file_category_description", type="text", nullable=true)
     * @Expose
     */
    private $description;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param text $description
     */
    public function setDescription($description) {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return text 
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->childFildCategory = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set parentFileCategory
     *
     * @param \Leadsius\ApiBundle\Entity\PlFileCategory $parentFileCategory
     * @return PlFileCategory
     */
    public function setParentFileCategory(\Leadsius\ApiBundle\Entity\PlFileCategory $parentFileCategory = null)
    {
        $this->parentFileCategory = $parentFileCategory;

        return $this;
    }

    /**
     * Get parentFileCategory
     *
     * @return \Leadsius\ApiBundle\Entity\PlFileCategory 
     */
    public function getParentFileCategory()
    {
        return $this->parentFileCategory;
    }

    /**
     * Add childFildCategory
     *
     * @param \Leadsius\ApiBundle\Entity\PlFileCategory $childFildCategory
     * @return PlFileCategory
     */
    public function addChildFildCategory(\Leadsius\ApiBundle\Entity\PlFileCategory $childFildCategory)
    {
        $this->childFildCategory[] = $childFildCategory;

        return $this;
    }

    /**
     * Remove childFildCategory
     *
     * @param \Leadsius\ApiBundle\Entity\PlFileCategory $childFildCategory
     */
    public function removeChildFildCategory(\Leadsius\ApiBundle\Entity\PlFileCategory $childFildCategory)
    {
        $this->childFildCategory->removeElement($childFildCategory);
    }

    /**
     * Get childFildCategory
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChildFildCategory()
    {
        return $this->childFildCategory;
    }

    /**
     * Set account
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $account
     * @return PlFileCategory
     */
    public function setAccount(\Leadsius\ApiBundle\Entity\PlAccount $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Leadsius\ApiBundle\Entity\PlAccount 
     */
    public function getAccount()
    {
        return $this->account;
    }
}
