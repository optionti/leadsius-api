<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\PlUnsubscribeRepository")
 * @ORM\Table(name="pl_unsubscribe")
 * @ExclusionPolicy("all")
 */
class PlUnsubscribe
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_unsubscribe", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Expose
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @Expose
     */
    private $updated;

    /**
     * @var PlContact
     *
     * @ORM\ManyToOne(targetEntity="PlContact", inversedBy="unsubscribe")
     * @ORM\JoinColumn(name="id_contact", referencedColumnName="id_contact", nullable=false)
     */
    protected $contact;

    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_account", referencedColumnName="id_account", nullable=true)
     * })
     */
    protected $account;

    /**
     * @var MaEmailCampaign
     *
     * @ORM\ManyToOne(targetEntity="MaEmailCampaign")
     * @ORM\JoinColumn(name="id_email_campaign", referencedColumnName="id_email_campaign", nullable=false)
     */
    protected $emailCampaign;

    /**
     * @var PlList
     *
     * @ORM\ManyToOne(targetEntity="PlList", inversedBy="unsubscribe")
     * @ORM\JoinColumn(name="id_list", referencedColumnName="id_list", nullable=true)
     */
    protected $list;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set idContact
     *
     * @param PlContact $idContact
     * @return PlUnsubscribe
     */
    public function setIdContact(PlContact $idContact)
    {
        $this->idContact = $idContact;

        return $this;
    }

    /**
     * Get idContact
     *
     * @return PlContact 
     */
    public function getIdContact()
    {
        return $this->idContact;
    }

    /**
     * Set account
     *
     * @param PlAccount $account
     * @return PlUnsubscribe
     */
    public function setAccount(PlAccount $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return PlAccount 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set emailCampaign
     *
     * @param MaEmailCampaign $emailCampaign
     * @return PlUnsubscribe
     */
    public function setEmailCampaign(MaEmailCampaign $emailCampaign)
    {
        $this->emailCampaign = $emailCampaign;

        return $this;
    }

    /**
     * Get emailCampaign
     *
     * @return MaEmailCampaign 
     */
    public function getEmailCampaign()
    {
        return $this->emailCampaign;
    }

    /**
     * Set list
     *
     * @param PlList $list
     * @return PlUnsubscribe
     */
    public function setList(PlList $list = null)
    {
        $this->list = $list;

        return $this;
    }

    /**
     * Get list
     *
     * @return PlList 
     */
    public function getList()
    {
        return $this->list;
    }
}
