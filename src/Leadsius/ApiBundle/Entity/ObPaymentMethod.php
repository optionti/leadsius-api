<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\ObPaymentMethodRepository")
 * @ORM\Table(name="ob_payment_method")
 * @ExclusionPolicy("all")
 */
class ObPaymentMethod
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_payment_method", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_method_name", type="string", length=45, nullable=false)
     *
     * @Expose
     */
    private $methodName;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set methodName
     *
     * @param string $methodName
     * @return ObPaymentMethod
     */
    public function setMethodName($methodName)
    {
        $this->methodName = $methodName;
    
        return $this;
    }

    /**
     * Get methodName
     *
     * @return string 
     */
    public function getMethodName()
    {
        return $this->methodName;
    }
}
