<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\MaEmailRepository")
 * @ORM\Table(name="ma_email")
 * @ExclusionPolicy("all")
 */
class MaEmail {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_email", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email_name", type="string", length=255, nullable=false)
     *
     * @Expose
     */
    private $name;

    /**
     * @var text
     *
     * @ORM\Column(name="email_html_content", type="text", nullable=true)
     *
     * @Expose
     */
    private $htmlContent;

    /**
     * @var text
     *
     * @ORM\Column(name="email_plain_content", type="text", nullable=true)
     *
     * @Expose
     */
    private $plainContent;

    /**
     * @var text
     *
     * @ORM\Column(name="email_url", type="text", nullable=true)
     *
     * @Expose
     */
    private $url;

    /**
     * @var text
     *
     * @ORM\Column(name="email_tags", type="text", nullable=true)
     *
     * @Expose
     */
    private $tags;

    /**
     * @var datetime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Expose
     * @JMS\Groups({"list"})
     */
    protected $created;

    /**
     * @var datetime $updated
     *
     * @Gedmo\Timestampable(on="update")     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @Expose
     * @JMS\Groups({"list"})
     */
    protected $updated;

    /**
     * @var datetime $deleted
     *
     * @ORM\Column(name="deleted", type="datetime", nullable=true)
     * @Expose
     * @JMS\Groups({"list"})
     */
    protected $deleted;

    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_account", referencedColumnName="id_account", nullable=false)
     * })
     */
    protected $account;

    /**
     * @var PlUser
     *
     * @ORM\ManyToOne(targetEntity="PlUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id_user", nullable=true)
     * })
     */
    private $user;

    /**
     * @var MaProgram
     *
     * @ORM\ManyToOne(targetEntity="MaProgram", inversedBy="idEmail")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_program", referencedColumnName="id_program", nullable=true)
     * })
     */
    protected $program;
    
    /**
     * @var PlPicklistOption
     *
     * @ORM\ManyToOne(targetEntity="PlPicklistOption")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="email_category", referencedColumnName="id_picklist_option", nullable=true)
     * })
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="MaEmailCampaign", mappedBy="idEmail")
     */
    protected $campaign;
    
    public function __toString() {

        return (string)$this->name;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set htmlContent
     *
     * @param string $htmlContent
     */
    public function setHtmlContent($htmlContent) {
        $this->htmlContent = $htmlContent;
    }

    /**
     * Get htmlContent
     *
     * @return string 
     */
    public function getHtmlContent() {
        return $this->htmlContent;
    }

    /**
     * Set plainContent
     *
     * @param string $plainContent
     */
    public function setPlainContent($plainContent) {
        $this->plainContent = $plainContent;
    }

    /**
     * Get plainContent
     *
     * @return string 
     */
    public function getPlainContent() {
        return $this->plainContent;
    }

    /**
     * Set url
     *
     * @param string $url
     */
    public function setUrl($url) {
        $this->url = $url;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl() {
        return $this->url;
    }

    /**
     * Set tags
     *
     * @param string $tags
     */
    public function setTags($tags) {
        $this->tags = $tags;
    }

    /**
     * Get tags
     *
     * @return string 
     */
    public function getTags() {
        return $this->tags;
    }

    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created) {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
	public function getCreated()
    {
        return $this->created;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
	public function getTimezoneCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated) {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set deleted
     *
     * @param datetime $deleted
     */
    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }
	
    /**
     * Get deleted
     *
     * @return datetime 
     */
    public function getDeleted() {
        return $this->deleted;
    }

    /**
     * Set emailCategory
     *
     * @param PlPicklistOption $emailCategory
     */
    public function setEmailCategory(PlPicklistOption $emailCategory) {
        $this->emailCategory = $emailCategory;
    }

    /**
     * Get emailCategory
     *
     * @return PlPicklistOption 
     */
    public function getEmailCategory() {
        return $this->emailCategory;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->campaign = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set account
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $account
     * @return MaEmail
     */
    public function setAccount(\Leadsius\ApiBundle\Entity\PlAccount $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Leadsius\ApiBundle\Entity\PlAccount 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set user
     *
     * @param \Leadsius\ApiBundle\Entity\PlUser $user
     * @return MaEmail
     */
    public function setUser(\Leadsius\ApiBundle\Entity\PlUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Leadsius\ApiBundle\Entity\PlUser 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set program
     *
     * @param \Leadsius\ApiBundle\Entity\MaProgram $program
     * @return MaEmail
     */
    public function setProgram(\Leadsius\ApiBundle\Entity\MaProgram $program = null)
    {
        $this->program = $program;

        return $this;
    }

    /**
     * Get program
     *
     * @return \Leadsius\ApiBundle\Entity\MaProgram 
     */
    public function getProgram()
    {
        return $this->program;
    }

    /**
     * Set category
     *
     * @param \Leadsius\ApiBundle\Entity\PlPicklistOption $category
     * @return MaEmail
     */
    public function setCategory(\Leadsius\ApiBundle\Entity\PlPicklistOption $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Leadsius\ApiBundle\Entity\PlPicklistOption 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add campaign
     *
     * @param \Leadsius\ApiBundle\Entity\MaEmailCampaign $campaign
     * @return MaEmail
     */
    public function addCampaign(\Leadsius\ApiBundle\Entity\MaEmailCampaign $campaign)
    {
        $this->campaign[] = $campaign;

        return $this;
    }

    /**
     * Remove campaign
     *
     * @param \Leadsius\ApiBundle\Entity\MaEmailCampaign $campaign
     */
    public function removeCampaign(\Leadsius\ApiBundle\Entity\MaEmailCampaign $campaign)
    {
        $this->campaign->removeElement($campaign);
    }

    /**
     * Get campaign
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCampaign()
    {
        return $this->campaign;
    }
}
