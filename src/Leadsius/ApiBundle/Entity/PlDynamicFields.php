<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;
/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\PlFilterRepository")
 * @ORM\Table(name="pl_dynamic_fields")
 * @ExclusionPolicy("all")
 */
class PlDynamicFields
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_dynamic_fields", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dynamic_fields_value", type="string", length=45, nullable=true)
     * @Expose
     */
    private $value;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Expose
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @Expose
     */
    private $updated;

    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount")
     * @ORM\JoinColumn(name="id_account", referencedColumnName="id_account", nullable=false)
     */
    protected $account;

    /**
     * @var PlCompany
     *
     * @ORM\ManyToOne(targetEntity="PlCompany", inversedBy="dynamicFields")
     * @ORM\JoinColumn(name="id_company", referencedColumnName="id_company", nullable=true)
     */
    private $company;

    /**
     * @var PlContact
     *
     * @ORM\ManyToOne(targetEntity="PlContact", inversedBy="dynamicFields" )
     * @ORM\JoinColumn(name="id_contact", referencedColumnName="id_contact", nullable=true)
     */
    private $contact;

    /**
     * @var PlField
     *
     * @ORM\ManyToOne(targetEntity="PlField", inversedBy="dynamicFields")
     * @ORM\JoinColumn(name="id_field", referencedColumnName="id_field", nullable=false)
     * @Expose
     */
    private $field;

    /**
     * Get idDynamicFields
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set account
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $account
     * @return PlDynamicFields
     */
    public function setAccount(\Leadsius\ApiBundle\Entity\PlAccount $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Leadsius\ApiBundle\Entity\PlAccount 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set company
     *
     * @param \Leadsius\ApiBundle\Entity\PlCompany $company
     * @return PlDynamicFields
     */
    public function setCompany(\Leadsius\ApiBundle\Entity\PlCompany $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \Leadsius\ApiBundle\Entity\PlCompany 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set contact
     *
     * @param \Leadsius\ApiBundle\Entity\PlContact $contact
     * @return PlDynamicFields
     */
    public function setContact(\Leadsius\ApiBundle\Entity\PlContact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return \Leadsius\ApiBundle\Entity\PlContact 
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set field
     *
     * @param \Leadsius\ApiBundle\Entity\PlField $field
     * @return PlDynamicFields
     */
    public function setField(\Leadsius\ApiBundle\Entity\PlField $field)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Get field
     *
     * @return \Leadsius\ApiBundle\Entity\PlField 
     */
    public function getField()
    {
        return $this->field;
    }
}
