<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\PlCustomUrlRepository")
 * @ORM\Table(name="pl_custom_url")
 * @ExclusionPolicy("all")
 */
class PlCustomUrl {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_custom_url", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount", inversedBy="customUrls")
     * @ORM\JoinColumn(name="id_account", referencedColumnName="id_account", nullable=false)
     */
    protected $account;

    /**
     * @var string
     *
     * @ORM\Column(name="url_name", type="string", length=255, nullable=true)
     * @Expose
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="url_type", type="string", length=255, nullable=true)
     * @Expose
     */
    private $type;
    
    /**
     * @var string
     *
     * @ORM\Column(name="url_schema", type="string", length=55, nullable=false)
     * @Expose
     */
    private $schema;

    /**
     * @var string
     *
     * @ORM\Column(name="url_address", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="Please enter the url address.")
     * @Expose
     */
    protected $address;

    /**
     * @var string
     *
     * @ORM\Column(name="verification_code", type="string", length=255, nullable=true)
     * @Expose
     */
    private $verificationCode;
    
    /**
     * @var boolean
     * 
     * @ORM\Column(name="verified", type="boolean", nullable=true)
     * @Expose
     */
    protected $verified;
    
    /**
     * @var boolean
     * 
     * @ORM\Column(name="selected", type="boolean", nullable=true)
     * @Expose
     */
    protected $selected;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Expose
     * @Gedmo\Timestampable(on="create")
     */
    protected $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @Expose
     */
    private $updated;

    /**
     * @var datetime
     *
     * @ORM\Column(name="deleted", type="datetime", nullable=true)
     * @Expose
     */
    protected $deleted;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set verified
     *
     * @param boolean $verified
     */
    public function setVerified($verified)
    {
        $this->verified = $verified;
    }

    /**
     * Get verified
     *
     * @return boolean 
     */
    public function getVerified()
    {
        return $this->verified;
    }

    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set deleted
     *
     * @param datetime $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * Get deleted
     *
     * @return datetime 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set idAccount
     *
     * @param PlAccount $idAccount
     */
    public function setIdAccount(PlAccount $idAccount)
    {
        $this->idAccount = $idAccount;
    }

    /**
     * Get idAccount
     *
     * @return PlAccount 
     */
    public function getIdAccount()
    {
        return $this->idAccount;
    }

    /**
     * Set type
     *
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set verificationCode
     *
     * @param string $verificationCode
     */
    public function setVerificationCode($verificationCode)
    {
        $this->verificationCode = $verificationCode;
    }

    /**
     * Get verificationCode
     *
     * @return string 
     */
    public function getVerificationCode()
    {
        return $this->verificationCode;
    }

    /**
     * Set schema
     *
     * @param string $schema
     */
    public function setSchema($schema)
    {
        $this->schema = $schema;
    }

    /**
     * Get schema
     *
     * @return string 
     */
    public function getSchema()
    {
        return $this->schema;
    }

    /**
     * Set selected
     *
     * @param boolean $selected
     */
    public function setSelected($selected)
    {
        $this->selected = $selected;
    }

    /**
     * Get selected
     *
     * @return boolean 
     */
    public function getSelected()
    {
        return $this->selected;
    }

    /**
     * Set account
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $account
     * @return PlCustomUrl
     */
    public function setAccount(\Leadsius\ApiBundle\Entity\PlAccount $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Leadsius\ApiBundle\Entity\PlAccount 
     */
    public function getAccount()
    {
        return $this->account;
    }
}
