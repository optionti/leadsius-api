<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\PlCompanyRepository")
 * @ORM\Table(name="pl_company")
 * @ExclusionPolicy("all")
 */
class PlCompany
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_company", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="company_name", type="string", length=60, nullable=false)
     *
     * @Expose
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="company_website", type="string", length=45, nullable=true)
     *
     * @Expose
     */
    private $website;

    /**
     * @var integer
     *
     * @ORM\Column(name="company_employees", type="integer", nullable=true)
     *
     * @Expose
     */
    private $employees;

    /**
     * @var float
     *
     * @ORM\Column(name="company_revenue", type="float", nullable=true)
     *
     * @Expose
     */
    private $reveneu;

    /**
     * @var string
     *
     * @ORM\Column(name="company_industry", type="string", length=45, nullable=true)
     *
     * @Expose
     */
    private $industry;


    /**
     * @var string
     *
     * @ORM\Column(name="company_phone", type="string", length=45, nullable=true)
     *
     * @Expose
     */
    private $phone;


    /**
     * @var string
     *
     * @ORM\Column(name="company_address", type="text", nullable=true)
     *
     * @Expose
     */
    private $address;

    /**
     * @var integer
     *
     * @ORM\Column(name="company_pobox", type="string", length=155, nullable=true)
     *
     * @Expose
     */
    private $pobox;

    /**
     * @var string
     *
     * @ORM\Column(name="company_postal_code", type="string", length=45, nullable=true)
     *
     * @Expose
     */
    private $postalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="company_city", type="string", length=45, nullable=true)
     *
     * @Expose
     */
    private $city;

    /**
     * @var string
     *
     * ORM\Column(name="company_region", type="string", length=45, nullable=true)
     *
     * @Expose
     */
    private $region;


    /**
     * @var string
     *
     * @ORM\Column(name="company_other", type="text", nullable=true)
     *
     * @Expose
     */
    private $other;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Expose
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @Expose
     */
    private $updated;


    /**
     * @var datetime
     *
     * @ORM\Column(name="deleted", type="datetime", nullable=true)
     *
     * @Expose
     */
    private $deleted;

    /**
     * @var string
     *
     * @ORM\Column(name="company_status", type="string", length=45, nullable=true)
     *
     * @Expose
     */
    private $status;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="PlContact", mappedBy="company", cascade={"persist"})
     */
    private $contacts;

    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount", inversedBy="companies")
     * @ORM\JoinColumn(name="id_account", referencedColumnName="id_account")
     */
    private $account;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="PlDynamicFields", mappedBy="company")
     */
    private $dynamicFields;

    /**
     * @var PlCountry
     *
     * @ORM\ManyToOne(targetEntity="PlCountry")
     * @ORM\JoinColumn(name="id_country", referencedColumnName="id_country", nullable=true)
     */
    private $country;

    /**
     * @var PlUser
     *
     * @ORM\ManyToOne(targetEntity="PlUser")
     * @ORM\JoinColumn(name="id_user_responsible", referencedColumnName="id_user", nullable=true)
     */
    private $userResponsible;

    /**
     * @var PlPicklistOption
     *
     * @ORM\ManyToOne(targetEntity="PlPicklistOption")
     * @ORM\JoinColumn(name="type", referencedColumnName="id_picklist_option", nullable=true)
     * @Expose
     */
    private $typeOption;

    public function __toString()
    {
        return $this->name;
    }

    public function __construct()
    {
        $this->contacts = new ArrayCollection();
        $this->dynamicFields = new ArrayCollection();
    }

    /**
     * @param string $address
     *
     * @return $this
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $city
     *
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param \Datetime $created
     *
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return \Datetime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \Datetime $deleted
     *
     * @return $this
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
        return $this;
    }

    /**
     * @return \Datetime
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param int $employees
     *
     * @return $this
     */
    public function setEmployees($employees)
    {
        $this->employees = $employees;
        return $this;
    }

    /**
     * @return int
     */
    public function getEmployees()
    {
        return $this->employees;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $industry
     *
     * @return $this
     */
    public function setIndustry($industry)
    {
        $this->industry = $industry;
        return $this;
    }

    /**
     * @return string
     */
    public function getIndustry()
    {
        return $this->industry;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $other
     *
     * @return $this
     */
    public function setOther($other)
    {
        $this->other = $other;
        return $this;
    }

    /**
     * @return string
     */
    public function getOther()
    {
        return $this->other;
    }

    /**
     * @param string $phone
     *
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param int $pobox
     *
     * @return $this
     */
    public function setPobox($pobox)
    {
        $this->pobox = $pobox;
        return $this;
    }

    /**
     * @return int
     */
    public function getPobox()
    {
        return $this->pobox;
    }

    /**
     * @param string $postalCode
     *
     * @return $this
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param string $region
     *
     * @return $this
     */
    public function setRegion($region)
    {
        $this->region = $region;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param float $reveneu
     *
     * @return $this
     */
    public function setReveneu($reveneu)
    {
        $this->reveneu = $reveneu;
        return $this;
    }

    /**
     * @return float
     */
    public function getReveneu()
    {
        return $this->reveneu;
    }

    /**
     * @param string $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param \DateTime $updated
     *
     * @return $this
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
        return $this;
    }

    /**
     * @return \Datetime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param string $website
     *
     * @return $this
     */
    public function setWebsite($website)
    {
        $this->website = $website;
        return $this;
    }

    /**
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Add contacts
     *
     * @param \Leadsius\ApiBundle\Entity\PlContact $contacts
     * @return PlCompany
     */
    public function addContact(\Leadsius\ApiBundle\Entity\PlContact $contacts)
    {
        $this->contacts[] = $contacts;

        return $this;
    }

    /**
     * Remove contacts
     *
     * @param \Leadsius\ApiBundle\Entity\PlContact $contacts
     */
    public function removeContact(\Leadsius\ApiBundle\Entity\PlContact $contacts)
    {
        $this->contacts->removeElement($contacts);
    }

    /**
     * Get contacts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * Set account
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $account
     * @return PlCompany
     */
    public function setAccount(\Leadsius\ApiBundle\Entity\PlAccount $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Leadsius\ApiBundle\Entity\PlAccount 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Add dynamicFields
     *
     * @param \Leadsius\ApiBundle\Entity\PlDynamicFields $dynamicFields
     * @return PlCompany
     */
    public function addDynamicField(\Leadsius\ApiBundle\Entity\PlDynamicFields $dynamicFields)
    {
        $this->dynamicFields[] = $dynamicFields;

        return $this;
    }

    /**
     * Remove dynamicFields
     *
     * @param \Leadsius\ApiBundle\Entity\PlDynamicFields $dynamicFields
     */
    public function removeDynamicField(\Leadsius\ApiBundle\Entity\PlDynamicFields $dynamicFields)
    {
        $this->dynamicFields->removeElement($dynamicFields);
    }

    /**
     * Get dynamicFields
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDynamicFields()
    {
        return $this->dynamicFields;
    }

    /**
     * Set country
     *
     * @param \Leadsius\ApiBundle\Entity\PlCountry $country
     * @return PlCompany
     */
    public function setCountry(\Leadsius\ApiBundle\Entity\PlCountry $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \Leadsius\ApiBundle\Entity\PlCountry 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set userResponsible
     *
     * @param \Leadsius\ApiBundle\Entity\PlUser $userResponsible
     * @return PlCompany
     */
    public function setUserResponsible(\Leadsius\ApiBundle\Entity\PlUser $userResponsible = null)
    {
        $this->userResponsible = $userResponsible;

        return $this;
    }

    /**
     * Get userResponsible
     *
     * @return \Leadsius\ApiBundle\Entity\PlUser 
     */
    public function getUserResponsible()
    {
        return $this->userResponsible;
    }

    /**
     * Set typeOption
     *
     * @param \Leadsius\ApiBundle\Entity\PlPicklistOption $typeOption
     * @return PlCompany
     */
    public function setTypeOption(\Leadsius\ApiBundle\Entity\PlPicklistOption $typeOption = null)
    {
        $this->typeOption = $typeOption;

        return $this;
    }

    /**
     * Get typeOption
     *
     * @return \Leadsius\ApiBundle\Entity\PlPicklistOption 
     */
    public function getTypeOption()
    {
        return $this->typeOption;
    }
}
