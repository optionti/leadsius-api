<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\MaWebformRepository")
 * @ORM\Table(name="ma_webform")
 * @JMS\ExclusionPolicy("all")
 */
class MaWebform {

    /**
     * @var integer 
     *
     * @ORM\Column(name="id_webform", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @JMS\Expose
     */
    private $id;

    /**
     * @var string 
     *
     * @ORM\Column(name="webform_name", type="string", length=255, nullable=false)
     *
     * @JMS\Expose
     */
    private $name;

    /**
     * @var text 
     *
     * @ORM\Column(name="webform_html_content", type="text", nullable=true)
     *
     * @JMS\Expose
     */
    private $htmlContent;

    /**
     * @var text 
     *
     * @ORM\Column(name="webform_html_content_bootstrap", type="text", nullable=true)
     */
    private $htmlContentBootstrap;

    /**
     * @var text
     *
     * @ORM\Column(name="webform_json_content", type="text", nullable=true)
     *
     * @JMS\Expose
     */
    private $jsonContent;

    /**
     * @var string 
     *
     * @ORM\Column(name="webform_layout", type="string", length=255, nullable=false)
     */
    private $layout;

    /**
     * @var string 
     *
     * @ORM\Column(name="webform_confirmation_option", type="string", length=255, nullable=true)
     *
     * @JMS\Expose
     */
    private $confirmationOption;

    /**
     * @var text 
     *
     * @ORM\Column(name="webform_alert_message", type="text", nullable=true)
     *
     * @JMS\Expose
     */
    private $alertMessage;

    /**
     * @var string 
     *
     * @ORM\Column(name="webform_redirect_external_page", type="string", length=255, nullable=true)
     *
     * @JMS\Expose
     */
    private $webformRedirectExternalPage;

    /**
     * @var boolean 
     *
     * @ORM\Column(name="webform_overwrite_existing_value", type="boolean", nullable=true)
     */
    private $webformOverwriteExistingValue;

    /**
     * @var boolean 
     *
     * @ORM\Column(name="webform_add_value_empty", type="boolean", nullable=true)
     */
    private $addValueEmpty;

    /**
     * @var string 
     *
     * @ORM\Column(name="webform_customization", type="string", length=4000, nullable=true)
     *
     * @JMS\Expose
     */
    private $customization;

    /**
     * @var string 
     *
     * @ORM\Column(name="webform_user_email_subject", type="string", length=255, nullable=true)
     *
     * @JMS\Expose
     */
    private $userEmailSubject;

    /**
     * @var text 
     *
     * @ORM\Column(name="webform_user_email_message", type="text", nullable=true)
     */
    private $userEmailMessage;


    /**
     * @var text 
     *
     * @ORM\Column(name="webform_admin_email_subject", type="string", length=255, nullable=true)
     *
     * @JMS\Expose
     */
    private $adminEmailSubject;

    /**
     * @var text 
     *
     * @ORM\Column(name="webform_admin_email_message", type="text", nullable=true)
     */
    private $adminEmailMessage;
    
    /**
     * @var text 
     *
     * @ORM\Column(name="custom_css", type="text", nullable=true)
     *
     * @JMS\Expose
     */
    private $customCss;
    
    /**
     * @var text 
     *
     * @ORM\Column(name="custom_javascript", type="text", nullable=true)
     *
     * @JMS\Expose
     */
    private $customJavascript;
    
    /**
     * @var datetime $created
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="create")
     *
     * @JMS\Expose
     */
    protected $created;

    /**
     * @var datetime $updated
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="update")
     *
     * @JMS\Expose
     */
    protected $updated;

    /**
     * @var datetime $deleted
     *
     * @ORM\Column(name="deleted", type="datetime", nullable=true)
     */
    protected $deleted;

    /**
     * @var PlAccount
     *
     * @ORM\ManyToOne(targetEntity="PlAccount")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_account", referencedColumnName="id_account", nullable=false)
     * })
     */
    protected $account;

    /**
     * @var PlUser
     *
     * @ORM\ManyToOne(targetEntity="PlUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id_user", nullable=true)
     * })
     */
    private $user;

    /**
     * @var MaLandingPage
     *
     * @ORM\ManyToOne(targetEntity="MaLandingPage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="webform_thank_you_page", referencedColumnName="id_landing_page", nullable=true)
     * })
     */
    protected $thankYouPage;

    /**
     * @var MaSystemEmail
     *
     * @ORM\ManyToOne(targetEntity="MaSystemEmail")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="webform_user_email_from", referencedColumnName="id_system", nullable=true)
     * })
     */
    private $userEmailFrom;

    /**
     * @var PlUser
     *
     * @ORM\ManyToOne(targetEntity="PlUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id_user", nullable=true)
     * })
     */ 
    private $emailTo;

    /**
     * @var MaSystemEmail
     *
     * @ORM\ManyToOne(targetEntity="MaSystemEmail")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="webform_admin_email_from", referencedColumnName="id_system", nullable=true)
     * })
     */
    private $emailFrom;

    /**
     * @var MaProgram
     *
     * @ORM\ManyToOne(targetEntity="MaProgram")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_program", referencedColumnName="id_program", nullable=true)
     * })
     */
    protected $program;

    /**
     * @var PlPicklistOption
     *
     * @ORM\ManyToOne(targetEntity="PlPicklistOption")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="webform_category", referencedColumnName="id_picklist_option", nullable=true)
     * })
     */
    private $category;

    /**
     *
     * @var $idMaWebformResponse
     * @ORM\OneToMany(targetEntity="MaWebform", mappedBy="webform")
     */
    private $response;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set htmlContent
     *
     * @param string $htmlContent
     */
    public function setHtmlContent($htmlContent) {
        $this->htmlContent = $htmlContent;
    }

    /**
     * Get htmlContent
     *
     * @return string 
     */
    public function getHtmlContent() {
        return $this->htmlContent;
    }

    /**
     * Set htmlContentBootstrap
     *
     * @param string $htmlContentBootstrap
     */
    public function setHtmlContentBootstrap($htmlContentBootstrap) {
        $this->htmlContentBootstrap = $htmlContentBootstrap;
    }

    /**
     * Get htmlContentBootstrap
     *
     * @return string 
     */
    public function getHtmlContentBootstrap() {
        return $this->htmlContentBootstrap;
    }

    /**
     * @param text $jsonContent
     * @return $this
     */
    public function setJsonContent($jsonContent) {
        $this->jsonContent = $jsonContent;

        return $this;
    }

    /**
     * @return text
     */
    public function getJsonContent() {
        return $this->jsonContent;
    }

    /**
     * Set layout
     *
     * @param string $layout
     */
    public function setLayout($layout) {
        $this->layout = $layout;
    }

    /**
     * Get layout
     *
     * @return string 
     */
    public function getLayout() {
        return $this->layout;
    }

    /**
     * Set confirmationOption
     *
     * @param string $confirmationOption
     */
    public function setConfirmationOption($confirmationOption) {
        $this->confirmationOption = $confirmationOption;
    }

    /**
     * Get confirmationOption
     *
     * @return string 
     */
    public function getConfirmationOption() {
        return $this->confirmationOption;
    }

    /**
     * Set alertMessage
     *
     * @param string $alertMessage
     */
    public function setAlertMessage($alertMessage) {
        $this->alertMessage = $alertMessage;
    }

    /**
     * Get alertMessage
     *
     * @return string 
     */
    public function getAlertMessage() {
        return $this->alertMessage;
    }

    /**
     * Set redirectExternalPage
     *
     * @param string $redirectExternalPage
     */
    public function setRedirectExternalPage($redirectExternalPage) {
        $this->redirectExternalPage = $redirectExternalPage;
    }

    /**
     * Get redirectExternalPage
     *
     * @return string 
     */
    public function getRedirectExternalPage() {
        return $this->redirectExternalPage;
    }

    /**
     * Set webformOverwriteExistingValue
     *
     * @param boolean $webformOverwriteExistingValue
     */
    public function setWebformOverwriteExistingValue($webformOverwriteExistingValue) {
        $this->webformOverwriteExistingValue = $webformOverwriteExistingValue;
    }

    /**
     * Get webformOverwriteExistingValue
     *
     * @return boolean 
     */
    public function getWebformOverwriteExistingValue() {
        return $this->webformOverwriteExistingValue;
    }

    /**
     * Set addValueEmpty
     *
     * @param boolean $webformOverwriteExistingValue
     */
    public function setAddValueEmpty($addValueEmpty) {
        $this->addValueEmpty = $addValueEmpty;
    }

    /**
     * Get addValueEmpty
     *
     * @return boolean 
     */
    public function getAddValueEmpty() {
        return $this->addValueEmpty;
    }

    /**
     * Set adminEmailSubject
     *
     * @param string $userEmailSubject
     */
    public function setUserEmailSubject($userEmailSubject) {
        $this->userEmailSubject = $userEmailSubject;
    }

    /**
     * Get userEmailSubject
     *
     * @return string 
     */
    public function getUserEmailSubject() {
        return $this->userEmailSubject;
    }

    /**
     * Set adminEmailMessage
     *
     * @param string $userEmailMessage
     */
    public function setUserEmailMessage($userEmailMessage) {
        $this->userEmailMessage = $userEmailMessage;
    }

    /**
     * Get userEmailMessage
     *
     * @return string 
     */
    public function getUserEmailMessage() {
        return $this->userEmailMessage;
    }

    /**
     * Set adminEmailSubject
     *
     * @param string $adminEmailSubject
     */
    public function setAdminEmailSubject($adminEmailSubject) {
        $this->adminEmailSubject = $adminEmailSubject;
    }

    /**
     * Get adminEmailSubject
     *
     * @return string 
     */
    public function getAdminEmailSubject() {
        return $this->adminEmailSubject;
    }

    /**
     * Set adminEmailMessage
     *
     * @param string $adminEmailMessage
     */
    public function setAdminEmailMessage($adminEmailMessage) {
        $this->adminEmailMessage = $adminEmailMessage;
    }

    /**
     * Get adminEmailMessage
     *
     * @return string 
     */
    public function getAdminEmailMessage() {
        return $this->adminEmailMessage;
    }

    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created) {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated) {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set deleted
     *
     * @param datetime $deleted
     */
    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }
	
    /**
     * Get deleted
     *
     * @return datetime 
     */
    public function getDeleted() {
        return $this->deleted;
    }

    /**
     * Set customization
     *
     * @param string $customization
     */
    public function setCustomization($customization)
    {
        $this->customization = $customization;
    }

    /**
     * Get customization
     *
     * @return string 
     */
    public function getCustomization()
    {
        return $this->customization;
    }

    /**
     * Set customCss
     *
     * @param text $customCss
     */
    public function setCustomCss($customCss)
    {
        $this->customCss = $customCss;
    }

    /**
     * Get customCss
     *
     * @return text 
     */
    public function getCustomCss()
    {
        return $this->customCss;
    }

    /**
     * Set customJavascript
     *
     * @param text $customJavascript
     */
    public function setCustomJavascript($customJavascript)
    {
        $this->customJavascript = $customJavascript;
    }

    /**
     * Get customJavascript
     *
     * @return text 
     */
    public function getCustomJavascript()
    {
        return $this->customJavascript;
    }


    /**
     * Set webformRedirectExternalPage
     *
     * @param string $webformRedirectExternalPage
     * @return MaWebform
     */
    public function setWebformRedirectExternalPage($webformRedirectExternalPage)
    {
        $this->webformRedirectExternalPage = $webformRedirectExternalPage;

        return $this;
    }

    /**
     * Get webformRedirectExternalPage
     *
     * @return string 
     */
    public function getWebformRedirectExternalPage()
    {
        return $this->webformRedirectExternalPage;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->response = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set account
     *
     * @param \Leadsius\ApiBundle\Entity\PlAccount $account
     * @return MaWebform
     */
    public function setAccount(\Leadsius\ApiBundle\Entity\PlAccount $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Leadsius\ApiBundle\Entity\PlAccount 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set user
     *
     * @param \Leadsius\ApiBundle\Entity\PlUser $user
     * @return MaWebform
     */
    public function setUser(\Leadsius\ApiBundle\Entity\PlUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Leadsius\ApiBundle\Entity\PlUser 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set thankYouPage
     *
     * @param \Leadsius\ApiBundle\Entity\MaLandingPage $thankYouPage
     * @return MaWebform
     */
    public function setThankYouPage(\Leadsius\ApiBundle\Entity\MaLandingPage $thankYouPage = null)
    {
        $this->thankYouPage = $thankYouPage;

        return $this;
    }

    /**
     * Get thankYouPage
     *
     * @return \Leadsius\ApiBundle\Entity\MaLandingPage 
     */
    public function getThankYouPage()
    {
        return $this->thankYouPage;
    }

    /**
     * Set userEmailFrom
     *
     * @param \Leadsius\ApiBundle\Entity\MaSystemEmail $userEmailFrom
     * @return MaWebform
     */
    public function setUserEmailFrom(\Leadsius\ApiBundle\Entity\MaSystemEmail $userEmailFrom = null)
    {
        $this->userEmailFrom = $userEmailFrom;

        return $this;
    }

    /**
     * Get userEmailFrom
     *
     * @return \Leadsius\ApiBundle\Entity\MaSystemEmail 
     */
    public function getUserEmailFrom()
    {
        return $this->userEmailFrom;
    }

    /**
     * Set emailTo
     *
     * @param \Leadsius\ApiBundle\Entity\PlUser $emailTo
     * @return MaWebform
     */
    public function setEmailTo(\Leadsius\ApiBundle\Entity\PlUser $emailTo = null)
    {
        $this->emailTo = $emailTo;

        return $this;
    }

    /**
     * Get emailTo
     *
     * @return \Leadsius\ApiBundle\Entity\PlUser 
     */
    public function getEmailTo()
    {
        return $this->emailTo;
    }

    /**
     * Set emailFrom
     *
     * @param \Leadsius\ApiBundle\Entity\MaSystemEmail $emailFrom
     * @return MaWebform
     */
    public function setEmailFrom(\Leadsius\ApiBundle\Entity\MaSystemEmail $emailFrom = null)
    {
        $this->emailFrom = $emailFrom;

        return $this;
    }

    /**
     * Get emailFrom
     *
     * @return \Leadsius\ApiBundle\Entity\MaSystemEmail 
     */
    public function getEmailFrom()
    {
        return $this->emailFrom;
    }

    /**
     * Set program
     *
     * @param \Leadsius\ApiBundle\Entity\MaProgram $program
     * @return MaWebform
     */
    public function setProgram(\Leadsius\ApiBundle\Entity\MaProgram $program = null)
    {
        $this->program = $program;

        return $this;
    }

    /**
     * Get program
     *
     * @return \Leadsius\ApiBundle\Entity\MaProgram 
     */
    public function getProgram()
    {
        return $this->program;
    }

    /**
     * Set category
     *
     * @param \Leadsius\ApiBundle\Entity\PlPicklistOption $category
     * @return MaWebform
     */
    public function setCategory(\Leadsius\ApiBundle\Entity\PlPicklistOption $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Leadsius\ApiBundle\Entity\PlPicklistOption 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add response
     *
     * @param \Leadsius\ApiBundle\Entity\MaWebform $response
     * @return MaWebform
     */
    public function addResponse(\Leadsius\ApiBundle\Entity\MaWebform $response)
    {
        $this->response[] = $response;

        return $this;
    }

    /**
     * Remove response
     *
     * @param \Leadsius\ApiBundle\Entity\MaWebform $response
     */
    public function removeResponse(\Leadsius\ApiBundle\Entity\MaWebform $response)
    {
        $this->response->removeElement($response);
    }

    /**
     * Get response
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getResponse()
    {
        return $this->response;
    }
}
