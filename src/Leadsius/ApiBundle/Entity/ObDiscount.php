<?php

namespace Leadsius\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Leadsius\ApiBundle\Entity\Repositories\ObDiscountRepository")
 * @ORM\Table(name="ob_discount")
 * @ExclusionPolicy("all")
 */
class ObDiscount {
    /**
     * @var integer
     *
     * @ORM\Column(name="id_discount", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="discount_price", type="string", length=45, nullable=false)
     *
     * @Expose
     */
    private $price;
    /**
     * @var string
     *
     * @ORM\Column(name="discount_type", type="string", length=45, nullable=false)
     *
     * @Expose
     */
    private $type;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="discount_start_date", type="datetime", nullable=false)
     *
     * @Expose
     */
    private $startDate;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="discount_end_date", type="datetime", nullable=false)
     *
     * @Expose
     */
    private $endDate;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="discount_visibility_start_date", type="datetime", nullable=false)
     *
     * @Expose
     */
    private $visibilityStartDate;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="discount_visibility_end_date", type="datetime", nullable=false)
     *
     * @Expose
     */
    private $visibilityEndDate;
    /**
     * @var ObPaymentInfo
     *
     * @ORM\ManyToOne(targetEntity="ObPaymentInfo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_payment_info", referencedColumnName="id_payment_info", nullable=true)
     * })
     */
    private $paymentInfo;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return ObDiscount
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return ObDiscount
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return ObDiscount
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    
        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return ObDiscount
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    
        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set visibilityStartDate
     *
     * @param \DateTime $visibilityStartDate
     * @return ObDiscount
     */
    public function setVisibilityStartDate($visibilityStartDate)
    {
        $this->visibilityStartDate = $visibilityStartDate;
    
        return $this;
    }

    /**
     * Get visibilityStartDate
     *
     * @return \DateTime 
     */
    public function getVisibilityStartDate()
    {
        return $this->visibilityStartDate;
    }

    /**
     * Set visibilityEndDate
     *
     * @param \DateTime $visibilityEndDate
     * @return ObDiscount
     */
    public function setVisibilityEndDate($visibilityEndDate)
    {
        $this->visibilityEndDate = $visibilityEndDate;
    
        return $this;
    }

    /**
     * Get visibilityEndDate
     *
     * @return \DateTime 
     */
    public function getVisibilityEndDate()
    {
        return $this->visibilityEndDate;
    }


    /**
     * Set paymentInfo
     *
     * @param \Leadsius\ApiBundle\Entity\ObPaymentInfo $paymentInfo
     * @return ObDiscount
     */
    public function setPaymentInfo(\Leadsius\ApiBundle\Entity\ObPaymentInfo $paymentInfo = null)
    {
        $this->paymentInfo = $paymentInfo;

        return $this;
    }

    /**
     * Get paymentInfo
     *
     * @return \Leadsius\ApiBundle\Entity\ObPaymentInfo 
     */
    public function getPaymentInfo()
    {
        return $this->paymentInfo;
    }
}
