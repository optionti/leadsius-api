<?php

namespace Leadsius\ApiBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Information from the site that generated a transaction
 *
 * @author Alberto Simón <asimon@optionti.com>
 *
 * @MongoDB\EmbeddedDocument
 */
class SiteInfo
{
    /**
     * @MongoDB\String
     */
    private $protocol;

    /**
     * @MongoDB\Int
     */
    private $port;

    /**
     * @MongoDB\String
     */
    private $queryString;

    /**
     * @MongoDB\String
     */
    private $pageTitle;

    /**
     * @MongoDB\String
     */
    private $keywords;

    public function __construct($protocol, $port, $queryString, $pageTitle, $keywords = null)
    {
        $this->protocol = $protocol;
        $this->port = $port;
        $this->queryString = $queryString;
        $this->pageTitle = $pageTitle;
        $this->keywords = $keywords;
    }

    public function getProtocol()
    {
        return $this->protocol;
    }

    public function setProtocol($protocol)
    {
        $this->protocol = $protocol;
    }

    public function getPort()
    {
        return $this->port;
    }

    public function setPort($port)
    {
        $this->port = $port;
    }

    public function getQueryString()
    {
        return $this->queryString;
    }

    public function setQueryString($queryString)
    {
        $this->queryString = $queryString;
    }

    public function getPageTitle()
    {
        return $this->pageTitle;
    }

    public function setPageTitle($pageTitle)
    {
        $this->pageTitle = $pageTitle;
    }

    public function getKeywords()
    {
        return $this->keywords;
    }

    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
    }
}
