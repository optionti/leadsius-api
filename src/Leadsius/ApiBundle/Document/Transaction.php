<?php

namespace Leadsius\ApiBundle\Document;

use DateTime;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * A tracking transaction
 *
 * @author Alberto Simón <asimon@optionti.com>
 *
 * @MongoDB\Document
 */
class Transaction
{
    const TYPE_OPEN = '1';
    const TYPE_CLICK = '2';
    const TYPE_BOUNCE = '3';
    const TYPE_UNSUBSCRIBE = '4';
    const TYPE_SUBMIT = '5';
    const TYPE_PAGE_VIEW = '6';
    const TYPE_VIEW = '7';
    const TYPE_STAY = '8';

    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\String
     * @MongoDB\Index(order="asc")
     */
    private $siteKey;

    /**
     * @MongoDB\String
     */
    private $url;

    /**
     * @MongoDB\String
     */
    private $refererUrl;

    /**
     * @MongoDB\String
     */
    private $ipAddress;

    /**
     * @MongoDB\String
     * @MongoDB\Index(order="asc")
     */
    private $cookieId;

    /**
     * @MongoDB\EmbedOne(targetDocument="BrowserInfo")
     */
    private $browserInfo;

    /**
     * @MongoDB\EmbedOne(targetDocument="SiteInfo")
     */
    private $siteInfo;

    /**
     * @MongoDB\EmbedOne(targetDocument="Activity")
     */
    private $activity;

    /**
     * Types:
     * - 1: open
     *
     * @MongoDB\Int
     */
    private $transactionType;

    /**
     * @MongoDB\Date
     */
    private $firstSeen;

    /**
     * @MongoDB\Date
     */
    private $lastSeen;

    /**
     * @MongoDB\Boolean
     * @MongoDB\Index(order="asc")
     */
    private $isFP = false;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Visit")
     */
    private $visit;

    public function __construct($cookieId, $transactionType = null, $timestamp = null)
    {
        if ($timestamp === null) {
            $timestamp = new DateTime('NOW');
        }

        if ($transactionType === null) {
            $transactionType = self::TYPE_OPEN;
        }

        $this->cookieId = $cookieId;
        $this->transactionType = $transactionType;
        $this->firstSeen = $timestamp;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCookieId()
    {
        return $this->cookieId;
    }

    public function getSiteKey()
    {
        return $this->siteKey;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function getRefererUrl()
    {
        return $this->refererUrl;
    }

    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    public function getBrowserInfo()
    {
        return $this->browserInfo;
    }

    public function getSiteInfo()
    {
        return $this->siteInfo;
    }

    public function getActivity()
    {
        return $this->activity;
    }

    public function getTransactionType()
    {
        return $this->transactionType;
    }

    public function getFirstSeen()
    {
        return $this->firstSeen;
    }

    public function getLastSeen()
    {
        return $this->lastSeen;
    }

    public function getIsFP()
    {
        return $this->isFP;
    }

    public function getKeywords()
    {
        return $this->keywords;
    }

    public function getVisit()
    {
        return $this->visit;
    }

    public function setSiteKey($siteKey)
    {
        $this->siteKey = $siteKey;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function setRefererUrl($refererUrl)
    {
        $this->refererUrl = $refererUrl;
    }

    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;
    }

    public function setBrowserInfo($browserInfo)
    {
        $this->browserInfo = $browserInfo;
    }

    public function setSiteInfo($siteInfo)
    {
        $this->siteInfo = $siteInfo;
    }

    public function setActivity($activity)
    {
        $this->activity = $activity;
    }

    public function setTransactionType($transactionType)
    {
        $this->transactionType = $transactionType;
    }

    public function setCookieId($cookieId)
    {
        $this->cookieId = $cookieId;
    }

    public function setFirstSeen($firstSeen)
    {
        $this->firstSeen = $firstSeen;
    }

    public function setLastSeen($lastSeen)
    {
        $this->lastSeen = $lastSeen;
    }

    public function setIsFP($isFP)
    {
        $this->isFP = $isFP;
    }

    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
    }

    public function setVisit($visit)
    {
        $this->visit = $visit;
    }
}
