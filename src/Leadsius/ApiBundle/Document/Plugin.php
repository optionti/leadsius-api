<?php

namespace Leadsius\ApiBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Activity information of a transaction
 *
 * @author Alberto Simón <asimon@optionti.com>
 *
 * @MongoDB\EmbeddedDocument
 */
class Plugin
{
    /**
     * @MongoDB\String
     */
    private $name;

    /**
     * @MongoDB\String
     */
    private $filename;

    /**
     * @MongoDB\String
     */
    private $description;

    function __construct($name, $filename, $description)
    {
        $this->name = $name;
        $this->filename = $filename;
        $this->description = $description;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getFilename()
    {
        return $this->filename;
    }

    public function setFilename($filename)
    {
        $this->filename = $filename;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }
}
