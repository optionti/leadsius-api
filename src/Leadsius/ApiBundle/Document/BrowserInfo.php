<?php

namespace Leadsius\ApiBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Activity information of a transaction
 *
 * @author Alberto Simón <asimon@optionti.com>
 *
 * @MongoDB\EmbeddedDocument
 */
class BrowserInfo
{
    /**
     * @MongoDB\String
     */
    private $userAgent;

    /**
     * @MongoDB\String
     */
    private $code;

    /**
     * @MongoDB\String
     */
    private $name;

    /**
     * @MongoDB\String
     */
    private $version;

    /**
     * @MongoDB\Boolean
     */
    private $cookiesEnabled;

    /**
     * @MongoDB\String
     */
    private $platform;

    /**
     * @MongoDB\String
     */
    private $httpAccept;

    /**
     * @MongoDB\EmbedMany(targetDocument="Plugin")
     */
    private $plugins;

    /**
     * @MongoDB\EmbedOne(targetDocument="ScreenResolution")
     */
    private $screenResolution;

    // Browser Capabilities

    /**
     * @MongoDB\Boolean
     */
    private $supportsGeoLocation;

    /**
     * @MongoDB\Boolean
     */
    private $supportsStorageUpdates;

    /**
     * @MongoDB\Boolean
     */
    private $supportsDNT;

    /**
     * @MongoDB\Boolean
     */
    private $supportsOnline;

    /**
     * @MongoDB\Boolean
     */
    private $supportsRegisterProtocolHandler;

    public function getUserAgent()
    {
        return $this->userAgent;
    }

    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getCookiesEnabled()
    {
        return $this->cookiesEnabled;
    }

    public function setCookiesEnabled($cookiesEnabled)
    {
        $this->cookiesEnabled = $cookiesEnabled;
    }

    public function getPlatform()
    {
        return $this->platform;
    }

    public function setPlatform($platform)
    {
        $this->platform = $platform;
    }

    public function getHttpAccept()
    {
        return $this->httpAccept;
    }

    public function setHttpAccept($httpAccept)
    {
        $this->httpAccept = $httpAccept;
    }

    public function getScreenResolution()
    {
        return $this->screenResolution;
    }

    public function setScreenResolution(ScreenResolution $screenResolution)
    {
        $this->screenResolution = $screenResolution;
    }

    public function __construct()
    {
        $this->plugins = new ArrayCollection();
    }

    /**
     * Add plugin
     *
     * @param Leadsius\ApiBundle\Document\Plugin $plugin
     */
    public function addPlugin(\Leadsius\ApiBundle\Document\Plugin $plugin)
    {
        $this->plugins[] = $plugin;
    }

    /**
     * Remove plugin
     *
     * @param Leadsius\ApiBundle\Document\Plugin $plugin
     */
    public function removePlugin(\Leadsius\ApiBundle\Document\Plugin $plugin)
    {
        $this->plugins->removeElement($plugin);
    }

    /**
     * Get plugins
     *
     * @return Collection $plugins
     */
    public function getPlugins()
    {
        return $this->plugins;
    }

    public function getSupportsGeoLocation()
    {
        return $this->supportsGeoLocation;
    }

    public function setSupportsGeoLocation($supportsGeoLocation)
    {
        $this->supportsGeoLocation = $supportsGeoLocation;
    }

    public function getSupportsStorageUpdates()
    {
        return $this->supportsStorageUpdates;
    }

    public function setSupportsStorageUpdates($supportsStorageUpdates)
    {
        $this->supportsStorageUpdates = $supportsStorageUpdates;
    }

    public function getSupportsDNT()
    {
        return $this->supportsDNT;
    }

    public function setSupportsDNT($supportsDNT)
    {
        $this->supportsDNT = $supportsDNT;
    }

    public function getSupportsOnline()
    {
        return $this->supportsOnline;
    }

    public function setSupportsOnline($supportsOnline)
    {
        $this->supportsOnline = $supportsOnline;
    }

    public function getSupportsRegisterProtocolHandler()
    {
        return $this->supportsRegisterProtocolHandler;
    }

    public function setSupportsRegisterProtocolHandler($supportsRegisterProtocolHandler)
    {
        $this->supportsRegisterProtocolHandler = $supportsRegisterProtocolHandler;
    }
}
