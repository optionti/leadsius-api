<?php

namespace Leadsius\ApiBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Activity information of a transaction
 *
 * @author Alberto Simón <asimon@optionti.com>
 *
 * @MongoDB\EmbeddedDocument
 */
class Activity
{
    /**
     * @MongoDB\Int
     */
    private $programId;

    /**
     * @MongoDB\Int
     */
    private $contactId;

    /**
     * @MongoDB\Int
     */
    private $workflowId;

    /**
     * @MongoDB\Int
     */
    private $workflowTaskId;

    /**
     * @MongoDB\Int
     */
    private $workflowParentTaskId;

    /**
     * @MongoDB\Int
     */
    private $emailCampaignId;

    /**
     * @MongoDB\Int
     */
    private $emailId;

    /**
     * @MongoDB\Int
     */
    private $landingPageId;

    /**
     * @MongoDB\Int
     */
    private $webformId;

    /**
     * @MongoDB\String
     */
    private $param;

    /**
     * @MongoDB\String
     */
    private $paramId;

    /**
     * @MongoDB\String
     */
    private $paramValue;

    /**
     * @MongoDB\String
     */
    private $activityType;

    /**
     * @MongoDB\String
     */
    private $redirectUrl;

    public function getProgramId()
    {
        return $this->programId;
    }

    public function getContactId()
    {
        return $this->contactId;
    }

    public function getWorkflowId()
    {
        return $this->workflowId;
    }

    public function getWorkflowTaskId()
    {
        return $this->workflowTaskId;
    }

    public function getWorkflowParentTaskId()
    {
        return $this->workflowParentTaskId;
    }

    public function getEmailCampaignId()
    {
        return $this->emailCampaignId;
    }

    public function getEmailId()
    {
        return $this->emailId;
    }

    public function getLandingPageId()
    {
        return $this->landingPageId;
    }

    public function getWebformId()
    {
        return $this->webformId;
    }

    public function getParam()
    {
        return $this->param;
    }

    public function getParamId()
    {
        return $this->paramId;
    }

    public function getParamValue()
    {
        return $this->paramValue;
    }

    public function getRedirectUrl()
    {
        return $this->redirectUrl;
    }

    public function getActivityType()
    {
        return $this->activityType;
    }

    public function setProgramId($programId)
    {
        $this->programId = $programId;
        return $this;
    }

    public function setContactId($contactId)
    {
        $this->contactId = $contactId;
        return $this;
    }

    public function setWorkflowId($workflowId)
    {
        $this->workflowId = $workflowId;
        return $this;
    }

    public function setWorkflowTaskId($workflowTaskId)
    {
        $this->workflowTaskId = $workflowTaskId;
        return $this;
    }

    public function setWorkflowParentTaskId($workflowParentTaskId)
    {
        $this->workflowParentTaskId = $workflowParentTaskId;
        return $this;
    }

    public function setEmailCampaignId($emailCampaignId)
    {
        $this->emailCampaignId = $emailCampaignId;
        return $this;
    }

    public function setEmailId($emailId)
    {
        $this->emailId = $emailId;
        return $this;
    }

    public function setLandingPageId($landingPageId)
    {
        $this->landingPageId = $landingPageId;
        return $this;
    }

    public function setWebformId($webformId)
    {
        $this->webformId = $webformId;
        return $this;
    }

    public function setParam($param)
    {
        $this->param = $param;
        return $this;
    }

    public function setParamId($paramId)
    {
        $this->paramId = $paramId;
        return $this;
    }

    public function setParamValue($paramValue)
    {
        $this->paramValue = $paramValue;
        return $this;
    }

    public function setRedirectUrl($redirectUrl)
    {
        $this->redirectUrl = $redirectUrl;
        return $this;
    }

    public function setActivityType($activityType)
    {
        $this->activityType = $activityType;
        return $this;
    }
}
