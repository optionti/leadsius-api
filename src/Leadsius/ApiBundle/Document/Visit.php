<?php

namespace Leadsius\ApiBundle\Document;

use DateTime;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * A tracking transaction
 *
 * @author Alberto Simón <asimon@optionti.com>
 *
 * @MongoDB\Document
 */
class Visit
{
    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\String
     * @MongoDB\UniqueIndex(order="asc")
     */
    private $visitKey;

    /**
     * @MongoDB\Date
     */
    private $firstSeen;

    /**
     * @MongoDB\ReferenceMany(targetDocument="Transaction")
     */
    private $transactions;

    public function __construct($visitKey)
    {
        $this->transactions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->visitKey = $visitKey;
        $this->firstSeen = new DateTime('NOW');
    }

    public function getId()
    {
        return $this->id;
    }

    public function getVisitKey()
    {
        return $this->visitKey;
    }

    public function setVisitKey($visitKey)
    {
        $this->visitKey = $visitKey;
    }

    /**
     * Set firstSeen
     *
     * @param date $firstSeen
     * @return self
     */
    public function setFirstSeen($firstSeen)
    {
        $this->firstSeen = $firstSeen;
        return $this;
    }

    /**
     * Get firstSeen
     *
     * @return date $firstSeen
     */
    public function getFirstSeen()
    {
        return $this->firstSeen;
    }

    /**
     * Add transaction
     *
     * @param Leadsius\ApiBundle\Document\Transaction $transaction
     */
    public function addTransaction(\Leadsius\ApiBundle\Document\Transaction $transaction)
    {
        $this->transactions[] = $transaction;
    }

    /**
     * Remove transaction
     *
     * @param Leadsius\ApiBundle\Document\Transaction $transaction
     */
    public function removeTransaction(\Leadsius\ApiBundle\Document\Transaction $transaction)
    {
        $this->transactions->removeElement($transaction);
    }

    /**
     * Get transactions
     *
     * @return Doctrine\Common\Collections\Collection $transactions
     */
    public function getTransactions()
    {
        return $this->transactions;
    }
}
