<?php

namespace Leadsius\ApiBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Activity information of a transaction
 *
 * @author Alberto Simón <asimon@optionti.com>
 *
 * @MongoDB\EmbeddedDocument
 */
class ScreenResolution
{
    /**
     * @MongoDB\Int
     */
    private $availableHeight;

    /**
     * @MongoDB\Int
     */
    private $availableWidth;

    /**
     * @MongoDB\Int
     */
    private $height;

    /**
     * @MongoDB\Int
     */
    private $width;

    /**
     * @MongoDB\Int
     */
    private $colorDepth;

    /**
     * @MongoDB\Int
     */
    private $pixelDepth;

    function __construct($availableHeight, $availableWidth, $height, $width, $colorDepth, $pixelDepth)
    {
        $this->availableHeight = $availableHeight;
        $this->availableWidth = $availableWidth;

        $this->height = $height;
        $this->width = $width;

        $this->colorDepth = $colorDepth;
        $this->pixelDepth = $pixelDepth;
    }

    public function getAvailableHeight()
    {
        return $this->availableHeight;
    }

    public function setAvailableHeight($availableHeight)
    {
        $this->availableHeight = $availableHeight;
    }

    public function getAvailableWidth()
    {
        return $this->availableWidth;
    }

    public function setAvailableWidth($availableWidth)
    {
        $this->availableWidth = $availableWidth;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function setHeight($height)
    {
        $this->height = $height;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function setWidth($width)
    {
        $this->width = $width;
    }

    public function getColorDepth()
    {
        return $this->colorDepth;
    }

    public function setColorDepth($colorDepth)
    {
        $this->colorDepth = $colorDepth;
    }

    public function getPixelDepth()
    {
        return $this->pixelDepth;
    }

    public function setPixelDepth($pixelDepth)
    {
        $this->pixelDepth = $pixelDepth;
    }
}
