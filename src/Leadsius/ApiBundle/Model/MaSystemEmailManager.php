<?php

namespace Leadsius\ApiBundle\Model;
use Doctrine\ORM\Query\ResultSetMapping;

class MaSystemEmailManager extends BaseModelManager
{
    public function getEmailaddresses($options)
    {
        $email_addresses = $this->repository->findEmailAddresses($options);
        $total_emails_addresses = $this->repository->findEmailAddresses($options, true);

        $total_pages = ceil( $total_emails_addresses / $options['page_size'] );

        $data = array(
            'page' => $options['page'],
            'page_size' => $options['page_size'],
            'total_pages' => $total_pages,
            'total' => $total_emails_addresses,
            'email_addresses' => $email_addresses
        );

        return $data;
    }
}
