<?php

namespace Leadsius\ApiBundle\Model;

class PlListManager extends BaseModelManager
{
    public function addContactsToList($list, $contacts)
    {
        foreach ($contacts as $contact) {
                $list->addContact( $contact );
        }

        $this->save($list);
    }

    public function getLists($options)
    {
        $lists = $this->repository->findLists($options);
        $total_lists = $this->repository->findlists($options, true);

        $total_pages = ceil( $total_lists / $options['page_size'] );

        $data = array(
            'page' => $options['page'],
            'page_size' => $options['page_size'],
            'total_pages' => $total_pages,
            'total' => $total_lists,
            'lists' => $lists,
        );

        return $data;
    }
}
