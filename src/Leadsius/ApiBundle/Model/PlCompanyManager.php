<?php

namespace Leadsius\ApiBundle\Model;

class PlCompanyManager extends BaseModelManager
{
    public function addContactsToCompany($company, $contacts)
    {
        foreach ($contacts as $contact) {
            $contact->setCompany( $company );
        }

        $this->save($company);
    }

    public function getCompanies($options)
    {
        $companies = $this->repository->findCompanies($options);
        $total_companies = $this->repository->findCompanies($options, true);

        $total_pages = ceil( $total_companies / $options['page_size'] );

        $data = array(
            'page' => $options['page'],
            'page_size' => $options['page_size'],
            'total_pages' => $total_pages,
            'total' => $total_companies,
            'companies' => $companies,
        );

        return $data;
    }
}
