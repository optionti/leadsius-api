<?php

namespace Leadsius\ApiBundle\Model;
use Doctrine\ORM\Query\ResultSetMapping;
use Leadsius\ApiBundle\Entity\MaEmailCampaign;
use Leadsius\ApiBundle\Entity\MaWorkflow;
use Leadsius\ApiBundle\Entity\PlAccount;
use Leadsius\ApiBundle\Entity\PlUser;

class MaWorkflowTaskManager extends BaseModelManager
{
    public function getWorkflowTasks($options)
    {
        $workflowTasks = $this->repository->findWorkflowTasks($options);
        $total_workflow_tasks = $this->repository->findWorkflowTasks($options, true);

        $total_pages = ceil( $total_workflow_tasks / $options['page_size'] );

        $data = array(
            'page' => $options['page'],
            'page_size' => $options['page_size'],
            'total_pages' => $total_pages,
            'total' => $total_workflow_tasks,
            'workflowTasks' => $workflowTasks
        );

        return $data;
    }

    public function getWorkFlowTaskByIdWorkflow($id)
    {

        return $this->repository->findBy(array("workflow" => $id));
    }

    public function createInitial(PlAccount $account, PlUser $user, MaWorkflow $workflow)
    {
        $emailCampaign = new MaEmailCampaign();

        $emailCampaign
            ->setAccount($account)
            ->setUser($user)
            ->setName($workflow->getName() . ' condition task')
            ->setType('WORKFLOWTASK')
            ->setProgram( $workflow->getProgram() )
        ;

        $workflowTask = $this->create();

        $workflowTask
            ->setAccount($account)
            ->setUser($user)
            ->setWorkflow($workflow)
            ->setEmailCampaign($emailCampaign)
        ;

        return $workflowTask;
    }
}
