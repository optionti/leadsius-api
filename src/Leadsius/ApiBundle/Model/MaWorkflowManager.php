<?php

namespace Leadsius\ApiBundle\Model;
use Doctrine\ORM\Query\ResultSetMapping;

class MaWorkflowManager extends BaseModelManager
{
    public function getWorkflows($options)
    {
        $workflows = $this->repository->findWorkflows($options);
        $total_workflows = $this->repository->findWorkflows($options, true);

        $total_pages = ceil( $total_workflows / $options['page_size'] );

        $data = array(
            'page' => $options['page'],
            'page_size' => $options['page_size'],
            'total_pages' => $total_pages,
            'total' => $total_workflows,
            'workflows' => $workflows
        );

        return $data;
    }
}
