<?php

namespace Leadsius\ApiBundle\Model;

use Doctrine\ORM\EntityManager;

abstract class BaseModelManager
{
    protected $em;
    protected $class;
    protected $repository;
    protected $container;

    public function __construct(EntityManager $em, $class)
    {
        $this->em = $em;
        $this->repository = $em->getRepository($class);
        $metadata = $em->getClassMetadata($class);
        $this->class = $metadata->name;
        $this->container = null;
    }

    public function setContainer($container)
    {
        $this->container = $container;

        return $this;
    }

    public function getContainer()
    {
        return $this->container;
    }

    public function getDispatcher()
    {
        return $this->getContainer()->get('event_dispatcher');
    }

    public function getClass()
    {
        return $this->class;
    }

    public function create()
    {
        $class = $this->getClass();
        return new $class;
    }

    public function find($id)
    {
        return $this->repository->find($id);
    }

    public function reload($model)
    {
        $this->em->refresh($model);
    }

    public function save($model, $flush = true)
    {
        $this->em->persist($model);
        if ($flush) {
            $this->em->flush();
        }
    }

    public function delete($model, $flush = true)
    {
        $this->em->remove($model);
        if ($flush) {
            $this->em->flush();
        }
    }
}
