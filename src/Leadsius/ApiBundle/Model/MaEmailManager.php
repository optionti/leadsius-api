<?php

namespace Leadsius\ApiBundle\Model;
use Doctrine\ORM\Query\ResultSetMapping;

class MaEmailManager extends BaseModelManager
{
    public function getEmails($options)
    {
        $emails = $this->repository->findEmails($options);
        $total_emails = $this->repository->findEmails($options, true);

        $total_pages = ceil( $total_emails / $options['page_size'] );

        $data = array(
            'page' => $options['page'],
            'page_size' => $options['page_size'],
            'total_pages' => $total_pages,
            'total' => $total_emails,
            'emails' => $emails
        );

        return $data;
    }
}
