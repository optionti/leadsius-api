<?php

namespace Leadsius\ApiBundle\Model;
use Doctrine\ORM\Query\ResultSetMapping;

class PlContactManager extends BaseModelManager
{
    public function getContacts($options)
    {
        if (isset($options['filter']) && isset($options['account']) && null !== $this->getContainer()) {
            $filter = $this->em->getRepository('LeadsiusApiBundle:PlFilter')->find($options['filter']);

            if ($filter) {
                $filterAdvanced = json_decode( $filter->getAdvanced() , true );

                $filterBuilder = $this->container->get('leadsius_api.util.filter_builder_class');
                $filterBuilder->filterAddAdvancedFilter($filterAdvanced['advancedFilter'], $options['account']->getSystemKey());

                $qb = $this->repository->findContacts($options, false, true);
                $filterBuilder->generate($qb);

                $total_contacts = $qb
                    ->select('COUNT(DISTINCT PlContact)')
                    ->getQuery()
                    ->getSingleScalarResult()
                ;

                $skip = $options['page_size'] * ($options['page'] - 1);
                $contacts = $qb
                    ->select('DISTINCT PlContact')
                    ->setMaxResults($options['page_size'] )
                    ->setFirstResult( $skip )
                    ->getQuery()
                    ->getResult()
                ;
            }

        }

        if (!isset($contacts)) {
            $contacts = $this->repository->findContacts($options);
            $total_contacts = $this->repository->findContacts($options, true);
        }

        $idcontacts = array();
        for ($i = 0, $len = count($contacts); $i < $len; $i++) {
            $idcontacts[] = $contacts[$i]->getId();
        }
        
        $activities = $this->getContactActivities(implode(',',$idcontacts));
        
        for ($i = 0, $lenCo = count($contacts); $i < $lenCo; $i++) {
            for ($ii = 0, $lenAc = count($activities); $ii < $lenAc; $ii++) {
                if(isset($activities[$ii]['contact_id'])){
                    if($contacts[$i]->getId()===$activities[$ii]['contact_id']){
                        unset($activities[$ii]['contact_id']);
                        $contacts[$i]->setActivitiesSummary($activities[$ii]);
                    }
                }
            }
        }
        
        $total_pages = ceil( $total_contacts / $options['page_size'] );

        $data = array(
            'page' => $options['page'],
            'page_size' => $options['page_size'],
            'total_pages' => $total_pages,
            'total' => $total_contacts,
            'contacts' => $contacts,
        );

        return $data;
    }

    public function getContactActivities($contact_id) {
        
        if (null === $this->container) {
            return;
        }
        
        $em_rs = $this->container->get('doctrine.orm.redshift_entity_manager');
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('contact_id', 'contact_id');
        $rsm->addScalarResult('email_open', 'email_open');
        $rsm->addScalarResult('email_click', 'email_click');
        $rsm->addScalarResult('landingpage_view', 'landingpage_view');
        $rsm->addScalarResult('landingpage_click', 'landingpage_click');
        $rsm->addScalarResult('webform_view', 'webform_view');
        $rsm->addScalarResult('webform_submit', 'webform_submit');
        $rsm->addScalarResult('pages_view', 'pages_view');
        $rsm->addScalarResult('visits', 'visits');
        $rsm->addScalarResult('total', 'total');
        $rsm->addScalarResult('last_activity', 'last_activity');
        
        $sql = "
            SELECT DISTINCT
                dci.contact_id AS contact_id,
                dci.email_open AS email_open,
                dci.email_click AS email_click,
                dci.landingpage_view AS landingpage_view,
                dci.landingpage_click AS landingpage_click,
                dci.webform_view AS webform_view,
                dci.webform_submit AS webform_submit,
                dci.page_view AS pages_view,
                dci.visits AS visits,
                dci.total_activities AS total,
                ic.contact_last_activity AS last_activity
            FROM dim_contact_insight AS dci
              LEFT JOIN info_contact AS ic
                ON dci.contact_id = ic.contact_id
            WHERE ic.contact_id  IN ($contact_id)
        ";
        $query = $em_rs->createNativeQuery($sql, $rsm);
        
        return $activities = $query->getResult();
    }

    public function getActivities($options, $site_key, $contact_id)
    {
        if (null === $this->container) {
            return;
        }
        
        $page = 1;
        $page_size = 20;
        $sort = 'factmaster_id';
        $sort_dir = 'asc';
        $q = null;
        $wheres = null;
        $sort_fields = array(
            'factmaster_id',
            'factmaster_pageview_title',
            'factmaster_activity_type',
            'factmaster_activity_event',
            'factmaster_activity_type_event',
            'factmaster_datetime',
            'workflow_id',
            'workflow_name',
            'workflow_type',
            'landingpage_id',
            'landingpage_name',
            'webform_id',
            'webform_name',
            'email_id',
            'email_name',
        );
        
        if (null !== $options || is_array($options)) {
            $page = isset($options['page']) && is_int($options['page']) && 0 < $options['page'] ? $options['page'] : $page;
            $page_size = isset($options['page_size']) && is_int($options['page_size']) && 0 < $options['page_size'] ? $options['page_size'] : $page_size;
            $sort = isset($options['sort']) && in_array($options['sort'], $sort_fields) ? $options['sort'] : $sort;
            $sort_dir = isset($options['sort_dir']) && in_array($options['sort_dir'], array('asc', 'desc')) ? $options['sort_dir'] : $sort_dir;
            $q = isset($options['query']) ? $options['query'] : $q;
            $wheres = isset($options['wheres']) && is_array($options['wheres']) ? $options['wheres'] : $wheres;
        }
        
        $em_rs = $this->container->get('doctrine.orm.redshift_entity_manager');
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('factmaster_id', 'factmaster_id');
        $rsm->addScalarResult('factmaster_pageview_title', 'factmaster_pageview_title');
        $rsm->addScalarResult('factmaster_activity_type', 'factmaster_activity_type');
        $rsm->addScalarResult('factmaster_activity_event', 'factmaster_activity_event');
        $rsm->addScalarResult('factmaster_activity_type_event', 'factmaster_activity_type_event');
        $rsm->addScalarResult('factmaster_datetime', 'factmaster_datetime');
        $rsm->addScalarResult('workflow_id', 'workflow_id');
        $rsm->addScalarResult('workflow_name', 'workflow_name');
        $rsm->addScalarResult('workflow_type', 'workflow_type');
        $rsm->addScalarResult('landingpage_id', 'landingpage_id');
        $rsm->addScalarResult('landingpage_name', 'landingpage_name');
        $rsm->addScalarResult('webform_id', 'webform_id');
        $rsm->addScalarResult('webform_name', 'webform_name');
        $rsm->addScalarResult('email_id', 'email_id');
        $rsm->addScalarResult('email_name', 'email_name');
        
        $sqlAllTotal = "SELECT f.factmaster_id ";
       
        $sqlAll = "
            SELECT
                f.factmaster_id,
                f.factmaster_pageview_title,
                f.factmaster_activity_type,
                f.factmaster_activity_event,
                f.factmaster_activity_type_event,
                f.factmaster_datetime,
                iw.workflow_id,
                iw.workflow_name,
                iw.workflow_type,
                il.landingpage_id,
                il.landingpage_name,
                iwf.webform_id,
                iwf.webform_name,
                ie.email_id,
                ie.email_name ";
         $sql = "
            FROM fact_master AS f
            LEFT JOIN info_workflow AS iw
                ON f.factmaster_workflow_id = iw.workflow_id
            LEFT JOIN info_email AS ie
                ON f.factmaster_email_id = ie.email_id
            LEFT JOIN info_landingpage AS il
                ON f.factmaster_landingpage_id = il.landingpage_id
            LEFT JOIN info_webform AS iwf
                ON f.factmaster_webform_id = iwf.webform_id
            WHERE ((f.factmaster_guid_cookie IN (SELECT factmaster_guid_cookie FROM dim_contact_cookie d where d.factmaster_contact_id = :contact_id)
                        AND (f.factmaster_contact_id = :contact_id OR f.factmaster_contact_id IS NULL)) OR f.factmaster_contact_id = :contact_id )
            AND f.factmaster_site_key = :site_key
        ";
        
        if (null !== $q && '' !== $q) {
            $sql .= "
                AND (
                    UPPER(iw.workflow_name) LIKE UPPER(:query)
                    OR UPPER(il.landingpage_name) LIKE UPPER(:query)
                    OR UPPER(iwf.webform_name) LIKE UPPER(:query)
                    OR UPPER(ie.email_name) LIKE UPPER(:query)
                )
            ";
        }
        if (null !== $wheres && '' !== $wheres) {
            foreach ($wheres as $item_c => $item_d) {
                if ('' != $item_d) {
                    if('activity_type' == $item_c){
                        $sql .= " AND f.factmaster_activity_type = '{$item_d}'";
                    }

                    if('activity_type_event' == $item_c){
                        $sql .= " AND f.factmaster_activity_type_event = '{$item_d}'";
                    }

                    if('workflow_type' == $item_c){
                        $sql .= " AND iw.workflow_type = '{$item_d}'";
                    }
                }
            }
        }
        
        $sql .= " ORDER BY {$sort} {$sort_dir} ";
        {
            $query = $em_rs->createNativeQuery($sqlAllTotal.$sql, $rsm);
            $query->setParameter('contact_id', $contact_id);
            $query->setParameter('site_key', $site_key);

            if (null !== $q && '' !== $q) {
                $query->setParameter(':query', '%' . $q . '%');
            }

            $activitiesCount = $query->getResult();
        }
        
        $skip = $page_size * ($page - 1);
        $sql .= " LIMIT {$page_size} OFFSET {$skip} ";

        $query = $em_rs->createNativeQuery($sqlAll.$sql, $rsm);
        $query->setParameter('contact_id', $contact_id);
        $query->setParameter('site_key', $site_key);
        
        if (null !== $q && '' !== $q) {
            $query->setParameter(':query', '%' . $q . '%');
        }
        
        $activities = $query->getResult();
        
        $total_activities = count($activitiesCount);
        $total_pages = ceil( $total_activities / $page_size );
        $data = array(
            'page' => $page,
            'page_size' => $page_size,
            'total_pages' => $total_pages,
            'total' => $total_activities,
            'activities' => $activities,
        );
        
        return $data;
    }
}
