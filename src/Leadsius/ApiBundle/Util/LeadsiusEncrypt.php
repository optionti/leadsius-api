<?php

namespace Leadsius\ApiBundle\Util;


class LeadsiusEncrypt {
    private $password;

    public function __construct($password)
    {
        $this->password = $password;
    }

    public function encrypt($data)
    {
        return base64_encode(
                base64_encode(
                    mcrypt_encrypt(
                        MCRYPT_RIJNDAEL_256,
                        md5($this->password),
                        $data,
                        MCRYPT_MODE_CFB,
                        md5(md5($this->password))
                    )
                )
        );
    }

    public function decrypt($data)
    {
        $decrypted = rtrim(
            mcrypt_decrypt(
                MCRYPT_RIJNDAEL_256,
                md5($this->password),
                base64_decode(
                    base64_decode($data)
                ),
                MCRYPT_MODE_CFB,
                md5(md5($this->password))
            ),
            "\0"
        );

        $cad_get = explode('&', $decrypted); // Separo la url por &

        $queryReturn = array();
        foreach ($cad_get as $value) {
            list($key, $value) = explode('=', $value); // Asigno los valores al GET
            $queryReturn[$key] = utf8_decode($value);
        }

        return $queryReturn;
    }

    public function decryptParam($data)
    {
        return rtrim(
            mcrypt_decrypt(
                MCRYPT_RIJNDAEL_256,
                md5($this->password),
                base64_decode(base64_decode($data)),
                MCRYPT_MODE_CFB,
                md5(md5($this->password))
            ),
            "\0"
        );
    }
} 
