<?php


namespace Leadsius\ApiBundle\Util;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMapping;
use Leadsius\ApiBundle\Document\Transaction;

/**
 * Description of FilterBuilderClass
 *
 * @author scoppia
 */
class TrackerActivityType {
    const TYPE_OPEN = 1;
    const TYPE_CLICK = 2;
    const TYPE_BOUNCE = 3;
    const TYPE_UNSUBSCRIBE = 4;
    const TYPE_SUBMIT = 5;
    const TYPE_PAGE_VIEW = 6;
    const TYPE_VIEW = 7;
    const TYPE_STAY = 8;
}

class FactMasterActivityTypeEvent {
    const LANDINGPAGE_VIEW = 'LandingpageView';
    const WEBFORM_VIEW = 'WebformView';
    const PAGE_VIEW = 'Pageview';
    const LANDINGPAGE_WEBFORM_SUBMIT = 'LandingpageWebformSubmit';
    const EMAIL_CLICK = 'EmailClick';
    const EMAIL_OPEN = 'EmailOpen';
    const LANDINGPAGE_CLICK = 'LandingpageClick';
    const LANDINGPAGE_WEBFORM_VIEW = 'LandingpageWebformView';
    const WEBFORM_SUBMIT = 'WebformSubmit';
}

class TrackerActivityEvent {
    const EMAIL = "'ehtml'";
    const LANDING_PAGE = "'lphtml'";
    const WEB_FORM = "'wfhtml'";
    const LP_WB = "'lpwfhtml'";
}

class Select {
    private $select;

    function addSelect($string) {
        $this->select[] = $string;
    }

    function getSelect() {
        return $this->select;
    }
}

class FilterBuilderClass {

    private $em;
    private $trackerEm;
    private $query;
    private $queryTracker;
    private $emailCampaignLog;
    private $has_dinamic;
    private $has_log;
    private $res_filters;
    private $res_parameters;
    private $expr;
    private $parameterIndex;
    //
    private $delay = false;
    //activities
    private $debug = 1;
    //Debug
    private $messages = array();

    const ACTIVITY_TYPE_SENT = 'sent';
    const ACTIVITY_EVENT_OPEN = 'open';
    const ACTIVITY_EVENT_CLICK = 'click';
    const ACTIVITY_EVENT_SUBMIT = 'submit';
    const ACTIVITY_EVENT_VIEW = 'view';
    const LOGICAL_CONDITION_IN = 'IN';
    const LOGICAL_CONDITION_NOT_IN = 'NOT IN';

    const EM_TRACKER = 'tracker';

    public function __construct(EntityManager $em, EntityManager $dwhEm, DocumentManager $trackerEm) {
        $this->em = $em;
        $this->dwhEm = $dwhEm;
        $this->trackerEm = $trackerEm;

        $this->res_filters = array('query' => array(), 'tracker' => array(), 'emailCampaignLog' => array());
        $this->res_parameters = $this->res_filters;

        $this->parameterIndex = 0;
        $this->has_dinamic = false;
        $this->has_log = false;

        $this->expr = $this->em->createQueryBuilder()->expr();
    }

    /**
     *
     *   id_contact must be alias contactId
     */
    private function initializeSelect() {

//        $this->queryTracker = $this->trackerEm->createQueryBuilder()
//            ->select('DISTINCT(a.contactId) AS contactId')
//            ->from('LeadsiusApiBundle:Transaction', 'a');

        $this->queryTracker = $this->trackerEm->createQueryBuilder('LeadsiusApiBundle:Transaction');

        $this->emailCampaignLog = $this->em->createQueryBuilder()
            ->select('PlContact.id AS contactId')
            ->from('LeadsiusApiBundle:PlContact', 'PlContact')
            ->leftJoin('PlContact.emailCampaignLogs', 'ec');

        $this->webformResponses = $this->em->createQueryBuilder()
            ->select('DISTINCT(PlContact.id) AS contactId')
            ->from('LeadsiusApiBundle:MaWebformResponse', 'wr')
            ->leftJoin('wr.contact', 'PlContact');
    }

    public function setQueryTracker($queryTracker) {
        $this->queryTracker = $queryTracker;
    }

    public function filterAddAdvancedFilter($filtersAdvanced, $systemKey = false) {

        foreach ($filtersAdvanced as $key => $filterGroup) {
            foreach ($filterGroup as $filterKey => $filter) {
                $this->initializeSelect();

                switch ($filter['type']) {
                    case 'stable':

                        if (isset($filter['entity']) && $filter['entity'] == 'Activity'){
							$this->filterAddActivity($key, $filter, $systemKey);
						}                            
                        else{
                            $this->filterAddOperator($key, $filter);
						}
                        break;
                    case 'dynamic':
                        $this->filterAddDynamic($key, $filter);
                        break;
                    case 'all':
                    default:
                        break;
                }
            }

            $this->addGroupFilter();
        }
    }

    public function filterAddOperator($key, $filter) {
        extract($filter);

        $matches = array();
        if (preg_match('/^(?:contact_|company_)(.+)$/', $field, $matches)) {
            $field = $matches[1];
        }
        $matches = array();
        if (preg_match('/^id(?:Contact)$/', $field, $matches)) {
            $field = 'id';
        }
        $matches = array();
        if (preg_match('/^id(?:_company)$/', $field, $matches)) {
            $field = 'company';
        }
        $field = $this->camelcase($field);

        $newFilter = $this->newFilter('query', $operator, $entity, $field, $value);
        $this->addFilter('query', $key, $newFilter);
    }

    public function filterAddDynamic($key, $filter) {
        $this->has_dinamic = true;

        extract($filter);

        $matches = array();
        preg_match('/^([\w]+)DynamicField([\d]+)$/', $field, $matches);

        $idField = $matches[2];
        $entity = $matches[1];

        $_allowedEntitys = array(
            'contact' => 'contactDynamicFields',
            'company' => 'companyDynamicFields',
        );

        if (array_key_exists($entity, $_allowedEntitys))
            $entity = $_allowedEntitys[$entity];

        $newFilter = $this->expr->andx();
        $newFilter->add($this->newFilter('query', $operator, $entity, 'value', $value));
        $newFilter->add($this->newFilter('query', '=', $entity, 'field', $idField));

        $this->addFilter('query', $key, $newFilter);
    }

    public function filterAddActivity($key, $filter, $systemKey = false) {

        extract($filter);

        $this->messages[] = $field; //debug

        switch ($field) {
            case 'openedEmail':
                $type = 'tracker_openedEmail';

                $newFilter = $this->queryTracker;

				$newFilter->addAnd($this->newFilter($type, '=', '', 'siteKey', $systemKey));
                $newFilter->addAnd($this->newFilter($type, '=', '', 'transactionType', TrackerActivityType::TYPE_OPEN));
                $newFilter->addAnd($this->newFilter($type, 'IN', '', 'activity.emailId', $this->arrayValuesToInt($value)));

                if ($this->delay)
                    $newFilter->addAnd($this->newFilter($type, '<=', '', 'firstSeen', date_create($this->delay)));

                $this->addFilter($type, $key, $newFilter);
                
                $res = $this->generateFilters($type, $this->queryTracker);
                $newFilter = $this->newFilter('query', 'in', 'PlContact', 'id', $res);

                $this->addFilter('query', $key, $newFilter);
                break;
            case 'notOpenedEmail':

                //CampaignLog
                $type = 'emailCampaignLog_notOpenedEmail';

                $newFilter = $this->expr->andx();
                $newFilter->add($this->newFilter($type, '=', 'ec', 'activityType', self::ACTIVITY_TYPE_SENT));
                $newFilter->add($this->newFilter($type, 'in', 'ec', 'idEmail', $value));

                if ($this->delay)
                    $newFilter->add($this->newDelay($type, '<=', 'ec', 'created', date_create($this->delay))); //Delay

                $this->addFilter($type, $key, $newFilter);

                $res = $this->generateFilters($type, $this->emailCampaignLog);

                //Tracker
                $type = 'tracker_notOpenedEmail';

                $newFilter = $this->queryTracker;
				$newFilter->addAnd($this->newFilter($type, '=', '', 'siteKey', $systemKey));
                $newFilter->addAnd($this->newFilter($type, '=', '', 'transactionType', TrackerActivityType::TYPE_OPEN));
                $newFilter->addAnd($this->newFilter($type, 'in', 'activity', 'emailId', $this->arrayValuesToInt($value)));
                $newFilter->addAnd($this->newFilter($type, 'in', 'activity', 'contactId', $res));

                $this->addFilter($type, $key, $newFilter);

                $res = $this->generateFilters($type, $this->queryTracker);

                $newFilter = $this->newFilter('query', 'notIn', 'PlContact', 'id', $res);

                $this->addFilter('query', $key, $newFilter);

                break;
            case 'sentEmail':
                $type = 'emailCampaignLog_sentEmail';

                $newFilter = $this->expr->andx();
                $newFilter->add($this->newFilter($type, '=', 'ec', 'activityType', self::ACTIVITY_TYPE_SENT));
                $newFilter->add($this->newFilter($type, 'in', 'ec', 'idEmail', $value));

                if ($this->delay)
                    $newFilter->add($this->newDelay($type, '<=', 'ec', 'created', $this->delay)); //Delay

                $this->addFilter($type, $key, $newFilter);

                $res = $this->generateFilters($type, $this->emailCampaignLog);

                $newFilter = $this->newFilter('query', 'in', 'PlContact', 'id', $res);

                $this->addFilter('query', $key, $newFilter);

                break;
            case 'emailNotSent':
                $type = 'emailCampaignLog_emailNotSent';

                $newFilter = $this->expr->andx();
                $newFilter->add($this->newFilter($type, '=', 'ec', 'activityType', self::ACTIVITY_TYPE_SENT));
                $newFilter->add($this->newFilter($type, 'in', 'ec', 'idEmail', $value));

                $this->addFilter($type, $key, $newFilter);

                $res = $this->generateFilters($type, $this->emailCampaignLog);

                $newFilter = $this->newFilter('query', 'notIn', 'PlContact', 'id', $res);

                $this->addFilter('query', $key, $newFilter);
                break;
            case 'clickEmail':
                $type = 'tracker_clickEmail';

                $newFilter = $this->queryTracker;
				$newFilter->addAnd($this->newFilter($type, '=', '', 'siteKey', $systemKey));
                $newFilter->addAnd($this->newFilter($type, '=', '', 'transactionType', TrackerActivityType::TYPE_CLICK));
                $newFilter->addAnd($this->newFilter($type, '=', 'activity', 'emailId', (int)$value));
                $newFilter->addAnd($this->newFilter($type, 'in', 'activity', 'paramValue', $links));

                if ($this->delay)
                    $newFilter->addAnd($this->newFilter($type, '<=', '', 'firstSeen', date_create($this->delay)));

                $this->addFilter($type, $key, $newFilter);

                $res = $this->generateFilters($type, $this->queryTracker);

                $this->messages[] = "'query', 'in', 'PlContact', 'id', " . implode(',', $res);

                $newFilter = $this->newFilter('query', 'in', 'PlContact', 'id', $res);

                $this->addFilter('query', $key, $newFilter);

                break;
            case 'filledInForm':
                $type = 'webform_filledInForm';

                $newFilter = $this->expr->andx();
                $newFilter->add($this->newFilter($type, 'in', 'wr', 'webform', $value)); // idWebform => id

                if ($this->delay)
                    $newFilter->add($this->newDelay($type, '<=', 'wr', 'created', date_create($this->delay))); //Delay

                $this->addFilter($type, $key, $newFilter);

                $res = $this->generateFilters($type, $this->webformResponses);
                $newFilter = $this->newFilter('query', 'in', 'PlContact', 'id', $res);

                $this->addFilter('query', $key, $newFilter);
                break;
            case 'viewLandingPage':
                $type = 'tracker_viewLandingPage';

                $newFilter = $this->queryTracker;
				$newFilter->addAnd($this->newFilter($type, '=', '', 'siteKey', $systemKey));
                $newFilter->addAnd($this->newFilter($type, '=', '', 'transactionType', TrackerActivityType::TYPE_VIEW));
                $newFilter->addAnd($this->newFilter($type, 'in', 'activity', 'landingPageId', $this->arrayValuesToInt($value)));

                if ($this->delay)
                    $newFilter->addAnd($this->newFilter($type, '<=', '', 'firstSeen', date_create($this->delay)));

                $this->addFilter($type, $key, $newFilter);

                $res = $this->generateFilters($type, $this->queryTracker);

                $newFilter = $this->newFilter('query', 'in', 'PlContact', 'id', $res);

                $this->addFilter('query', $key, $newFilter);
                break;
            case 'clickLandingPage':
                $type = 'tracker_clickLandingPage';

                $newFilter = $this->queryTracker;
				$newFilter->addAnd($this->newFilter($type, '=', '', 'siteKey', $systemKey));
                $newFilter->addAnd($this->newFilter($type, '=', '', 'transactionType', TrackerActivityType::TYPE_CLICK));
                $newFilter->addAnd($this->newFilter($type, '=', 'activity', 'landingPageId', (int)$value));
                $newFilter->addAnd($this->newFilter($type, 'in', 'activity', 'paramValue', $links));

                if ($this->delay)
                    $newFilter->addAnd($this->newFilter($type, '<=', '', 'firstSeen', date_create($this->delay)));

                $this->addFilter($type, $key, $newFilter);

                $res = $this->generateFilters($type, $this->queryTracker);

                $newFilter = $this->newFilter('query', 'in', 'PlContact', 'id', $res);

                $this->addFilter('query', $key, $newFilter);
                break;
            case 'viewWebform':
                $type = 'tracker_viewWebform';

                $newFilter = $this->queryTracker;
				$newFilter->addAnd($this->newFilter($type, '=', '', 'siteKey', $systemKey));
                $newFilter->addAnd($this->newFilter($type, '=', '', 'transactionType', TrackerActivityType::TYPE_VIEW));
                $newFilter->addAnd($this->newFilter($type, 'in', 'activity', 'webformId', $this->arrayValuesToInt($value)));

                if ($this->delay)
                    $newFilter->addAnd($this->newFilter($type, '<=', '', 'firstSeen', date_create($this->delay)));

                $this->addFilter($type, $key, $newFilter);

                $res = $this->generateFilters($type, $this->queryTracker);

                $newFilter = $this->newFilter('query', 'in', 'PlContact', 'id', $res);

                $this->addFilter('query', $key, $newFilter);
                break;
            case 'firstActivityDate':
                $column = 'contact_first_activity';
            case 'lastActivityDate':
                $column = 'contact_last_activity';
            case 'activityDate':

                if (!isset($column))
                    break;

                $type = 'dwh_firstActivityDate';

                $rsm = new \Doctrine\ORM\Query\ResultSetMapping;
                $rsm->addScalarResult('contact_id', 'contactId');


                $delay = ($this->delay) ? "AND $column <= {$this->delay}" : ''; //Delay

                $sql = "
                    SELECT DISTINCT
                        f.factmaster_contact_id AS contact_id
                    FROM fact_master AS f
                    LEFT JOIN info_contact AS ic
                    ON f.factmaster_contact_id = ic.contact_id
                    WHERE f.factmaster_site_key = :site_key
                    AND DATE(ic.contact_last_activity) = :last_activity
                    {$delay}
                ";
                $last_activity = date('Y-m-d', strtotime($value));

                $query = $this->dwhEm->createNativeQuery($sql, $rsm);

                $query->setParameter('last_activity', $last_activity);
                $query->setParameter('site_key', $systemKey);

                $res = $this->formatIds($query->execute());

                $newFilter = $this->newFilter('query', 'in', 'PlContact', 'id', $res);

                $this->addFilter('query', $key, $newFilter);

                break;
            case 'contact_creation_date':

                $type = 'contact_firstActivityDate';

                $rsm = new \Doctrine\ORM\Query\ResultSetMapping;
                $rsm->addScalarResult('id', 'contactId');

                $delay = ($this->delay) ? "AND $column <= {$this->delay}" : ''; //Delay

                if ($this->delay)
                    $delay = "AND $column <= {$this->delay}"; //Delay

                $query = $this->em->createNativeQuery("SELECT id_contact as id
                        FROM pl_contact
                        WHERE MONTH(created) = :month AND
                        YEAR(created) = :year;
                        $delay
                        ", $rsm);
                $query->setParameter('month', date('m', strtotime($value)));
                $query->setParameter('year', date('Y', strtotime($value)));
                $res = $this->formatIds($query->execute());

                $newFilter = $this->newFilter('query', 'in', 'PlContact', 'id', $res);

                $this->addFilter('query', $key, $newFilter);

                break;
            case 'numberOfTotalActivities':
                $type = 'dwh_numberOfTotalActivities';

                $rsm = new ResultSetMapping();
                $rsm->addScalarResult('contact_id', 'contactId');

                $sql = "
                    SELECT DISTINCT
                        dci.contact_id
                    FROM fact_master AS f
                    LEFT JOIN dim_contact_cookie AS dcc
                    ON f.factmaster_guid_cookie = dcc.factmaster_guid_cookie
                    LEFT JOIN dim_contact_insight AS dci
                    ON dcc.factmaster_contact_id = dci.contact_id
                    WHERE f.factmaster_site_key = :site_key
                    AND dci.total_activities {$operator} :value
                ";

                $query = $this->dwhEm->createNativeQuery($sql, $rsm);

                $query->setParameter('value', $value);
                $query->setParameter('site_key', $systemKey);
                $contacts = $query->getResult();
                $contactsId = $this->formatIds( $contacts );

                $newFilter = $this->newFilter('query', 'in', 'PlContact', 'id', $contactsId);

                $this->addFilter('query', $key, $newFilter);
                break;
            case 'numberOfOpenEmails':
                $type = 'dwh_numberOfOpenEmails';

                $rsm = new ResultSetMapping();
                $rsm->addScalarResult('contact_id', 'contactId');

                $sql = "
                    SELECT DISTINCT
                        dci.contact_id
                    FROM fact_master AS f
                    LEFT JOIN dim_contact_cookie AS dcc
                    ON f.factmaster_guid_cookie = dcc.factmaster_guid_cookie
                    LEFT JOIN dim_contact_insight AS dci
                    ON dcc.factmaster_contact_id = dci.contact_id
                    WHERE f.factmaster_site_key = :site_key
                    AND email_open {$operator} :value
                ";

                $query = $this->dwhEm->createNativeQuery($sql, $rsm);

                $query->setParameter('value', $value);
                $query->setParameter('site_key', $systemKey);
                $contacts = $query->getResult();
                $contactsId = $this->formatIds( $contacts );

                $newFilter = $this->newFilter('query', 'in', 'PlContact', 'id', $contactsId);

                $this->addFilter('query', $key, $newFilter);
                break;
            case 'numberOfEmailClicks':
                $type = 'dwh_numberOfEmailClicks';

                $rsm = new ResultSetMapping();
                $rsm->addScalarResult('contact_id', 'contactId');

                $sql = "
                    SELECT DISTINCT
                        dci.contact_id
                    FROM fact_master AS f
                    LEFT JOIN dim_contact_cookie AS dcc
                    ON f.factmaster_guid_cookie = dcc.factmaster_guid_cookie
                    LEFT JOIN dim_contact_insight AS dci
                    ON dcc.factmaster_contact_id = dci.contact_id
                    WHERE f.factmaster_site_key = :site_key
                    AND email_click {$operator} :value
                ";

                $query = $this->dwhEm->createNativeQuery($sql, $rsm);

                $query->setParameter('value', $value);
                $query->setParameter('site_key', $systemKey);
                $contacts = $query->getResult();
                $contactsId = $this->formatIds( $contacts );

                $newFilter = $this->newFilter('query', 'in', 'PlContact', 'id', $contactsId);

                $this->addFilter('query', $key, $newFilter);
                break;
            case 'numberOfPageviews':
                $type = 'dwh_numberOfPageviews';

                $rsm = new ResultSetMapping();
                $rsm->addScalarResult('contact_id', 'contactId');

                $sql = "
                    SELECT DISTINCT
                        dci.contact_id
                    FROM fact_master AS f
                    LEFT JOIN dim_contact_cookie AS dcc
                    ON f.factmaster_guid_cookie = dcc.factmaster_guid_cookie
                    LEFT JOIN dim_contact_insight AS dci
                    ON dcc.factmaster_contact_id = dci.contact_id
                    WHERE f.factmaster_site_key = :site_key
                    AND landingpage_view {$operator} :value
                ";

                $query = $this->dwhEm->createNativeQuery($sql, $rsm);

                $query->setParameter('value', $value);
                $query->setParameter('site_key', $systemKey);
                $contacts = $query->getResult();
                $contactsId = $this->formatIds( $contacts );

                $newFilter = $this->newFilter('query', 'in', 'PlContact', 'id', $contactsId);

                $this->addFilter('query', $key, $newFilter);
                break;
            case 'numberOfLandingPageClicks':
                $type = 'dwh_numberOfLandingPageClicks';

                $rsm = new ResultSetMapping();
                $rsm->addScalarResult('contact_id', 'contactId');

                $sql = "
                    SELECT DISTINCT
                        dci.contact_id
                    FROM fact_master AS f
                    LEFT JOIN dim_contact_cookie AS dcc
                    ON f.factmaster_guid_cookie = dcc.factmaster_guid_cookie
                    LEFT JOIN dim_contact_insight AS dci
                    ON dcc.factmaster_contact_id = dci.contact_id
                    WHERE f.factmaster_site_key = :site_key
                    AND landingpage_click {$operator} :value
                ";

                $query = $this->dwhEm->createNativeQuery($sql, $rsm);

                $query->setParameter('value', $value);
                $query->setParameter('site_key', $systemKey);
                $contacts = $query->getResult();
                $contactsId = $this->formatIds( $contacts );

                $newFilter = $this->newFilter('query', 'in', 'PlContact', 'id', $contactsId);

                $this->addFilter('query', $key, $newFilter);
                break;
            case 'numberOfWebsiteVisits':
                $type = 'dwh_numberOfWebsiteVisits';

                $rsm = new ResultSetMapping();
                $rsm->addScalarResult('contact_id', 'contactId');

                $sql = "
                    SELECT DISTINCT
                        dci.contact_id
                    FROM fact_master AS f
                    LEFT JOIN dim_contact_cookie AS dcc
                    ON f.factmaster_guid_cookie = dcc.factmaster_guid_cookie
                    LEFT JOIN dim_contact_insight AS dci
                    ON dcc.factmaster_contact_id = dci.contact_id
                    WHERE f.factmaster_site_key = :site_key
                    AND visits {$operator} :value
                ";

                $query = $this->dwhEm->createNativeQuery($sql, $rsm);

                $query->setParameter('value', $value);
                $query->setParameter('site_key', $systemKey);
                $contacts = $query->getResult();
                $contactsId = $this->formatIds( $contacts );

                $newFilter = $this->newFilter('query', 'in', 'PlContact', 'id', $contactsId);

                $this->addFilter('query', $key, $newFilter);
                break;
            case 'numberOfWebFormSubmits':
                $type = 'dwh_numberOfWebFormSubmits';

                $rsm = new ResultSetMapping();
                $rsm->addScalarResult('contact_id', 'contactId');

                $sql = "
                    SELECT DISTINCT
                        dci.contact_id
                    FROM fact_master AS f
                    LEFT JOIN dim_contact_cookie AS dcc
                    ON f.factmaster_guid_cookie = dcc.factmaster_guid_cookie
                    LEFT JOIN dim_contact_insight AS dci
                    ON dcc.factmaster_contact_id = dci.contact_id
                    WHERE f.factmaster_site_key = :site_key
                    AND webform_submit {$operator} :value
                ";

                $query = $this->dwhEm->createNativeQuery($sql, $rsm);

                $query->setParameter('value', $value);
                $query->setParameter('site_key', $systemKey);
                $contacts = $query->getResult();
                $contactsId = $this->formatIds( $contacts );

                $newFilter = $this->newFilter('query', 'in', 'PlContact', 'id', $contactsId);

                $this->addFilter('query', $key, $newFilter);
                break;
            case 'notClickedEmail': // All e-mails
                // Condition: activityEvent not equal "click" value
                $type = 'tracker_notClickedEmail';

                $newFilter = $this->queryTracker;
				$newFilter->addAnd($this->newFilter($type, '=', '', 'siteKey', $systemKey));
                $newFilter->addAnd($this->newFilter($type, '=', '', 'transactionType', TrackerActivityType::TYPE_CLICK));
                $newFilter->addAnd($this->newFilter($type, '=', 'activity', 'emailId', (int)$value));
                $newFilter->addAnd($this->newFilter($type, 'in', 'activity', 'paramValue', $links));


                if ($this->delay)
                    $newFilter->addAnd($this->newFilter($type, '<=', '', 'firstSeen', date_create($this->delay)));

                $this->addFilter($type, $key, $newFilter); // Ask for key

                $res = $this->generateFilters($type, $this->queryTracker);

                $this->messages[] = "'query', 'notin', 'PlContact', 'id', " . implode(',', $res);

                $newFilter = $this->newFilter('query', 'notin', 'PlContact', 'id', $res);

                $this->addFilter('query', $key, $newFilter);
                break;
            case 'notSubmitedWebForm':
                // Condition: idContact not null
                $type = 'webformResponse_notSubmitedWebForm';

                $newFilter = $this->expr->andx();
                $newFilter->add($this->newFilter($type, '<>', 'wr', 'contact', 'NULL'));
                $newFilter->add($this->newFilter($type, 'in', 'wr', 'webform', $value)); // idWebform => id

                if ($this->delay)
                    $newFilter->add($this->newDelay($type, '<=', 'wr', 'created', date_create($this->delay)));

                $this->addFilter($type, $key, $newFilter);

                $newFilter = $this->newFilter('query', '=', 'PlContact', 'account', $idAccount);
                $this->addFilter('query', $key, $newFilter);

                $res = $this->generateFilters($type, $this->webformResponses);

                $newFilter = $this->newFilter('query', 'notIn', 'PlContact', 'id', $res);

                $this->addFilter('query', $key, $newFilter);
                break;
            case 'notClickedLandingPage':
                // Condition: activityEvent not equal "click" value, and webFormId in $value
                $type = 'tracker_notClickedLandingPage';

                $newFilter = $this->queryTracker;
				$newFilter->addAnd($this->newFilter($type, '=', '', 'siteKey', $systemKey));
                $newFilter->addAnd($this->newFilter($type, '=', '', 'transactionType', TrackerActivityType::TYPE_CLICK));
                $newFilter->addAnd($this->newFilter($type, '=', 'activity', 'landingPageId', (int)$value));
                $newFilter->addAnd($this->newFilter($type, 'in', 'activity', 'paramValue', $links));

                if ($this->delay)
                    $newFilter->addAnd($this->newFilter($type, '<=', '', 'firstSeen', date_create($this->delay)));

                $this->addFilter($type, $key, $newFilter); // Ask for key

                $res = $this->generateFilters($type, $this->queryTracker);

                $newFilter = $this->newFilter('query', 'notin', 'PlContact', 'id', $res);

                $this->addFilter('query', $key, $newFilter);
                break;
            case 'notViewedLandingPage':
                $type = 'tracker_notViewedLandingPage';

                $newFilter = $this->queryTracker;
				$newFilter->addAnd($this->newFilter($type, '=', '', 'siteKey', $systemKey));
                $newFilter->addAnd($this->newFilter($type, '=', '', 'transactionType', TrackerActivityType::TYPE_VIEW));
                $newFilter->addAnd($this->newFilter($type, '=', 'activity', 'landingPageId', (int)$value));
                $newFilter->addAnd($this->newFilter($type, 'in', 'activity', 'paramValue', $links));

                if ($this->delay)
                    $newFilter->addAnd($this->newFilter($type, '<=', '', 'firstSeen', date_create($this->delay)));

                $this->addFilter($type, $key, $newFilter); // Ask for key

                $res = $this->generateFilters($type, $this->queryTracker);

                $newFilter = $this->newFilter('query', 'notin', 'PlContact', 'id', $res);

                $this->addFilter('query', $key, $newFilter);
                break;
            case 'notViewedWebForm':
                $type = 'tracker_notViewedWebForm';

                $newFilter = $this->queryTracker;
				$newFilter->addAnd($this->newFilter($type, '=', '', 'siteKey', $systemKey));
                $newFilter->addAnd($this->newFilter($type, '=', '', 'transactionType', TrackerActivityType::TYPE_VIEW));
                $newFilter->addAnd($this->newFilter($type, 'in', 'activity', 'webformId', $this->arrayValuesToInt($value)));

                if ($this->delay)
                    $newFilter->addAnd($this->newFilter($type, '<=', '', 'firstSeen', date_create($this->delay)));

                $this->addFilter($type, $key, $newFilter); // Ask for key

                $res = $this->generateFilters($type, $this->queryTracker);

                $newFilter = $this->newFilter('query', 'notin', 'PlContact', 'id', $res);

                $this->addFilter('query', $key, $newFilter);
                break;
            case 'viewPageview':
                $type = 'dwh_viewPageview';

                $rsm = new ResultSetMapping();
                $rsm->addScalarResult('contact_id', 'contactId');

                $query = $this->dwhEm->createNativeQuery("
                    SELECT DISTINCT
                        ic.contact_id
                    FROM fact_master AS f
                        LEFT JOIN dim_contact_cookie AS dcc
                            ON f.factmaster_guid_cookie = dcc.factmaster_guid_cookie
                        LEFT JOIN info_contact AS ic
                            ON dcc.factmaster_contact_id = ic.contact_id
                    WHERE ic.contact_id_account =  :account_id
                        AND f.factmaster_activity_type_event = :activity_type_event
                        AND f.factmaster_activity_event = :activity_event
                        AND f.factmaster_pageview_url IN (:pageview_urls)
                        AND f.factmaster_guid_cookie IS NOT NULL
                        AND ic.contact_id IS NOT NULL
                ", $rsm);

                $query->setParameter('account_id', $idAccount);
                $query->setParameter('activity_type_event', FactMasterActivityTypeEvent::PAGE_VIEW);
                $query->setParameter('activity_event', TrackerActivityType::TYPE_PAGE_VIEW);
                $query->setParameter('pageview_urls', $value);
                $contacts = $query->getResult();
                $contactsId = $this->formatIds( $contacts );

                $newFilter = $this->newFilter('query', 'in', 'PlContact', 'id', $contactsId);

                $this->addFilter('query', $key, $newFilter);

                break;
            default:
                $this->addFilter('query', $key, $this->newFilter('query', 'in', 'PlContact', 'id', array('-1')));
                break;
        }
    }

    private function arrayValuesToInt($array) {
        return array_map('intval', $array);
    }

    public function addFilter($logType, $key, $where) {
        $this->addFilterAdvanced($logType, $key, "", $where);
    }

    private function addFilterAdvanced($logType, $key, $select, $where) {
        $this->addSelect($logType, $key, $select);
        $this->addWhere($logType, $key, $where);
    }

    private function addSelect($logType, $key, $select) {
        $this->res_filters[$logType]['filter_group_select'][$key] = $select;
    }

    private function addWhere($logType, $key, $where) {
        $this->res_filters[$logType]['filter_group'][$key][] = $where;
    }

    private function newDelay($filterType, $operator, $entity, $field, $value, $parser = false) {
        return $this->newFilter($filterType, $operator, $entity, $field, $value);
    }

    public function newFilter($filterType, $operator, $entity, $field, $value, $parser = false) {

        $parameterIndex = $this->nextParameterIndex();

        $column = $field;

        if (isset($entity) && trim($entity) != '')
            $column = "$entity.$field";

        if ($parser) {
            $column = "$parser($column)";
        }
        
        if (strpos($filterType, self::EM_TRACKER) !== false) {
            return $this->newFilterFromMongo($filterType, $operator, $column, $value, $parameterIndex);
        }

        return $this->newFilterFromMysql($filterType, $operator, $column, $value, $parameterIndex);

    }

    public function newFilterFromMongo($filterType, $operator, $column, $value, $parameterIndex) {

        switch (strtoupper($operator)) {
            case '=':
                //Is equal to
                $res = $this->queryTracker->expr()->field($column)->equals($value);
                break;
            case '<>':
                //Is not equal to
                $res = $this->queryTracker->expr()->field($column)->notEqual($value);
                break;
            case 'IN':
                //In
                $res = $this->queryTracker->expr()->field($column)->in($value);
                break;
            case 'NOTIN':
                //NotIn
                $res = $this->queryTracker->expr()->field($column)->notIn($value);
                break;
            case '<':
                //Less
                $res = $this->queryTracker->expr()->field($column)->lt($value);
                break;
            case '<=':
                //less or equal
                $res = $this->queryTracker->expr()->field($column)->lte($value);
                break;
            case '>':
                //greater
                $res = $this->queryTracker->expr()->field($column)->gt($value);
                break;
            case '>=':
                //greater or equal
                $res = $this->queryTracker->expr()->field($column)->gte($value);
                break;
            default:
                die('FilterBuilderClass->newFilter() need valid operator');
                break;
        }

        return $res;
    }

    private function newFilterFromMysql($filterType, $operator, $column, $value, $parameterIndex) {

        switch (strtoupper($operator)) {
            case '=':
                //Is equal to
                $res = $this->expr->eq($column, ":parameter_$parameterIndex");
                $this->res_parameters[$filterType]["parameter_$parameterIndex"] = "$value";
                break;
            case '<>':
                //Is not equal to
                $res = $this->expr->neq($column, ":parameter_$parameterIndex");
                $this->res_parameters[$filterType]["parameter_$parameterIndex"] = $value;
                break;
            case 'LIKE ~%':
                //Starts with
                $res = $this->expr->like($column, ":parameter_$parameterIndex");
                $this->res_parameters[$filterType]["parameter_$parameterIndex"] = "$value%";
                break;
            case 'LIKE %~':
                //Ends with
                $res = $this->expr->like($column, ":parameter_$parameterIndex");
                $this->res_parameters[$filterType]["parameter_$parameterIndex"] = "%$value";
                break;
            case 'LIKE %~%':
                //Contains
                $res = $this->expr->like($column, ":parameter_$parameterIndex");
                $this->res_parameters[$filterType]["parameter_$parameterIndex"] = "%$value%";
                break;
            case 'NOT LIKE %~%':
                //Does not contain
                $res = $this->expr->not($this->expr->like($column, ":parameter_$parameterIndex"));
                $this->res_parameters[$filterType]["parameter_$parameterIndex"] = "%$value%";
                break;
            case 'IN':
                //In
                $res = $this->expr->in($column, ":parameter_$parameterIndex");
                $this->res_parameters[$filterType]["parameter_$parameterIndex"] = $value;
                break;
            case 'NOTIN':
                //NotIn
                $res = $this->expr->notIn($column, ":parameter_$parameterIndex");
                $this->res_parameters[$filterType]["parameter_$parameterIndex"] = $value;
                break;
            case '<':
                //Less
                $res = $this->expr->lt($column, ":parameter_$parameterIndex");
                $this->res_parameters[$filterType]["parameter_$parameterIndex"] = $value;
                break;
            case '<=':
                //less or equal
                $res = $this->expr->lte($column, ":parameter_$parameterIndex");
                $this->res_parameters[$filterType]["parameter_$parameterIndex"] = $value;
                break;
            case '>':
                //greater
                $res = $this->expr->gt($column, ":parameter_$parameterIndex");
                $this->res_parameters[$filterType]["parameter_$parameterIndex"] = $value;
                break;
            case '>=':
                //greater or equal
                $res = $this->expr->gte($column, ":parameter_$parameterIndex");
                $this->res_parameters[$filterType]["parameter_$parameterIndex"] = $value;
                break;
            default:
                die('FilterBuilderClass->newFilter() need valid operator');
                break;
        }

        return $res;
    }

    public function addGroupFilter() {

    }

    public function generate(&$query, $logicalCondition = 'or') {

        if (count($this->res_filters['query']) > 0) {
            $where = $this->generateFilterWhere('query', $logicalCondition);
            $this->messages[] = (string)$where; //debug
            $query->andWhere($where);
        }

        if ($this->res_parameters['query'])
            foreach ($this->res_parameters['query'] as $key => $parameter) {

                $query->setParameter($key, $parameter);

                if (is_array($parameter))
                    $parameter = implode(',', $parameter);

                $this->messages[] = "$key, $parameter"; //debug
            }

        return $this->messages;
    }

    public function generateFilters($type, $queryContent) {
        if (count($this->res_filters[$type]) <= 0)
            return false;

        if (strpos($type, self::EM_TRACKER) !== false) {
            return $this->generateFromMongo($type, $queryContent);
        }

        return $this->generateFromMysql($type, $queryContent);


    }

    private function generateFromMongo($type, $queryContent) {

        $filterContacts = array();

        foreach ($queryContent->getQuery()->execute() as $key => $result) {
            if (trim($result->getActivity()->getContactId()) != '')
                $filterContacts[] = $result->getActivity()->getContactId();
        }
        
        return ($filterContacts && count($filterContacts) > 0) ? $filterContacts : array('-1');
    }

    private function generateFromMysql($type, $queryContent) {

        $where = $this->generateFilterWhere($type);

        $this->messages[] = (string)$where; //debug

        $queryContent->orWhere($where);
        if ($this->res_parameters[$type])
            foreach ($this->res_parameters[$type] as $key => $parameter) {

                $queryContent->setParameter($key, $parameter);


                if (is_array($parameter))
                    $parameter = implode(',', $parameter);
                if (is_a($parameter,'DateTime'))
                    $this->messages[] = "$key, ".date_format($parameter, 'Y-m-d H:i:s'); //debug
                else
                    $this->messages[] = "$key, $parameter"; //debug
            }


        $contacts = $this->formatIds($queryContent->getQuery()->execute());

        $contacts = ($contacts && count($contacts) > 0) ? $contacts : array('-1');

        return $contacts;
    }

    private function generateNativeFilters($type, $queryContent) {
        if (count($this->res_filters[$type]) <= 0)
            return false;

        $rsm = new \Doctrine\ORM\Query\ResultSetMapping;
        $rsm->addScalarResult('contactId', 'contactId');
        $query = $this->dwhEm->createNativeQuery("
            WITH pseudotable AS (
                SELECT factmaster_contact_id as contactId
                " . $this->generateFilterSelect($type) . "
                FROM fact_master f
                GROUP BY contactId
            )
            SELECT *
            FROM pseudotable
            WHERE " . $this->generateFilterWhere($type) . "
        	", $rsm);

//        echo "
//            WITH pseudotable AS (
//                SELECT factmaster_contact_id as contactId
//                " . $this->generateFilterSelect($type) . "
//                FROM fact_master f
//                GROUP BY contactId
//            )
//            SELECT *
//            FROM pseudotable
//            WHERE " . $this->generateFilterWhere($type) ;
//        die;

        if ($this->res_parameters[$type])
            foreach ($this->res_parameters[$type] as $key => $parameter) {
                $query->setParameter($key, $parameter);
            }

        $contacts = $this->formatIds($query->execute());

        $contacts = ($contacts && count($contacts) > 0) ? $contacts : array('-1');

        return $contacts;
    }

    private function nextParameterIndex() {
        return $this->parameterIndex += 1;
    }

    private function generateFilterSelect($type) {
        $select = array();

        foreach ($this->res_filters[$type]['filter_group_select'] as $key => $value) {
            if (count($value)) {
                $select[] = implode(', ', $value);
            }
        }


        if (count($select))
            return ',' . implode(', ', $select);

        return '';
    }

    private function generateFilterWhere($type, $logicalCondition = 'or') {
        $orX = $this->expr->orX();
        if ($logicalCondition != 'or')
            $orX = $this->expr->andX();

        foreach ($this->res_filters[$type]['filter_group'] as $key => $filterGroup) {

            $andX = $this->expr->andx();
            foreach ($filterGroup as $key => $filter) {

                $andX->add(
                    $this->expr->andx($filter)
                );
            }

            $orX->add($andX);
        }
        return $orX;
    }

    private function formatIds($filterData) {
        if (count($filterData) <= 0)
            return false;

        $filterContacts = array();

        foreach ($filterData as $key => $result) {
            if (trim($result['contactId']) != '')
                $filterContacts[] = $result['contactId'];
        }

        return $filterContacts;
    }

    private function camelcase($string) {
        $string = explode('_', $string);
        array_walk_recursive($string, function (&$item, $key) {
            $item = ucfirst($item);
        });
        return lcfirst(implode('', $string));
    }

    public function setDelay($delay) {
        $this->delay = $delay;
    }

    public function addDelay($delay) {
        $this->delay = $delay;
    }

    static public function log($data) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";

    }
}

?>
