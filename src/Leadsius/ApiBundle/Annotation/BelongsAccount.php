<?php

namespace Leadsius\ApiBundle\Annotation;

use Doctrine\Common\Annotations\Annotation;
use Doctrine\Common\Annotations\Annotation\Target;
use Doctrine\Common\Annotations\Annotation\Attributes;
use Doctrine\Common\Annotations\Annotation\Attribute;

/**
 * Class BelongsAccountAnnotation
 * @package Leadsius\ApiBundle\Annotation
 *
 * @Annotation
 * @Target("METHOD")
 * @Attributes({
 *  @Attribute("name", type="string", required=true),
 *  @Attribute("class", type="string", required=true)
 * })
 */
class BelongsAccount
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $class;
} 
