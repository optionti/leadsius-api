<?php

namespace Leadsius\ApiBundle\EventListener;

use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\EntityManager;
use Leadsius\ApiBundle\Annotation\BelongsAccount;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\SecurityContext;

class BelongsAccountListener
{
    private $annotationReader;
    private $securityContext;
    private $entityManager;

    public function __construct(Reader $annotationReader, SecurityContext $securityContext, EntityManager $entityManager)
    {
        $this->annotationReader = $annotationReader;
        $this->securityContext = $securityContext;
        $this->entityManager = $entityManager;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();
        $request = $event->getRequest();

        if (!is_array($controller)) {
            return;
        }

        if (!($request instanceof Request)) {
            return;
        }

        $action = new \ReflectionMethod($controller[0], $controller[1]);

        $annotation = $this
            ->annotationReader
            ->getMethodAnnotation(
                $action,
                'Leadsius\ApiBundle\Annotation\BelongsAccount'
            )
        ;

        if (null === $annotation) {
            return;
        }

        if (!($annotation instanceof BelongsAccount)) {
            return;
        }

        $user = $this->securityContext->getToken()->getUser();

        if (!$user) {
            return;
        }

        $account = $user->getAccount();

        if (!$account) {
            return;
        }

        if (!$request->attributes->has($annotation->name)) {
            return;
        }

        $repository = $this->entityManager->getRepository($annotation->class);
        $entity = $repository->find($request->attributes->get($annotation->name));

        if (!method_exists($entity, 'getAccount')) {
            return;
        }

        $entityAccount = $entity->getAccount();

        if (!$entityAccount) {
            return;
        }

        if ($account->getId() !== $entityAccount->getId()) {
            throw new NotFoundHttpException('The resource you were looking for doesn\'t exist');
        }
    }
} 
