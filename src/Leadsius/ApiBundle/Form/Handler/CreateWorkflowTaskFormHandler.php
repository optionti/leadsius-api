<?php

namespace Leadsius\ApiBundle\Form\Handler;


use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class CreateWorkflowTaskFormHandler
{
    public function __construct()
    {

    }

    public function handle(FormInterface $form, Request $request)
    {
        if (!$request->isMethod('POST')) {
            return false;
        }

        $form->handleRequest($request);

        if (!$form->isValid()) {
            return false;
        }

        return true;
    }
} 
