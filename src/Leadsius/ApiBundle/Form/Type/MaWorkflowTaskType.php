<?php

namespace Leadsius\ApiBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;

class MaWorkflowTaskType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type')
            ->add('condition')
            ->add(
                'startDate',
                'datetime',
                array(
                    'widget' => 'single_text',
                    'input' => 'datetime',
                    'required' => true,
                )
            )
            ->add(
                'endDate',
                'datetime',
                array(
                    'widget' => 'single_text',
                    'input' => 'datetime',
                    'required' => true,
                )
            )
            ->add('delay')
            ->add('active')
            ->add('emailCampaign', new MaEmailCampaignType())
            ->add('parentWorkflowTask')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Leadsius\ApiBundle\Entity\MaWorkflowTask'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return '';
    }
}
