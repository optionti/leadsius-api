<?php

namespace Leadsius\ApiBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PlContactType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname')
            ->add('lastname')
            ->add('email')
            ->add('phone')
            ->add('mobile_phone')
            ->add('doNotCall')
            ->add('do_not_email')
            ->add('do_not_email_until')
            ->add('bounce')
            ->add('reports_to')
            ->add('title')
            ->add('role')
            ->add('description')
            ->add('other')
            ->add('source')
            ->add('user_account')
            ->add('status')
            ->add('company')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Leadsius\ApiBundle\Entity\PlContact'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return '';
    }
}
