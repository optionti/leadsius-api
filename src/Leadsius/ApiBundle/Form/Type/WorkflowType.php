<?php

namespace Leadsius\ApiBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class WorkflowType extends MaWorkflowType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // TODO: Add Timezones: 'model_timezone' => 'America/Santiago', 'view_timezone' => 'GMT'
        $builder
            ->add('name')
            ->add('description')
            ->add(
                'startDate',
                'datetime',
                array(
                    'widget' => 'single_text',
                    'input' => 'datetime',
                    'required' => true,
                    'constraints' => array(
                        new Assert\NotBlank()
                    )
                )
            )
            ->add(
                'endDate',
                'datetime',
                array(
                    'widget' => 'single_text',
                    'input' => 'datetime',
                    'required' => true,
                    'constraints' => array(
                        new Assert\NotBlank()
                    )
                )
            )
        ;
    }
}
