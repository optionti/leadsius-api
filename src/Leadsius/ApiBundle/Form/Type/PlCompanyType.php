<?php

namespace Leadsius\ApiBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PlCompanyType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('website')
            ->add('employees')
            ->add('reveneu')
            ->add('industry')
            ->add('phone')
            ->add('address')
            ->add('pobox')
            ->add('postalCode')
            ->add('city')
            ->add('other')
            ->add('status')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Leadsius\ApiBundle\Entity\PlCompany'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return '';
    }
}
