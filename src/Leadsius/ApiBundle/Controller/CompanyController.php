<?php

namespace Leadsius\ApiBundle\Controller;

use Leadsius\ApiBundle\Annotation\BelongsAccount;
use Leadsius\ApiBundle\Entity\PlCompany;
use Leadsius\ApiBundle\Form\Type\PlCompanyType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpKernel\Exception\HttpException;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\View\View;

class CompanyController extends BaseController
{
    /**
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Companies",
     *  resource=true,
     *  description="Return all companies",
     *  parameters={
     *      {"name"="page", "dataType"="integer", "required"=false, "description"="Page to retrieve"},
     *      {"name"="page_size", "dataType"="integer", "required"=false, "description"="Contacts per page"},
     *      {"name"="sort", "dataType"="string", "required"=false, "description"="Fields to sort"},
     *      {"name"="sort_dir", "dataType"="string", "required"=false, "description"="Sort direction", "format"="asc|desc"},
     *      {"name"="query", "dataType"="string", "required"=false, "description"="Query to filter result"}
     *  }
     * )
     */
    public function getCompaniesAction(Request $request)
    {
        $account = $this->getUser()->getAccount();

        $options = $this->makeOptionsFromRequest($request);

        $options['wheres'] = array(
            'account' => $account,
        );

        $manager = $this->getPlCompanyManager();
        return $manager->getCompanies($options);
    }

    /**
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Companies",
     *  description="Create company",
     *  statusCodes={
     *      200="Returned when company is created",
     *      400="Returned when new company is not valid"
     *  },
     *  input="Leadsius\ApiBundle\Form\Type\PlCompanyType"
     * )
     */
    public function postCompaniesAction(Request $request)
    {
        $account = $this->getUser()->getAccount();

        $manager = $this->getPlCompanyManager();
        $company = $manager->create();
        $company->setAccount( $account );
        $form = $this->createForm(new PlCompanyType(), $company);
        $form->handleRequest($request);

        if (!$form->isValid()) {
            return View::create($form, 400);
        }

        // TODO: Uncomment to persist company
        $manager->save($company);
        $view = $this->view($company);
        $view
            ->setStatusCode(201)
            ->setLocation( $this->generateUrl(
                    'get_company',
                    array(
                        'company' => $company->getId(),
                        'api_key' => $request->query->get('api_key')
                    )
                )
            )
        ;
        return $this->handleView($view);
    }

    /**
     * @ParamConverter("company", class="LeadsiusApiBundle:PlCompany")
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Companies",
     *  resource=true,
     *  description="Retrieve a company",
     *  requirements={
     *      { "name"="company", "dataType"="integer", "requirement"="\d+", "description"="Company id to retrieve" }
     *  }
     * )
     *
     * @BelongsAccount("company", class="LeadsiusApiBundle:PlCompany")
     */
    public function getCompanyAction(PlCompany $company)
    {
        return $company;
    }

    /**
     * @ParamConverter("company", class="LeadsiusApiBundle:PlCompany")
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Companies",
     *  description="Delete a company",
     *  statusCodes={
     *      204="Eliminated"
     *  },
     *  requirements={
     *      { "name"="company", "dataType"="integer", "requirement"="\d+", "description"="Company id to delete" }
     *  }
     * )
     */
    public function deleteCompanyAction(PlCompany $company)
    {
        $this->getPlCompanyManager()->delete($company);
    }

    /**
     * @ParamConverter("company", class="LeadsiusApiBundle:PlCompany")
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Companies",
     *  description="Update company",
     *  statusCodes={
     *      200="Returned when company is created",
     *      400="Returned when new company is not valid"
     *  },
     *  requirements={
     *      { "name"="company", "dataType"="integer", "requirement"="\d+", "description"="Company id to update" }
     *  }
     * )
     */
    public function patchCompanyAction(Request $request, PlCompany $company)
    {
        $form = $this->createForm(new PlCompanyType(), $company, array(
            'method' => 'PATCH',
        ));
        $form->handleRequest($request);

        if (!$form->isValid()) {
            return View::create($form, 400);
        }

        $this->getPlCompanyManager()->save($company);
        $view = $this->view($company);
        $view
            ->setStatusCode(200)
        ;
        return $this->handleView($view);
    }

    /**
     * @ParamConverter("company", class="LeadsiusApiBundle:PlCompany")
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Companies",
     *  resource=true,
     *  description="Get all contacts in company",
     *  requirements={
     *      { "name"="company", "dataType"="integer", "requirement"="\d+", "description"="Company id to retrieve" }
     *  },
     *  parameters={
     *      {"name"="page", "dataType"="integer", "required"=false, "description"="Page to retrieve"},
     *      {"name"="page_size", "dataType"="integer", "required"=false, "description"="Contacts per page"},
     *      {"name"="sort", "dataType"="string", "required"=false, "description"="Fields to sort"},
     *      {"name"="sort_dir", "dataType"="string", "required"=false, "description"="Sort direction", "format"="asc|desc"},
     *      {"name"="query", "dataType"="string", "required"=false, "description"="Query to filter result"}
     *  }
     * )
     */
    public function getCompanyContactsAction(Request $request, PlCompany $company)
    {
        $account = $this->getUser()->getAccount();

        $options = $this->makeOptionsFromRequest($request);

        $options['wheres'] = array(
            'account' => $account,
            'company' => $company->getId(),
        );

        $manager = $this->get('leadsius_api.model.pl_contact_manager');
        $manager->setContainer($this->container);
        $data =  $manager->getContacts($options);

        return $data;
    }

    /**
     * @ParamConverter("company", class="LeadsiusApiBundle:PlCompany")
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Companies",
     *  description="Add contacts to company",
     *  requirements={
     *      { "name"="company", "dataType"="integer", "requirement"="\d+", "description"="Company id to contact" }
     *  }
     * )
     */
    public function postCompanyContactAction(Request $request, PlCompany $company)
    {
        $data = json_decode( $request->getContent() , true );

        if (!isset($data['contacts']) || empty($data['contacts']) || !is_array($data['contacts'])) {
            throw new HttpException(422, 'Not contacts to add');
        }

        $contacts = $this->getDoctrine()->getRepository('LeadsiusApiBundle:PlContact')->findContactsWithId($data['contacts']);

        if (empty($data['contacts'])) {
            throw new HttpException(422, 'Not contacts to add');
        }

        $this->getPlCompanyManager()->addContactsToCompany($company, $contacts);

        return $this->handleView( $this->view(null, 204) );
    }

    public function getPlCompanyManager()
    {
        return $this->container->get('leadsius_api.model.pl_company_manager');
    }
}
