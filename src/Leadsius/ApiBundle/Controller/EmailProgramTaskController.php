<?php

namespace Leadsius\ApiBundle\Controller;


use Leadsius\ApiBundle\Annotation\BelongsAccount;
use Leadsius\ApiBundle\Entity\MaWorkflow;
use Leadsius\ApiBundle\Entity\MaWorkflowTask;
use Leadsius\ApiBundle\Form\Type\MaWorkflowTaskType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\View\View;

class EmailProgramTaskController extends BaseController
{
    /**
     * @FOSRest\Get("/email-programs/{workflow}/tasks")
     * @ParamConverter("workflow", class="Leadsius\ApiBundle\Entity\MaWorkflow")
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Email Program Tasks",
     *  resource=true,
     *  description="Retrieve all email task from email program",
     *  requirements={
     *      { "name"="workflow", "dataType"="integer", "requirement"="\d+", "description"="" }
     *  },
     *  parameters={
     *      {"name"="page", "dataType"="integer", "required"=false, "description"="Page to retrieve"},
     *      {"name"="page_size", "dataType"="integer", "required"=false, "description"="Workflow tasks per page"},
     *      {"name"="sort", "dataType"="string", "required"=false, "description"="Fields to sort"},
     *      {"name"="sort_dir", "dataType"="string", "required"=false, "description"="Sort direction", "format"="asc|desc"},
     *      {"name"="type", "dataType"="string", "required"=false, "description"="Workflow Task type, use 'startWithTask' for scheduled emails, use 'startWithCondition' for triggers or 'email' for conditional emails (if opened or if not opened)", "format"="startWithTask|startWithCondition|email"},
     *      {"name"="query", "dataType"="string", "required"=false, "description"="Query to filter results"}
     *  }
     * )
     */
    public function getEmailProgramTasksAction(Request $request, MaWorkflow $workflow)
    {
        $account = $this->getUser()->getAccount();

        $options = $this->makeOptionsFromRequest($request);

        $options['account'] = $account;
        $options['workflow'] = $workflow->getId();
        $options['type'] = $request->get('type') ?: null;
        $options['wheres'] = array(
            'account' => $account,
        );

        $manager = $this->getMaWorkflowTaskManager();
        $manager->setContainer($this->container);
        $data =  $manager->getWorkflowTasks($options);

        return $data;
    }

    /**
     * @FOSRest\Post("/email-programs/{workflow}/tasks")
     * @ParamConverter("workflow", class="LeadsiusApiBundle:MaWorkflow")
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Email Program Tasks",
     *  description="Create an email program",
     *  statusCodes={
     *      200="Returned when workflow is created",
     *      400="Returned when the workflow is not valid"
     *  },
       input="Leadsius\ApiBundle\Form\Type\MaEmailCampaignType"
     * )
     */
    public function postEmailProgramTasksAction(Request $request, $workflow)
    {
        $user = $this->getUser();
        $account = $user->getAccount();

        $manager = $this->getMaWorkflowTaskManager();

        $workflowTask = $manager->createInitial($account, $user, $workflow);

        $form = $this->createForm(new MaWorkflowTaskType(), $workflowTask);
        $formHandler = $this->get('leadius_api.form.handler.create_workflow_task');
        if (!$formHandler->handle($form, $request)) {
            return View::create($form, 400);
        }

        $manager->save($workflowTask);
        $view = $this->view($workflowTask);
        $view
            ->setStatusCode(201)
            ->setLocation( $this->generateUrl(
                    'get_email_program_task',
                    array(
                        'workflow' => $workflow->getId(),
                        'workflow_task' => $workflowTask->getId(),
                        'api_key' => $request->query->get('api_key')
                    )
                )
            )
        ;
        return $this->handleView($view);
    }

    /**
     * @FOSRest\Get("/email-programs/{workflow}/tasks/{workflow_task}")
     * @ParamConverter("workflow", class="Leadsius\ApiBundle\Entity\MaWorkflow")
     * @ParamConverter("workflow_task", class="Leadsius\ApiBundle\Entity\MaWorkflowTask")
     *
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Email Program Tasks",
     *  resource=true,
     *  description="Retrieve a Workflow Task",
     *  requirements={
     *      { "name"="workflow", "dataType"="integer", "requirement"="\d+", "description"="Workflow id to retrieve" }
     *  }
     * )
     *
     * @BelongsAccount("workflow_task", class="Leadsius\ApiBundle\Entity\MaWorkflowTask")
     */
    public function getEmailProgramTaskAction(MaWorkflow $workflow, MaWorkflowTask $workflow_task)
    {
        // -- Validate Workflow Task belongs to Workflow
        if ($workflow->getId() !== $workflow_task->getWorkflow()->getId()) {
            throw $this->createNotFoundException('This email program doesn\'t have a task with id ' . $workflow_task->getId());
        }

        return $workflow_task;
    }

    private function getMaWorkflowTaskManager()
    {
        return $this->container->get('leadsius_api.model.ma_workflow_task_manager');
    }

    private function getMaEmailCampaignManager()
    {
        return $this->container->get('leadsius_api.model.ma_email_campaign_manager');
    }
}
