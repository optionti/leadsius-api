<?php

namespace Leadsius\ApiBundle\Controller;

use JMS\Serializer\SerializationContext;
use Leadsius\ApiBundle\Annotation\BelongsAccount;
use Leadsius\ApiBundle\Entity\MaWorkflow;
use Leadsius\ApiBundle\Form\Type\EmailProgramType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\View\View;

class EmailProgramController extends BaseController
{
    /**
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Email Programs",
     *  resource=true,
     *  description="Return all workflows or email programs",
     *  parameters={
     *      {"name"="page", "dataType"="integer", "required"=false, "description"="Page to retrieve"},
     *      {"name"="page_size", "dataType"="integer", "required"=false, "description"="Workflows per page"},
     *      {"name"="sort", "dataType"="string", "required"=false, "description"="Fields to sort"},
     *      {"name"="sort_dir", "dataType"="string", "required"=false, "description"="Sort direction", "format"="asc|desc"},
     *      {"name"="query", "dataType"="string", "required"=false, "description"="Query to filter results"}
     *  }
     * )
     * @FOSRest\Get("/email-programs")
     */
    public function getEmailProgramsAction(Request $request)
    {
        $account = $this->getUser()->getAccount();

        $options = $this->makeOptionsFromRequest($request);

        $options['account'] = $account;
        $options['wheres'] = array(
            'account' => $account,
            'type' => 'startWithTask'
        );

        $manager = $this->getMaEmailProgramManager();
        $manager->setContainer($this->container);
        $data =  $manager->getWorkflows($options);

        return $data;
    }

    /**
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Email Programs",
     *  description="Create an email program",
     *  statusCodes={
     *      200="Returned when workflow is created",
     *      400="Returned when the workflow is not valid"
     *  }
     * )
     * @FOSRest\Post("/email-programs")
     */
    public function postEmailProgramAction(Request $request)
    {
        $account = $this->getUser()->getAccount();
        $user = $this->getUser();
        $manager = $this->getMaEmailProgramManager();

        $program = $this
            ->getDoctrine()
            ->getRepository('LeadsiusApiBundle:MaProgram')
            ->findFirstForAccount($account)
        ;

        if (!$program) {
            throw $this->createNotFoundException('Not found program for this account');
        }

        $emailProgram = $manager->create();
        $emailProgram->setUser($user);
        $emailProgram->setType('startWithTask');
        $emailProgram->setAccount($account);
        $emailProgram->setProgram($program);
        $form = $this->createForm(new EmailProgramType(), $emailProgram);
        $form->handleRequest($request);

        if (!$form->isValid()) {
            return View::create($form, 400);
        }

        $manager->save($emailProgram);
        $view = $this->view($emailProgram);
        $view
            ->setStatusCode(201)
            ->setLocation( $this->generateUrl(
                    'get_email_program',
                    array(
                        'id' => $emailProgram->getId(),
                        'api_key' => $request->query->get('api_key')
                    )
                )
            )
        ;
        return $this->handleView($view);
    }

    /**
     * @FOSRest\Get("/email-programs/{id}")
     * @ParamConverter("email_program", class="LeadsiusApiBundle:MaWorkflow")
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Email Programs",
     *  resource=true,
     *  description="Retrieve an email program",
     *  requirements={
     *      { "name"="workflow", "dataType"="integer", "requirement"="\d+", "description"="Email Program id to retrieve" }
     *  }
     * )
     *
     * @BelongsAccount("email_program", class="LeadsiusApiBundle:MaWorkflow")
     */
    public function getEmailProgramAction(MaWorkflow $email_program)
    {
        // -- Validate Workflow is a Email Program type
        if (MaWorkflow::TYPE_START_WITH_TASK !== $email_program->getType()) {
            throw $this->createNotFoundException('Not found email program with this id');
        }

        return $email_program;
    }

    public function getMaEmailProgramManager()
    {
        return $this->container->get('leadsius_api.model.ma_workflow_manager');
    }
}
