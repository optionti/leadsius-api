<?php

namespace Leadsius\ApiBundle\Controller;

use JMS\Serializer\SerializationContext;
use Leadsius\ApiBundle\Annotation\BelongsAccount;
use Leadsius\ApiBundle\Entity\MaEmail;
use Leadsius\ApiBundle\Form\Type\MaEmailType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\View\View;

class EmailController extends BaseController
{
    /**
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Emails",
     *  resource=true,
     *  description="Return all emails",
     *  parameters={
     *      {"name"="page", "dataType"="integer", "required"=false, "description"="Page to retrieve"},
     *      {"name"="page_size", "dataType"="integer", "required"=false, "description"="Emails per page"},
     *      {"name"="sort", "dataType"="string", "required"=false, "description"="Fields to sort"},
     *      {"name"="sort_dir", "dataType"="string", "required"=false, "description"="Sort direction", "format"="asc|desc"},
     *      {"name"="query", "dataType"="string", "required"=false, "description"="Query to filter results"}
     *  }
     * )
     */
    public function getEmailsAction(Request $request)
    {
        $account = $this->getUser()->getAccount();

        $options = $this->makeOptionsFromRequest($request);

        $options['account'] = $account;
        $options['wheres'] = array(
            'account' => $account,
        );

        $manager = $this->getMaEmailManager();
        $manager->setContainer($this->container);
        $data =  $manager->getEmails($options);

        return $data;
    }

    /**
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Emails",
     *  description="Create email",
     *  statusCodes={
     *      200="Returned when email is created",
     *      400="Returned when the email is not valid"
     *  }
     * )
     */
    public function postEmailAction(Request $request)
    {
        $account = $this->getUser()->getAccount();
        $user = $this->getUser();
        $manager = $this->getMaEmailManager();
        $program = $this->getDoctrine()->getManager()->getRepository("LeadsiusApiBundle:MaProgram")->findOneById($account->getId());

        $email = $manager->create();
        $email->setProgram( $program );
        $email->setUser( $user );
        $email->setAccount( $account );
        $form = $this->createForm(new MaEmailType(), $email);
        $form->handleRequest($request);

        if (!$form->isValid()) {
            return View::create($form, 400);
        }

        $manager->save($email);
        $view = $this->view($email);
        $view
            ->setStatusCode(201)
            ->setLocation( $this->generateUrl(
                    'get_email',
                    array(
                        'email' => $email->getId(),
                        'api_key' => $request->query->get('api_key')
                    )
                )
            )
        ;
        return $this->handleView($view);
    }

    /**
     * @ParamConverter("email", class="LeadsiusApiBundle:MaEmail")
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Emails",
     *  resource=true,
     *  description="Retrieve en email",
     *  requirements={
     *      { "name"="email", "dataType"="integer", "requirement"="\d+", "description"="Email id to retrieve" }
     *  }
     * )
     *
     * @BelongsAccount("email", class="LeadsiusApiBundle:MaEmail")
     */
    public function getEmailAction(MaEmail $email)
    {
        return $email;
    }

    public function getMaEmailManager()
    {
        return $this->container->get('leadsius_api.model.ma_email_manager');
    }
}
