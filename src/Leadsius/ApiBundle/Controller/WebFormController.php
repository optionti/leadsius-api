<?php

namespace Leadsius\ApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use Leadsius\ApiBundle\Entity\MaWebform;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;


class WebFormController extends BaseController {

    /**
     * @Rest\View
     *
     * @Nelmio\ApiDoc(
     *  section="WebForms",
     *  resource=true,
     *  description="Return all webforms",
     *  parameters={
     *      {"name"="page", "dataType"="integer", "required"=false, "description"="Page to retrieve"},
     *      {"name"="page_size", "dataType"="integer", "required"=false, "description"="Contacts per page"},
     *      {"name"="sort", "dataType"="string", "required"=false, "description"="Fields to sort"},
     *      {"name"="sort_dir", "dataType"="string", "required"=false, "description"="Sort direction", "format"="asc|desc"},
     *      {"name"="query", "dataType"="string", "required"=false, "description"="Query to filter results"}
     *  }
     * )
     */
    public function getWebformsAction(Request $request)
    {
        $account = $this->getUser()->getAccount();

        $options = $this->makeOptionsFromRequest($request);

        $options['account'] = $account;
        $options['wheres'] = array(
            'account' => $account,
        );

        $rp = $this->getDoctrine()->getRepository('LeadsiusApiBundle:MaWebform');
        $data = $rp->findMaWebform($options);
        $total = $rp->findMaWebform($options, true);

        return array(
            'page' => $options['page'],
            'page_size' => $options['page_size'],
            'total_pages' => ceil($total / $options['page_size']),
            'total' => $total,
            'webforms' => $data,
        );
    }

    /**
     * @ParamConverter("webform", class="LeadsiusApiBundle:MaWebform")
     * @Rest\View
     *
     * @Nelmio\ApiDoc(
     *  section="WebForms",
     *  description="Retrieve webform",
     *  requirements={
     *      { "name"="webform", "dataType"="integer", "requirement"="\d+", "description"="Webform id to retrieve" }
     *  }
     * )
     */
    public function getWebformAction(Request $request, MaWebform $webform)
    {
        $user = $this->getUser();
        $account = $user->getAccount();
        $systemKey = $account->getSystemKey();

        // -- Utilities ------------------------------------------------------
        $leadsiusEncrypt = $this->get('leadsius_api.util.leadsius_encrypt');
        $leadsiusAppRoute = $this->container->getParameter('leadsius_app_route');

        // -- Make Tracker Pixel Url
        $trackerRouteBase = $this->container->getParameter('tracker_route');
        $trackingRoute = $trackerRouteBase . '/' . substr($systemKey, 0, 10) . '/tr?';
        $trackingLink = http_build_query(array(
            // -- Parameter to lpwfhtml type
            't' => 'lpwfhtml',
            'cn' => $webform->getProgram()->getId(),
            'w' => $webform->getId(),
            'sk' => $systemKey,
            // -- Paratemeters to all
            'ev' => 'view',
            'p' => 'webform',
            'wm' => $webform->getId(),
        ));
        $trackingOpen = $leadsiusEncrypt->encrypt($trackingLink);
        $trackingPixelUrl = $trackingRoute . 'id=' . $trackingOpen;

        // -- Set success value -----------------------------------------------
        switch ($webform->getConfirmationOption()) {
            case 'alertMessage':
                $successValue = $webform->getAlertMessage();
                break;
            case 'thankyouPage':
                $maLandingPage = $webform->getThankYouPage();

                if (empty($maLandingPage)) {
                    $successValue = $leadsiusAppRoute . '/publish/webform/response/thankyou';
                } else {
                    $successValue = $leadsiusAppRoute . '/viewer/lp?' . http_build_query(array(
                            'l' => $leadsiusEncrypt->encrypt($maLandingPage->getId()),
                        ));
                }
                break;
            case 'redirectExternalPage':
                $successValue = $webform->getWebformRedirectExternalPage();
                break;
            default:
                $successValue = $webform->getAlertMessage();
        }

        // -- Make submit url -------------------------------------------------
        $submitUrl = $leadsiusAppRoute . '/publish/webform/response';
        // TODO: Uncomment down code and comment up code
//        $submitUrl = $this->generateUrl('post_webform_process', array(
//            'webform' => $webform->getId(),
//        ), true);

        return array(
            'webform_id' => $webform->getId(),
            'webform_title' => $webform->getName(),
            'webform_object' => json_decode( $webform->getJsonContent(), true ),
            'webform_success' => array(
                'success_action' => $webform->getConfirmationOption(),
                'success_value' => $successValue,
            ),
            'submit_url' => $submitUrl,
            'tracking_pixel' => $trackingPixelUrl,
        );
    }

    /**
     * @ParamConverter("webform", class="LeadsiusApiBundle:MaWebform")
     * @Rest\View
     * @Rest\Get("/webforms/{webform}/process")
     */
    public function postWebformProcessAction(MaWebform $webform)
    {
        return $webform;
    }

    private function getMaWebformManager()
    {
        return $this->get('leadsius_api.model.ma_webform_manager');
    }
} 
