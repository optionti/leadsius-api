<?php

namespace Leadsius\ApiBundle\Controller;

use JMS\Serializer\SerializationContext;
use Leadsius\ApiBundle\Entity\PlContact;
use Leadsius\ApiBundle\Form\Type\PlContactType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\View\View;
use Leadsius\ApiBundle\Annotation\BelongsAccount;

class ContactController extends BaseController
{
    /**
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Contacts",
     *  resource=true,
     *  description="Return all contacts",
     *  parameters={
     *      {"name"="page", "dataType"="integer", "required"=false, "description"="Page to retrieve"},
     *      {"name"="page_size", "dataType"="integer", "required"=false, "description"="Contacts per page"},
     *      {"name"="sort", "dataType"="string", "required"=false, "description"="Fields to sort"},
     *      {"name"="sort_dir", "dataType"="string", "required"=false, "description"="Sort direction", "format"="asc|desc"},
     *      {"name"="query", "dataType"="string", "required"=false, "description"="Query to filter results"},
     *      {"name"="filter", "dataType"="integer", "required"=false, "description"="Filter id to filter results"}
     *  }
     * )
     */
    public function getContactsAction(Request $request)
    {
        $account = $this->getUser()->getAccount();

        $options = $this->makeOptionsFromRequest($request);

        $options['account'] = $account;
        $options['wheres'] = array(
            'account' => $account,
        );

        $manager = $this->getPlContactManager();
        $manager->setContainer($this->container);
        $data =  $manager->getContacts($options);

        return $data;
    }

    /**
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Contacts",
     *  description="Create contact",
     *  statusCodes={
     *      200="Returned when contacts is created",
     *      400="Returned when new contact is not valid"
     *  }
     * )
     */
    public function postContactsAction(Request $request)
    {
        $account = $this->getUser()->getAccount();

        $manager = $this->getPlContactManager();

        $contact = $manager->create();
        $contact->setAccount( $account );
        $form = $this->createForm(new PlContactType(), $contact);
        $form->handleRequest($request);

        if (!$form->isValid()) {
            return View::create($form, 400);
        }

        $manager->save($contact);
        $view = $this->view($contact);
        $view
            ->setStatusCode(201)
            ->setLocation( $this->generateUrl(
                    'get_contact',
                    array(
                        'contact' => $contact->getId(),
                        'api_key' => $request->query->get('api_key')
                    )
                )
            )
        ;
        return $this->handleView($view);
    }

    /**
     * @ParamConverter("contact", class="LeadsiusApiBundle:PlContact")
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Contacts",
     *  resource=true,
     *  description="Retrieve contact",
     *  requirements={
     *      { "name"="contact", "dataType"="integer", "requirement"="\d+", "description"="Contact id to retrieve" }
     *  }
     * )
     *
     * @BelongsAccount("contact", class="LeadsiusApiBundle:PlContact")
     */
    public function getContactAction(PlContact $contact)
    {
        return $contact;
    }

    /**
     * @ParamConverter("contact", class="LeadsiusApiBundle:PlContact")
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Contacts",
     *  description="Delete contact",
     *  statusCodes={
     *      204="Eliminated"
     *  },
     *  requirements={
     *      { "name"="contact", "dataType"="integer", "requirement"="\d+", "description"="Contact id to delete" }
     *  }
     * )
     */
    public function deleteContactAction(PlContact $contact)
    {
        $this->getPlContactManager()->delete($contact);
    }

    /**
     * @ParamConverter("contact", class="LeadsiusApiBundle:PlContact")
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Contacts",
     *  description="Update contact",
     *  requirements={
     *      { "name"="contact", "dataType"="integer", "requirement"="\d+", "description"="Contact id to update" }
     *  }
     * )
     */
    public function patchContactAction(Request $request, PlContact $contact)
    {
        $form = $this->createForm(new PlContactType(), $contact, array(
            'method' => 'PATCH',
        ));
        $form->handleRequest($request);

        if (!$form->isValid()) {
            return View::create($form, 400);
        }

        $this->getPlContactManager()->save($contact);
        $view = $this->view($contact);
        $view
            ->setStatusCode(200)
        ;
        return $this->handleView($view);
    }

    /**
     * @ParamConverter("contact", class="LeadsiusApiBundle:PlContact")
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Contacts",
     *  resource=true,
     *  description="Get contact activities",
     *  requirements={
     *      { "name"="contact", "dataType"="integer", "requirement"="\d+", "description"="Contact id to activities" }
     *  }
     * )
     */
    public function getContactActivitiesAction(Request $request, PlContact $contact)
    {
        $account = $this->getUser()->getAccount();

        $options = $this->makeOptionsFromRequest($request);
        
        $options['wheres'] = array(
            'activity_type' => $request->query->get('activity_type'),
            'activity_type_event' => $request->query->get('activity_type_event'),
            'workflow_type' => $request->query->get('workflow_type'),
            'activity_event' => $request->query->get('activity_event')
        );
        $site_key = $this->getUser()->getAccount()->getSystemKey();
        $id = $contact->getId();
        $manager = $this->getPlContactManager();
        $manager->setContainer($this->container);
        $data =  $manager->getActivities($options,$site_key,$id);

        return $data;
    }

    public function getPlContactManager()
    {
        return $this->container->get('leadsius_api.model.pl_contact_manager');
    }
}
