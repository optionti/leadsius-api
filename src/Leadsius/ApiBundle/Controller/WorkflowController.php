<?php

namespace Leadsius\ApiBundle\Controller;

use JMS\Serializer\SerializationContext;
use Leadsius\ApiBundle\Annotation\BelongsAccount;
use Leadsius\ApiBundle\Entity\MaWorkflow;
use Leadsius\ApiBundle\Form\Type\WorkflowType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\View\View;

class WorkflowController extends BaseController
{
    /**
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Workflows",
     *  resource=true,
     *  description="Return all workflows or email programs",
     *  parameters={
     *      {"name"="page", "dataType"="integer", "required"=false, "description"="Page to retrieve"},
     *      {"name"="page_size", "dataType"="integer", "required"=false, "description"="Workflows per page"},
     *      {"name"="sort", "dataType"="string", "required"=false, "description"="Fields to sort"},
     *      {"name"="sort_dir", "dataType"="string", "required"=false, "description"="Sort direction", "format"="asc|desc"},
     *      {"name"="query", "dataType"="string", "required"=false, "description"="Query to filter results"}
     *  }
     * )
     */
    public function getWorkflowsAction(Request $request)
    {
        $account = $this->getUser()->getAccount();

        $options = $this->makeOptionsFromRequest($request);

        $options['account'] = $account;
        $options['wheres'] = array(
            'account' => $account,
            'type' => 'startWithCondition'
        );

        $manager = $this->getMaWorkflowManager();
        $manager->setContainer($this->container);
        $data =  $manager->getWorkflows($options);

        return $data;
    }

    /**
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Workflows",
     *  description="Create workflow",
     *  statusCodes={
     *      200="Returned when workflow is created",
     *      400="Returned when the workflow is not valid"
     *  }
     * )
     */
    public function postWorkflowsAction(Request $request)
    {
        $account = $this->getUser()->getAccount();
        $user = $this->getUser();
        $manager = $this->getMaWorkflowManager();

        $program = $this
            ->getDoctrine()
            ->getRepository('LeadsiusApiBundle:MaProgram')
            ->findFirstForAccount($account)
        ;

        if (!$program) {
            throw $this->createNotFoundException('Not found program for this account');
        }

        $workflow = $manager->create();
        $workflow->setUser( $user );
        $workflow->setAccount( $account );
        $workflow->setType( 'startWithCondition' );
        $workflow->setProgram($program);
        $form = $this->createForm(new WorkflowType(), $workflow);
        $form->handleRequest($request);

        if (!$form->isValid()) {
            return View::create($form, 400);
        }

        $manager->save($workflow);
        $view = $this->view($workflow);
        $view
            ->setStatusCode(201)
            ->setLocation( $this->generateUrl(
                    'get_workflow',
                    array(
                        'workflow' => $workflow->getId(),
                        'api_key' => $request->query->get('api_key')
                    )
                )
            )
        ;
        return $this->handleView($view);
    }

    /**
     * @ParamConverter("workflow", class="LeadsiusApiBundle:MaWorkflow")
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Workflows",
     *  resource=true,
     *  description="Retrieve workflow",
     *  requirements={
     *      { "name"="workflow", "dataType"="integer", "requirement"="\d+", "description"="Workflow id to retrieve" }
     *  }
     * )
     *
     * @BelongsAccount("workflow", class="LeadsiusApiBundle:MaWorkflow")
     */
    public function getWorkflowAction(MaWorkflow $workflow)
    {
        // -- Validate Workflow is a Workflow type
        if (MaWorkflow::TYPE_START_WITH_CONDITION !== $workflow->getType()) {
            throw $this->createNotFoundException('Not found workflow with this id');
        }

        return $workflow;
    }

    public function getMaWorkflowManager()
    {
        return $this->container->get('leadsius_api.model.ma_workflow_manager');
    }
}
