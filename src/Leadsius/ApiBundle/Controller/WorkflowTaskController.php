<?php

namespace Leadsius\ApiBundle\Controller;

use JMS\Serializer\SerializationContext;
use Leadsius\ApiBundle\Annotation\BelongsAccount;
use Leadsius\ApiBundle\Entity\MaEmailCampaign;
use Leadsius\ApiBundle\Entity\MaWorkflow;
use Leadsius\ApiBundle\Entity\MaWorkflowTask;
use Leadsius\ApiBundle\Form\Type\MaWorkflowTaskType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\View\View;

class WorkflowTaskController extends BaseController
{
    /**
     * @ParamConverter("workflow", class="LeadsiusApiBundle:MaWorkflow")
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Workflows Tasks",
     *  resource=true,
     *  description="Return all workflow tasks",
     *  parameters={
     *      {"name"="page", "dataType"="integer", "required"=false, "description"="Page to retrieve"},
     *      {"name"="page_size", "dataType"="integer", "required"=false, "description"="Workflow tasks per page"},
     *      {"name"="sort", "dataType"="string", "required"=false, "description"="Fields to sort"},
     *      {"name"="sort_dir", "dataType"="string", "required"=false, "description"="Sort direction", "format"="asc|desc"},
     *      {"name"="type", "dataType"="string", "required"=false, "description"="Workflow Task type, use 'startWithTask' for scheduled emails, use 'startWithCondition' for triggers or 'email' for conditional emails (if opened or if not opened)", "format"="startWithTask|startWithCondition|email"},
     *      {"name"="query", "dataType"="string", "required"=false, "description"="Query to filter results"}
     *  }
     * )
     */
    public function getWorkflowsTasksAction(Request $request, MaWorkflow $workflow)
    {
        $account = $this->getUser()->getAccount();

        $options = $this->makeOptionsFromRequest($request);

        $options['account'] = $account;
        $options['workflow'] = $workflow->getId();
        $options['type'] = $request->get("type") ?: null;
        $options['wheres'] = array(
            'account' => $account,
        );

        $manager = $this->getMaWorkflowTaskManager();
        $manager->setContainer($this->container);
        $data =  $manager->getWorkflowTasks($options);

        return $data;
    }

    /**
     * @ParamConverter("workflow", class="LeadsiusApiBundle:MaWorkflow")
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Workflows Tasks",
     *  description="Create workflow task",
     *  statusCodes={
     *      200="Returned when a task is created",
     *      400="Returned when the task is not valid"
     *  }
     * )
     */
    public function postWorkflowsTasksAction(Request $request, MaWorkflow $workflow)
    {
        $user = $this->getUser();
        $account = $user->getAccount();

        $manager = $this->getMaWorkflowTaskManager();

        $workflowTask = $manager->createInitial($account, $user, $workflow);

        $form = $this->createForm(new MaWorkflowTaskType(), $workflowTask);
        $formHandler = $this->get('leadius_api.form.handler.create_workflow_task');
        if (!$formHandler->handle($form, $request)) {
            return View::create($form, 400);
        }

        $manager->save($workflowTask);
        $view = $this->view($workflowTask);
        $view
            ->setStatusCode(201)
            ->setLocation( $this->generateUrl(
                    'get_workflows_task',
                    array(
                        'workflow' => $workflow->getId(),
                        'workflow_task' => $workflowTask->getId(),
                        'api_key' => $request->query->get('api_key')
                    )
                )
            )
        ;
        return $this->handleView($view);
    }

    /**
     * @ParamConverter("workflow", class="LeadsiusApiBundle:MaWorkflow")
     * @ParamConverter("workflow_task", class="LeadsiusApiBundle:MaWorkflowTask")
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Workflows Tasks",
     *  resource=true,
     *  description="Retrieve a Workflow Task",
     *  requirements={
     *      { "name"="workflow", "dataType"="integer", "requirement"="\d+", "description"="Workflow id to retrieve" }
     *  }
     * )
     *
     * @BelongsAccount("workflow_task", class="LeadsiusApiBundle:MaWorkflowTask")
     */
    public function getWorkflowsTaskAction(MaWorkflow $workflow, MaWorkflowTask $workflow_task)
    {
        // -- Validate Workflow Task belongs to Workflow
        if ($workflow->getId() !== $workflow_task->getWorkflow()->getId()) {
            throw $this->createNotFoundException('This workflow doesn\'t have a task with id ' . $workflow_task->getId());
        }

        return $workflow_task;
    }

    public function getMaWorkflowTaskManager()
    {
        return $this->container->get('leadsius_api.model.ma_workflow_task_manager');
    }

    public function getMaEmailCampaignManager()
    {
        return $this->container->get('leadsius_api.model.ma_email_campaign_manager');
    }
}
