<?php

namespace Leadsius\ApiBundle\Controller;

use JMS\Serializer\SerializationContext;
use Leadsius\ApiBundle\Annotation\BelongsAccount;
use Leadsius\ApiBundle\Entity\PlList;
use Leadsius\ApiBundle\Form\Type\PlListType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\View\View;

class ListController extends BaseController
{
    /**
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Lists",
     *  resource=true,
     *  description="Return all lists",
     *  parameters={
     *      {"name"="page", "dataType"="integer", "required"=false, "description"="Page to retrieve"},
     *      {"name"="page_size", "dataType"="integer", "required"=false, "description"="Lists per page"},
     *      {"name"="sort", "dataType"="string", "required"=false, "description"="Fields to sort"},
     *      {"name"="sort_dir", "dataType"="string", "required"=false, "description"="Sort direction", "format"="asc|desc"},
     *      {"name"="query", "dataType"="string", "required"=false, "description"="Query to filter result"}
     *  }
     * )
     */
    public function getListsAction(Request $request)
    {
        $account = $this->getUser()->getAccount();

        $options = $this->makeOptionsFromRequest($request);

        $options['wheres'] = array(
            'account' => $account,
            'type' => $request->query->get('type'),
            'populate' => $request->query->get('populate'),
        );

        $manager = $this->getPlListManager();
        return $manager->getLists($options);
    }

    /**
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Lists",
     *  description="Create list",
     *  statusCodes={
     *      201="Returned when lists is created",
     *      400="Returned when new list is not valid"
     *  }
     * )
     */
    public function postListsAction(Request $request)
    {
        $account = $this->getUser()->getAccount();
        $manager = $this->getPlListManager();

        $list = $manager->create();
        $list->setAccount( $account );
        $form = $this->createForm(new PlListType(), $list);
        $form->handleRequest($request);

        if (!$form->isValid()) {
            return View::create($form, 400);
        }
        
        $manager->save($list);
        $view = $this->view($list);
        $view
            ->setStatusCode(201)
            ->setLocation( $this->generateUrl(
                    'get_list',
                    array(
                        'list' => $list->getId(),
                        'api_key' => $request->query->get('api_key')
                    )
                )
            )
        ;
        return $this->handleView($view);
    }

    /**
     * @ParamConverter("list", class="LeadsiusApiBundle:PlList")
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Lists",
     *  resource=true,
     *  description="Retrieve list",
     *  requirements={
     *      { "name"="list", "dataType"="integer", "requirement"="\d+", "description"="List id to retrieve" }
     *  }
     * )
     *
     * @BelongsAccount("list", class="LeadsiusApiBundle:PlList")
     */
    public function getListAction(PlList $list)
    {
        return $list;
    }

    /**
     * @ParamConverter("list", class="LeadsiusApiBundle:PlList")
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Lists",
     *  description="Delete list",
     *  statusCodes={
     *      204="Eliminated"
     *  },
     *  requirements={
     *      { "name"="list", "dataType"="integer", "requirement"="\d+", "description"="List id to delete" }
     *  }
     * )
     */
    public function deleteListAction(PlList $list)
    {
        $this->getPlListManager()->delete($list);
    }

    /**
     * @ParamConverter("list", class="LeadsiusApiBundle:PlList")
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Lists",
     *  description="Update list",
     *  requirements={
     *      { "name"="list", "dataType"="integer", "requirement"="\d+", "description"="List id to update" }
     *  }
     * )
     */
    public function patchListAction(Request $request, PlList $list)
    {
        $form = $this->createForm(new PlListType(), $list, array(
            'method' => 'PATCH',
        ));
        $form->handleRequest($request);

        if (!$form->isValid()) {
            return View::create($form, 400);
        }

        $this->getPlListManager()->save($list);
        $view = $this->view($list);
        $view
            ->setStatusCode(200)
        ;
        return $this->handleView($view);
    }
    
    
    /**
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Lists",
     *  resource=true,
     *  description="Return all Contacts by List",
     *  parameters={
     *      {"name"="page", "dataType"="integer", "required"=false, "description"="Page to retrieve"},
     *      {"name"="page_size", "dataType"="integer", "required"=false, "description"="Lists per page"},
     *      {"name"="sort", "dataType"="string", "required"=false, "description"="Fields to sort"},
     *      {"name"="sort_dir", "dataType"="string", "required"=false, "description"="Sort direction", "format"="asc|desc"}
     *  }
     * )
     */
    public function getListContactsAction(Request $request, PlList $list)
    {
        $account = $this->getUser()->getAccount();

        $options = $this->makeOptionsFromRequest($request);
        
        if($list->getType()==='static'){   
            $options['wheres'] = array(
                'account' => $account,
            );

            $options['joins'] = array(
                'lists' => $list->getId(),
            );
        }
        if($list->getType()==='smart'){ 
            $options['account'] =  $account;
            $options['filter'] = $list->getListFilter();
        }
        $manager = $this->get('leadsius_api.model.pl_contact_manager');
        $manager->setContainer($this->container);
        $data =  $manager->getContacts($options);
        
        return $data;
    }

    /**
     * @ParamConverter("list", class="LeadsiusApiBundle:PlList")
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Lists",
     *  description="Add contacts to list",
     *  requirements={
     *      { "name"="list", "dataType"="integer", "requirement"="\d+", "description"="List id to post" }
     *  }
     * )
     */
    public function postListContactAction(Request $request, PlList $list)
    {
        $data = json_decode( $request->getContent() , true );

        if (!isset($data['contacts']) || empty($data['contacts']) || !is_array($data['contacts'])) {
            throw new HttpException(422, 'Not contacts to add');
        }

        $contacts = $this->getDoctrine()->getRepository('LeadsiusApiBundle:PlContact')->findContactsWithIdAndNotInList($data['contacts'], $list->getId());

        if (empty($contacts)) {
            throw new HttpException(422, 'Not contacts to add');
        }

        $this->getPlListManager()->addContactsToList($list, $contacts);

        return $this->handleView( $this->view(null, 204) );
    }

    public function getPlListManager()
    {
        return $this->container->get('leadsius_api.model.pl_list_manager');
    }
}
