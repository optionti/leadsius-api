<?php

namespace Leadsius\ApiBundle\Controller;

use Leadsius\ApiBundle\Annotation\BelongsAccount;
use Leadsius\ApiBundle\Entity\PlFilter;
use Leadsius\ApiBundle\Form\Type\PlFilterType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpKernel\Exception\HttpException;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\View\View;

class FilterController extends BaseController
{
    /**
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Filters",
     *  resource=true,
     *  description="Return all filters",
     *  parameters={
     *      {"name"="page", "dataType"="integer", "required"=false, "description"="Page to retrieve"},
     *      {"name"="page_size", "dataType"="integer", "required"=false, "description"="Contacts per page"},
     *      {"name"="sort", "dataType"="string", "required"=false, "description"="Fields to sort"},
     *      {"name"="sort_dir", "dataType"="string", "required"=false, "description"="Sort direction", "format"="asc|desc"},
     *      {"name"="query", "dataType"="string", "required"=false, "description"="Query to filter result"},
     *      {"name"="category", "dataType"="string", "required"=false, "description"="Category to filter result"}
     *  }
     * )
     */
    public function getFiltersAction(Request $request)
    {
        $account = $this->getUser()->getAccount();

        $options = $this->makeOptionsFromRequest($request);

        $options['wheres'] = array(
            'account' => $account,
            'category' => $request->get('category'),
        );

        $filters = $this->getDoctrine()->getRepository('LeadsiusApiBundle:PlFilter')->findFilters($options);
        $total_filters = $this->getDoctrine()->getRepository('LeadsiusApiBundle:PlFilter')->findFilters($options, true);
        $total_pages = ceil( $total_filters / $options['page_size'] );

        $data = array(
            'page' => $options['page'],
            'page_size' => $options['page_size'],
            'total' => $total_filters,
            'total_pages' => $total_pages,
            'filters' => $filters,
        );

        return $data;
    }

    /**
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Filters",
     *  description="Create filter",
     *  statusCodes={
     *      201="Returned when filter is created",
     *      400="Returned when new filter is not valid"
     *  },
     *  input="Leadsius\ApiBundle\Form\Type\PlFilterType"
     * )
     */
    public function postFiltersAction(Request $request)
    {
        $user = $this->getUser();
        $account = $user->getAccount();

        $filter = new PlFilter();
        $filter->setUser( $user );
        $filter->setAccount( $account );
        $form = $this->createForm(new PlFilterType(), $filter);
        $form->handleRequest($request);

        if (!$form->isValid()) {
            return View::create($form, 400);
        }

        $this->getPlFilterManager()->save( $filter );
        $view = $this->view($filter);
        $view
            ->setStatusCode(201)
            ->setLocation( $this->generateUrl(
                    'get_filters',
                    array(
                        'company' => $filter->getId(),
                        'api_key' => $request->query->get('api_key')
                    )
                )
            )
        ;
        return $this->handleView($view);
    }

    /**
     * @ParamConverter("filter", class="LeadsiusApiBundle:PlFilter")
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Filters",
     *  resource=true,
     *  description="Retrieve a filter",
     *  requirements={
     *      { "name"="filter", "dataType"="integer", "requirement"="\d+", "description"="Filter id to retrieve" }
     *  }
     * )
     *
     * @BelongsAccount("filter", class="LeadsiusApiBundle:PlFilter")
     */
    public function getFilterAction(PlFilter $filter)
    {
        return $filter;
    }

    /**
     * @ParamConverter("filter", class="LeadsiusApiBundle:PlFilter")
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Filters",
     *  description="Update filter",
     *  statusCodes={
     *      200="Returned when company is created",
     *      400="Returned when new company is not valid"
     *  },
     *  requirements={
     *      { "name"="filter", "dataType"="integer", "requirement"="\d+", "description"="Filter id to update" }
     *  }
     * )
     */
    public function patchFilterAction(Request $request, PlFilter $filter)
    {
        $form = $this->createForm(new PlFilterType(), $filter, array(
            'method' => 'PATCH',
        ));
        $form->handleRequest($request);

        if (!$form->isValid()) {
            return View::create($form, 400);
        }

        $this->getPlFilterManager()->save( $filter );
        $view = $this->view($filter);
        $view
            ->setStatusCode(200)
        ;
        return $this->handleView($view);
    }
    /**
     * @ParamConverter("filter", class="LeadsiusApiBundle:PlFilter")
     * @FOSRest\View
     * 
     * @ApiDoc(
     *  section="Filters",
     *  description="Delete a filter",
     *  statusCodes={
     *      204="Eliminated"
     *  },
     *  requirements={
     *      { "name"="filter", "dataType"="integer", "requirement"="\d+", "description"="Filter id to delete" }
     *  }
     * )
     */
    public function deleteFilterAction(PlFilter $filter)
    {
        $this->getPlFilterManager()->delete($filter);
    }

    public function getPlFilterManager()
    {
        return $this->container->get('leadsius_api.model.pl_filter_manager');
    }
}
