<?php

namespace Leadsius\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('LeadsiusApiBundle:Default:index.html.twig', array('name' => $name));
    }
}
