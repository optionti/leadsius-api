<?php

namespace Leadsius\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;

class BaseController extends FOSRestController
{
    /**
     * @param Request $request
     * @return array
     */
    public function makeOptionsFromRequest(Request $request)
    {
        $options = array(
            'page' => (int) $request->query->get('page', 1),
            'page_size' => (int) $request->query->get('page_size', $this->container->getParameter('page_size_default')),
            'sort' => $request->query->get('sort', 'id'),
            'sort_dir' => $request->query->get('sort_dir', 'asc'),
            'query' => $request->query->get('query'),
            'filter' => $request->query->get('filter'),
        );

        $options['page'] = 0 < $options['page'] ? $options['page'] : 1;
        $options['page_size'] = 0 < $options['page_size'] ? $options['page_size'] : $this->container->getParameter('page_size_default');

        return $options;
    }
}
