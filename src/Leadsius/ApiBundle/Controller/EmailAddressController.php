<?php

namespace Leadsius\ApiBundle\Controller;

use JMS\Serializer\SerializationContext;
use Leadsius\ApiBundle\Annotation\BelongsAccount;
use Leadsius\ApiBundle\Entity\MaSystemEmail;
use Leadsius\ApiBundle\Form\Type\MaSystemEmailType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;

class EmailAddressController extends BaseController
{
    /**
     * @Get("/email-addresses")
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Email Addresses",
     *  resource=true,
     *  description="Return all email addresses",
     *  parameters={
     *      {"name"="page", "dataType"="integer", "required"=false, "description"="Page to retrieve"},
     *      {"name"="page_size", "dataType"="integer", "required"=false, "description"="Email addresses per page"},
     *      {"name"="sort", "dataType"="string", "required"=false, "description"="Fields to sort"},
     *      {"name"="sort_dir", "dataType"="string", "required"=false, "description"="Sort direction", "format"="asc|desc"},
     *      {"name"="category", "dataType"="string", "required"=false, "description"="Email address category, use 'sender' for Senders or 'bounce' for Bounces or 'reply' for No Reply addresses", "format"="sender|bounce|reply"},
     *      {"name"="query", "dataType"="string", "required"=false, "description"="Query to filter results"}
     *  }
     * )
     */
    public function getEmailAddressesAction(Request $request)
    {
        $account = $this->getUser()->getAccount();

        $options = $this->makeOptionsFromRequest($request);

        $options['account'] = $account;
        $options['category'] = $request->get("category") ?: null;
        $options['wheres'] = array(
            'account' => $account,
            'hasEnabled' => true
        );

        $manager = $this->getMaSystemEmailManager();
        $manager->setContainer($this->container);
        $data =  $manager->getEmailaddresses($options);

        return $data;
    }

    /**
     * @Post("/email-addresses")
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Email Addresses",
     *  description="Create email address",
     *  statusCodes={
     *      200="Returned when email address is created",
     *      400="Returned when the email address is not valid"
     *  }
     * )
     */
    public function postEmailAddressAction(Request $request)
    {
        $account = $this->getUser()->getAccount();
        $manager = $this->getMaSystemEmailManager();

        $emailAddress = $manager->create();
        $emailAddress->setAccount( $account );
        $form = $this->createForm(new MaSystemEmailType(), $emailAddress);
        $form->handleRequest($request);

        if (!$form->isValid()) {
            return View::create($form, 400);
        }

        $manager->save($emailAddress);
        $view = $this->view($emailAddress);
        $view
            ->setStatusCode(201)
            ->setLocation( $this->generateUrl(
                    'get_email_address',
                    array(
                        'id' => $emailAddress->getId(),
                        'api_key' => $request->query->get('api_key')
                    )
                )
            )
        ;
        return $this->handleView($view);
    }

    /**
     * @Get("/email-addresses/{email_address}")
     * @ParamConverter("email_address", class="LeadsiusApiBundle:MaSystemEmail")
     * @FOSRest\View
     *
     * @ApiDoc(
     *  section="Email Addresses",
     *  resource=true,
     *  description="Retrieve an email address",
     *  requirements={
     *      { "name"="email", "dataType"="integer", "requirement"="\d+", "description"="Email address id to retrieve" }
     *  }
     * )
     *
     * @BelongsAccount("email_address", class="LeadsiusApiBundle:MaSystemEmail")
     */
    public function getEmailAddressAction(MaSystemEmail $email_address)
    {
        return $email_address;
    }

    public function getMaSystemEmailManager()
    {
        return $this->container->get('leadsius_api.model.ma_system_email_manager');
    }
}
